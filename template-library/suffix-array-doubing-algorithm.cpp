//ural1297 - Palindrome
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const int N = 2 * 1000 + 10;
const int LOG = 13;

int n, old_n;
char a[N];
int buffer[2][N], *rank(buffer[0]), *tmp(buffer[1]);
int sa[N], height[N];
int log_2[N], minimum[N][LOG];

bool check(int i, int j, int l) {
    return tmp[i] == tmp[j] && tmp[i + l] == tmp[j + l];
}

void sort(int m) {
    static int w[N];
    std::fill(w, w + m, 0);
    for (int i = 0; i < n; i++) {
        w[rank[i]]++;
    }
    for (int i = 1; i < m; i++) {
        w[i] += w[i - 1];
    }
    for (int i = n - 1; i >= 0; i--) {
        sa[--w[rank[tmp[i]]]] = tmp[i];
    }
}

void prepare() {
    int i, j, p, m(256);
    for (i = 0; i < n; i++) {
        tmp[i] = i;
        rank[i] = a[i];
    }
    sort(m);
    for (j = p = 1; p < n; j <<= 1, m = p) {
        for (p = 0, i = n - j; i < n; i++) {
            tmp[p++] = i;
        }
        for (i = 0; i < n; i++) {
            sa[i] >= j ? tmp[p++] = sa[i] - j : 0;
        }
        sort(m);
        std::swap(rank, tmp);
        for (rank[sa[0]] = 0, i = p = 1; i < n; i++) {
            rank[sa[i]] = check(sa[i - 1], sa[i], j) ? p - 1 : p++;
        }
    }
    for (i = p = 0; i < n; i++, p ? p-- : 0) {
        if (rank[i]) {
            j = sa[rank[i] - 1];
            for (; a[i + p] == a[j + p]; p++);
            minimum[rank[i]][0] = height[rank[i]] = p;
        }
    }
    for (j = 1; 1 << j < n; j++) {
        for (i = 1; i + (1 << j) <= n; i++) {
            minimum[i][j] = std::min(minimum[i][j - 1], minimum[i + (1 << j - 1)][j - 1]);
        }
    }
}

int query(int s, int t) {
    if (s > t) {
        std::swap(s, t);
    }
    s++;
    int l = log_2[t - s + 1];
    return std::min(minimum[s][l], minimum[t - (1 << l) + 1][l]);
}

int main() {
    for (int i = 2; i < N; i++) {
        log_2[i] = log_2[i >> 1] + 1;
    }
    gets(a);
    old_n = strlen(a);
    n = old_n << 1;
    for (int i = 0; i < old_n; i++) {
        a[n - i] = a[i];
    }
    a[old_n] = '*';
    a[(++n)++] = 0;
    prepare();
    int begin = 0, end = 1;
    for (int i = 0; i < old_n; i++) {
        if (0 < i && i < old_n - 1) {
            int l = query(rank[i + 1], rank[n - 2 - (i - 1)]);
            if (l * 2 + 1 > end - begin) {
                begin = i - l;
                end = i + l + 1;
            }
        }
        if (0 < i) {
            int l = query(rank[i], rank[n - 2 - (i - 1)]);
            if (l * 2 > end - begin) {
                begin = i - l;
                end = i + l;
            }
        }
    }
    puts(std::string(a + begin, a + end).c_str());
}

