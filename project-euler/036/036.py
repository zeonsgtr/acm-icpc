#!/usr/bin/python3
import itertools
import fractions

r = 0
n = 10 ** 6
for i in range(1, n):
    l1 = str(i)
    l2 = str(i)[::-1]
    if l1 == l2:
        l1 = list(str(bin(i)))[2:]
        l2 = list(str(bin(i)))[-1:1:-1]
        if l1 == l2:
            print(i)
            r = r + i
print(r)
