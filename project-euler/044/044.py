#!/usr/bin/python3
import itertools
import fractions

n = 10000
l = [i * (3 * i - 1) // 2 for i in range(1, n)]
s = set(l)
r = 10 ** 20
for i in range(len(l)):
    print(i)
    for j in range(i + 1, len(l)):
        if l[i] + l[j] in s and abs(l[i] - l[j]) in s:
            r = min(r, abs(l[i] - l[j]))
            print(r)
print(r)
