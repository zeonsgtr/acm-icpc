#!/usr/bin/python3
upper = 10 ** 6
res = 0
for x in range(2, upper):
    l = list(str(x))
    s = 0
    for i in l:
        s = s + int(i) ** 5
    if s == x:
        print(x)
        res = res + x
print(res)
