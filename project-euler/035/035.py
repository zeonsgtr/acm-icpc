#!/usr/bin/python3
import itertools
import fractions

def is_prime(x):
    if x < 2:
        return False
    i = 2
    while i * i <= x:
        if x % i == 0:
            return False
        i = i + 1
    return True

r = 0
upper = 10 ** 6
for i in range(2, upper):
    if is_prime(i):
        l = list(str(i))
        flag = True
        for j in range(1, len(l)):
            tl = l[j:] + l[:j]
            #print(l, tl)
            #if i == 719:
            #    quit()
            s = 0
            for k in tl:
                s = s * 10 + int(k)
            flag = is_prime(s)
            if not flag:
                break
        if flag:
            print(i)
            r = r + 1
print(r)
