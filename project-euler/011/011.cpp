#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <sstream>
#include <bitset>
#include <numeric>
#include <climits>
#include <string>
#include <cctype>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <vector>
#include <queue>
#include <list>
#include <map>
#include <set>
using namespace std;

#define foreach(e, x) for (__typeof((x).begin()) e = (x).begin(); e != (x).end(); ++e)

typedef long long int64; 

const int N = 20;

int a[N][N];

int main() {
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			scanf("%d", a[i] + j);
		}
	}
	int result = 0;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			if (i >= 3) {
				result = max(result, a[i][j] * a[i - 1][j] * a[i - 2][j] * a[i - 3][j]);
			}
			if (j >= 3) {
				result = max(result, a[i][j] * a[i][j - 1] * a[i][j - 2] * a[i][j - 3]);
			}
			if (i >= 3 && j >= 3) {
				result = max(result, a[i][j] * a[i - 1][j - 1] * a[i - 2][j - 2] * a[i - 3][j - 3]);
			}
			if (i >= 3 && j + 3 < N) {
				result = max(result, a[i][j] * a[i - 1][j + 1] * a[i - 2][j + 2] * a[i - 3][j + 3]);
			}
		}
	}
	cout << result << endl;
}

