#!/usr/bin/python3
import itertools
import fractions

num = set()
for i in range(1, 1000):
    num.add(i * (i + 1) // 2)
in_file = open('words.txt', 'r')
s = in_file.readline().split(',')
r = 0
for x in s:
    t = 0
    for j in range(1, len(x) - 1):
        t = t + ord(x[j]) - ord('A') + 1
    if t in num:
        r = r + 1
print(r)
