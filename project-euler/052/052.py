#!/usr/bin/python3
import itertools

x = 1
n = 7
while True:
    l = [([]) for i in range(1, n + 1)]
    flag = True
    for i in range(1, n):
        l[i] = list(str(i * x))
        l[i].sort()
        if i > 1 and l[i] != l[i - 1]:
            flag = False
            break
    print(x)
    if flag:
        break
    x = x + 1
