#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <sstream>
#include <bitset>
#include <numeric>
#include <climits>
#include <string>
#include <cctype>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <vector>
#include <queue>
#include <list>
#include <map>
#include <set>
using namespace std;

#define foreach(e, x) for (__typeof((x).begin()) e = (x).begin(); e != (x).end(); ++e)

typedef long long int64; 

char buf[1111];

int main() {
	string str;
	while (gets(buf)) {
		str += string(buf);
	}
	int result = 0;
	for (int i = 0; i + 5 <= (int)str.size(); i++) {
		int value = 1;
		for (int j = 0; j < 5; j++) {
			value *= str[i + j] - '0';
		}
		result = max(result, value);
	}
	cout << result << endl;
}

