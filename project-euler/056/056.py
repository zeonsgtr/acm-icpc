#!/usr/bin/python3
import itertools

s = 0
r = 0
n = 100
for a in range(1, n):
    for b in range(1, n):
        x = a ** b
        t = 0
        l = list(str(x))
        for i in l:
            t = t + int(i)
        if t > s:
            s = t
            r = x
    print(a)
print(s, r)
