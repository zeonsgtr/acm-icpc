#!/usr/bin/python3
n = 1001
a = [([0] * n) for i in range(n)]
a[n // 2][n // 2] = 1
x, y = n // 2, n // 2
now = 2
for i in range(2, n, 2):
    x = x - 1
    y = y + 1
    #print(i)
    for j in range(i):
        x = x + 1
        #print(x, y)
        a[x][y] = now
        now = now + 1
    for j in range(i):
        y = y - 1
        #print(x, y)
        a[x][y] = now
        now = now + 1
    for j in range(i):
        x = x - 1
        #print(x, y)
        a[x][y] = now
        now = now + 1
    for j in range(i):
        y = y + 1
        #print(x, y)
        a[x][y] = now
        now = now + 1
s = 0
for i in range(n):
    s += a[i][i] + a[i][n - i - 1]
print(s - 1)
