#!/usr/bin/python3
import itertools
import fractions

fac = [1]
for i in range(1, 10):
    fac.append(fac[-1] * i)
upper = 10 ** 6
r = 0
for x in range(10, upper):
    s = 0
    l = list(str(x))
    for j in l:
        s = s + fac[int(j)]
        if s > x:
            break
    if s == x:
        print(x)
        r = r + x
print(r)
