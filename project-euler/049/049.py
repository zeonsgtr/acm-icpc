#!/usr/bin/python3
import itertools

def is_prime(x):
    for i in range(2, x):
        if i * i > x:
            break
        if x % i == 0:
            return False
    return True

'''
i = 1001
while True:
    print(i)
    if i == 1487:
        i = i + 1
        continue
    l1 = list(str(i))
    if l1 in perms and is_prime(i):
        l2 = list(str(i + 3330))
        if l2 in perms and is_prime(i + 3330):
            l3 = list(str(i + 6660))
            if l3 in perms and is_prime(i + 6660):
                print(str(i) + str(i + 3330) + str(i + 6660))
                quit()
    i = i + 1
'''

def is_perm(l2, l1):
    if len(l2) != 4:
        return False
    for i in l2:
        if not i in l1:
            return False
    for i in l1:
        if not i in l2:
            return False
    return True

for i in range(1111, 10000):
    if i == 1487:
        continue
    l1 = tuple(str(i))
    if is_prime(i):
        for delta in range(1, 10000):
            l2 = tuple(str(i + delta))
            if is_prime(i + delta) and is_perm(l2, l1):
                l3 = tuple(str(i + delta + delta))
                if is_prime(i + delta + delta) and is_perm(l3, l1):
                    print(i, delta)
                    print(str(i) + str(i + delta) + str(i + delta + delta))
                    quit()
    print(i)
