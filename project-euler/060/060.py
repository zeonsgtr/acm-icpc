#!/usr/bin/python3

def is_prime(x):
    i = 2
    while i * i <= x:
        if x % i == 0:
            return False
        i = i + 1
    return True

l = []
n = 10 ** 5
for a in range(2, n):
    if is_prime(a):
        l.append(a)
        print(a)
print("step 1 finished")

def concatenate(a, b):
    return int(str(a) + str(b))

m = len(l)
for a in range(0, m):
    for b in range(a + 1, m):
        if not is_prime(concatenate(l[a], l[b])) or not is_prime(concatenate(l[b], l[a])):
            continue
        for c in range(b + 1, m):
            print("a = ", l[a], "b = ", l[b], "c = ", l[c])
            if not is_prime(concatenate(l[a], l[c])) or not is_prime(concatenate(l[c], l[a])):
                continue
            if not is_prime(concatenate(l[b], l[c])) or not is_prime(concatenate(l[c], l[b])):
                continue
            for d in range(c + 1, m):
                if not is_prime(concatenate(l[a], l[d])) or not is_prime(concatenate(l[d], l[a])):
                    continue
                if not is_prime(concatenate(l[b], l[d])) or not is_prime(concatenate(l[d], l[b])):
                    continue
                if not is_prime(concatenate(l[c], l[d])) or not is_prime(concatenate(l[d], l[c])):
                    continue
                for e in range(d + 1, m):
                    if not is_prime(concatenate(l[a], l[e])) or not is_prime(concatenate(l[e], l[a])):
                        continue
                    if not is_prime(concatenate(l[b], l[e])) or not is_prime(concatenate(l[e], l[b])):
                        continue
                    if not is_prime(concatenate(l[c], l[e])) or not is_prime(concatenate(l[e], l[c])):
                        continue
                    if not is_prime(concatenate(l[d], l[e])) or not is_prime(concatenate(l[e], l[d])):
                        continue
                    print(l[a], l[b], l[c], l[d], l[e])
                    print("sum = ", l[a] + l[b] + l[c] + l[d] + l[e])
