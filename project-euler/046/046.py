#!/usr/bin/python3
import itertools

n = 10 ** 5
v = [True for i in range(0, n)]
v[0] = v[1] = False
primes = []
for i in range(2, n):
    if v[i]:
        primes.append(i)
        j = i * i
        while j < n:
            v[j] = False
            j = j + i
for i in range(3, n):
    if not v[i] and (i & 1 == 1):
        j = 0
        flag = False
        while primes[j] < i:
            if int(((i - primes[j]) / 2) ** 0.5) ** 2 * 2 + primes[j] == i:
                flag = True
                break
            j = j + 1
        if not flag:
            print(i)
            break
