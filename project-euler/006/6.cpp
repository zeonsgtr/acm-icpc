#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <sstream>
#include <bitset>
#include <numeric>
#include <climits>
#include <string>
#include <cctype>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <vector>
#include <queue>
#include <list>
#include <map>
#include <set>
using namespace std;

#define foreach(e, x) for (__typeof((x).begin()) e = (x).begin(); e != (x).end(); ++e)

typedef long long int64; 

namespace Euler {
	template <class T> 
	string toString(T x) {
		ostringstream sout;
		sout << x;
		return sout.str();
	}

	bool isPalindrome(const string &s) {
		for (int i = 0; i + i <= (int)s.size(); i++) {
			if (s[i] != s[(int)s.size() - i - 1]) {
				return false;
			}
		}
		return true;
	}
}

int main() {
	int sum[2] = {};
	for (int i = 1; i <= 100; i++) {
		sum[0] += i * i;
		sum[1] += i;
	}
	cout << sum[1] * sum[1] - sum[0] << endl;
}

