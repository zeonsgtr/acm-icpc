#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <sstream>
#include <bitset>
#include <numeric>
#include <climits>
#include <string>
#include <cctype>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <vector>
#include <queue>
#include <list>
#include <map>
#include <set>
using namespace std;

#define foreach(e, x) for (__typeof((x).begin()) e = (x).begin(); e != (x).end(); ++e)

typedef long long int64; 

int main() {
	int n = 1000;
	for (int a = 1; a <= n; a++) {
		for (int b = a + 1; b <= n; b++) {
			int c = n - a - b;
			if (b < c && a * a + b * b == c * c) {
				cout << a * b * c << endl;
			}
		}
	}
}

