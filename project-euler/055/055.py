#!/usr/bin/python3
import itertools

def reverse(x):
    l = list(str(x))
    l.reverse()
    s = 0
    for i in l:
        s = s * 10 + int(i)
    return s

result = 0
n = 10 ** 4
for i in range(1, n):
    x = i
    flag = False
    for j in range(1, 50):
        x = x + reverse(x)
        if x == reverse(x):
            flag = True
            break
    if not flag:
        print(i)
        result = result + 1
print(result)
