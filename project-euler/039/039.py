#!/usr/bin/python3
import itertools
import fractions

r = (0, 0)
N = 1000
for i in range(3, N + 1):
    wys = 0
    for j in range(1, i):
        for k in range(j, i):
            if j * j + k * k == (i - j - k) ** 2:
                wys = wys + 1
    if wys > r[0]:
        r = (wys, i)
    print(i, wys)
print(r)
