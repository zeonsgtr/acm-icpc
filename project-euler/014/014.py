#!/usr/bin/python3
a = 0
b = 0
for i in range(1, 1000000):
    s = 0
    j = i
    while j > 1:
        if j & 1 == 1:
            j = j * 3 + 1
        else:
            j = j // 2
        s = s + 1
    if s > a:
        a = s
        b = i
    print(i)
print(b)
