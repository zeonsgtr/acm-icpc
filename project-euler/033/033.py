#!/usr/bin/python3
import itertools
import fractions

EPS = 1e-12
numerator = 1
denominator = 1
for a in range(10, 100):
    if a % 10 == 0:
        continue
    for b in range(a, 100):
        if b % 10 == 0:
            continue
        la = list(str(a))
        lb = list(str(b))
        p, q = -1, -1
        if la[0] == lb[0]:
            p, q = 0, 0
        elif la[0] == lb[1]:
            p, q = 0, 1
        elif la[1] == lb[0]:
            p, q = 1, 0
        elif la[1] == lb[1]:
            p, q = 1, 1
        if (p != -1 and q != -1 and la[1 - p] != lb[1 - q]
            and abs(a / b - int(la[1 - p]) / int(lb[1 - q])) < EPS):
            print(a, b)
            numerator = numerator * a
            denominator = denominator * b
print(numerator, '/', denominator, end = ' = ')
g = fractions.gcd(numerator, denominator)
print(numerator // g, '/', denominator // g)
