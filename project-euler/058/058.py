#!/usr/bin/python3
import itertools
import fractions

def is_prime(x):
    if x < 2:
        return False
    i = 2
    while i * i <= x:
        if x % i == 0:
            return False
        i = i + 1
    return True

prime_number = 0
total_number = 1
side_length = 1
add = 0
now = 1
while True:
    side_length = side_length + 1
    total_number = total_number + 4
    add = add + 2
    for i in range(0, 4):
        now = now + add
        if is_prime(now):
            prime_number = prime_number + 1
    if prime_number * 10 < total_number:
        print(side_length * 2 - 1)
        break
    print(side_length, now, prime_number, total_number)
