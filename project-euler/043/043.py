#!/usr/bin/python3
import itertools
import fractions

l = list(itertools.permutations([i for i in range(0, 10)]))
r = 0
mod = [2, 3, 5, 7, 11, 13, 17]
for a in l:
    flag = True
    for i in range(len(mod)):
        x = a[i + 1] * 100 + a[i + 2] * 10 + a[i + 3]
        if x % mod[i] != 0:
            flag = False
            break
    if flag:
        x = 0
        for i in range(len(a)):
            x = x * 10 + a[i]
        r = r + x
        print(a, x)
print(r)
