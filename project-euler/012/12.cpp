#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <sstream>
#include <bitset>
#include <numeric>
#include <climits>
#include <string>
#include <cctype>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <vector>
#include <queue>
#include <list>
#include <map>
#include <set>
using namespace std;

#define foreach(e, x) for (__typeof((x).begin()) e = (x).begin(); e != (x).end(); ++e)

typedef long long int64; 

int main() {
	int64 x = 0;
	for (int i = 1; ; i++) {
		x += i;
		int64 numFactors = 1;
		int64 y = x;
		for (int64 j = 2; j * j <= x; j++) {
			int cnt = 0;
			for (; x % j == 0; x /= j, cnt++);
			numFactors *= cnt + 1;
		}
		if (x > 1) {
			numFactors *= 2;
		}
		if (numFactors > 500) {
			cout << y << endl;
			break;
		}
		x = y;
	}
}

