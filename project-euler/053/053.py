#!/usr/bin/python3
import itertools

n = 100
C = [([0] * (n + 1)) for i in range(0, n + 1)]
s = 0
u = 10 ** 6
for i in range(0, n + 1):
    C[i][0] = 1
    for j in range(1, i + 1):
        C[i][j] = C[i - 1][j] + C[i - 1][j - 1]
        if C[i][j] > u:
            s = s + 1
print(s)
