#!/usr/bin/python3
C = [([0] * 41) for i in range(41)]
for i in range(41):
    C[i][0] = 1
    for j in range(1, i + 1):
        C[i][j] = C[i - 1][j] + C[i - 1][j - 1]
print(C[40][20])
