#!/usr/bin/python3
a = []
while True:
    x = list(map(int, input().split()))
    if len(x) == 0:
        break
    a.append(x)
n = len(a)
dp = [([0] * n) for i in range(n)]
for i in range(n):
    dp[n - 1][i] = a[n - 1][i]
for i in range(n - 2, -1, -1):
    print(i)
    for j in range(0, i + 1):
        dp[i][j] = max(dp[i + 1][j], dp[i + 1][j + 1]) + a[i][j]
    print()
print(dp[0][0])
