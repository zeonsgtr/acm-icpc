#!/usr/bin/python3
import itertools
import fractions

def is_prime(x):
    for i in range(2, x):
        if x % i == 0:
            return False
        if i * i > x:
            break
    return True

def check(x):
    l = list(str(x))
    l.sort()
    for i in range(len(l)):
        if i + 1 != int(l[i]):
            return False
    return True

n = 10 ** 9
m = 10 ** 5
for i in range(2, n):
    if i % m == 0:
        print(i, r)
    if check(i) and is_prime(i):
        r = i
        print(i)
print(r)
