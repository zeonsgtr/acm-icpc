#!/usr/bin/python3
import itertools
import fractions

l = [str(i) for i in range(1, 10)]
perms = list(itertools.permutations(l))
n = 10 ** 5
r = 0
for i in range(1, n):
    l = []
    j = 1
    while len(l) < 9:
        l = l + list(str(i * j))
        if tuple(l) in perms:
            print("".join(l))
            r = max(r, int("".join(l)))
        j = j + 1
    #print(i)
print(r)
