#!/usr/bin/python3
n = 201
dp = [0] * n
dp[0] = 1
a = [1, 2, 5, 10, 20, 50, 100, 200]
for delta in a:
    for i in range(1, n):
        if i - delta >= 0:
            dp[i] = dp[i] + dp[i - delta]
print(dp[-1])
