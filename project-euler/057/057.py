#!/usr/bin/python3
import itertools
import fractions

result = 0
n = 1000
for i in range(1, n + 1):
    numerator = 1
    denominator = 2
    for j in range(1, i):
        numerator = numerator + 2 * denominator
        numerator, denominator = denominator, numerator
        g = fractions.gcd(numerator, denominator)
        numerator = numerator // g
        denominator = denominator // g
    numerator = numerator + denominator
    g = fractions.gcd(numerator, denominator)
    numerator = numerator // g
    denominator = denominator // g
    if len(str(numerator)) > len(str(denominator)):
        result = result + 1
        print(i, numerator, denominator)
    #print(i, numerator, denominator)
print(result)
