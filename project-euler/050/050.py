#!/usr/bin/python3
import itertools

n = 10 ** 6
v = [True for i in range(0, n + 1)]
v[0] = v[1] = False
primes = []
for i in range(2, n + 1):
    if v[i]:
        primes.append(i)
        j = i * i
        while j <= n:
            v[j] = False
            j = j + i
which = 0
length = 0
for i in range(len(primes)):
    s = 0
    for j in range(i, len(primes)):
        s = s + primes[j]
        if s > n:
            break
        if v[s] and j - i + 1 > length:
            length = j - i + 1
            which = s
print(which, length)
