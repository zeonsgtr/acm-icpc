#!/usr/bin/python3
import itertools

s = 0
mod = 10 ** 10
for i in range(1, 1001):
    s = (s + pow(i, i, mod)) % mod
print(s)
