#!/usr/bin/python3
import itertools

n = 10 ** 6
v = [True for i in range(0, n + 1)]
v[0] = v[1] = False
primes = []
for i in range(2, n + 1):
    if v[i]:
        primes.append(i)
        j = i * i
        while j <= n:
            v[j] = False
            j = j + i
        print('primes', i)

for p in primes:
    l = list(str(p))
    m = 2 ** len(l)
    flag = False
    for msk in range(1, m):
        cnt = 0
        same = True
        digit = -1
        for i in range(0, len(l)):
            if (msk >> i & 1) > 0:
                if digit == -1:
                    digit = int(l[i])
                elif digit != int(l[i]):
                    same = False
                    break
        if not same:
            continue
        for j in range(0, 10):
            s = 0
            for i in range(0, len(l)):
                if (msk >> i & 1) > 0:
                    s = s * 10 + j
                else:
                    s = s * 10 + int(l[i])
            if v[s] and len(str(s)) == len(l):
                cnt = cnt + 1
        if cnt == 8:
            flag = True
            print(p)
            cnt = 0
            '''
            for j in range(0, 10):
                s = 0
                for i in range(0, len(l)):
                    if (msk >> i & 1) > 0:
                        s = s * 10 + j
                    else:
                        s = s * 10 + int(l[i])
                if v[s] and len(str(s)) == len(l):
                    cnt = cnt + 1
                    print(cnt, '=', s)
            '''
            break
    if flag:
        break
    print(p)
