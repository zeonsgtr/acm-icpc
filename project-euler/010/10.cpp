#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <sstream>
#include <bitset>
#include <numeric>
#include <climits>
#include <string>
#include <cctype>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <vector>
#include <queue>
#include <list>
#include <map>
#include <set>
using namespace std;

#define foreach(e, x) for (__typeof((x).begin()) e = (x).begin(); e != (x).end(); ++e)

typedef long long int64; 

int main() {
	const int n = 2 * int(1e6);
	bitset <n + 10> v;
	v.set();
	int64 result = 0;
	for (int i = 2; i <= n; i++) {
		if (v[i]) {
			result += i;
			if (1LL * i * i <= n) {
				for (int j = i * i; j <= n; j += i) {
					v[j] = false;
				}
			}
		}
	}
	cout << result << endl;
}

