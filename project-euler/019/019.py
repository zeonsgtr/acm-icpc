#!/usr/bin/python3
s = 0
for i in range(1901, 2001):
    for j in range(12):
        m = j + 1
        if m < 3:
            m = m + 12
        w = (i + (i // 4) + (20 // 4) - 2 * 20 + (26 * (m + 1) // 10) + 0) % 7
        if w == 0:
            s = s + 1
print(s)
