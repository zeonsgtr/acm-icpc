#!/usr/bin/python3
num = set()
for i in range(2, 28124):
    s = 1
    j = 2
    while j * j <= i:
        if i % j == 0:
            s = s + j
            if j * j < i:
                s = s + i // j
        j = j + 1
    if s > i:
        num.add(i)
result = 1 + 2
for i in range(3, 28124):
    flag = False
    for x in num:
        if i - x in num:
            flag = True
            break
    if not flag:
        result = result + i
print(result)
