#!/usr/bin/python3
import itertools
import fractions

def is_prime(x):
    if x < 2:
        return False
    for i in range(2, x):
        if i * i > x:
            break
        if x % i == 0:
            return False
    return True

r = 0
n = 10 ** 6
for i in range(10, n):
    if is_prime(i):
        l = str(i)
        s = 0
        flag = True
        for j in range(len(l)):
            s = s * 10 + int(l[j])
            if not is_prime(s):
                flag = False
                break
        if not flag:
            continue
        l = l[::-1]
        s = 0
        for j in range(len(l)):
            s = s + int(l[j]) * 10 ** j
            if not is_prime(s):
                flag = False
                break
        if flag:
            print(i)
            r = r + i
print(r)
