#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <sstream>
#include <bitset>
#include <numeric>
#include <climits>
#include <string>
#include <cctype>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <vector>
#include <queue>
#include <list>
#include <map>
#include <set>
using namespace std;

#define foreach(e, x) for (__typeof((x).begin()) e = (x).begin(); e != (x).end(); ++e)

typedef long long int64; 

namespace Euler {
	template <class T> 
	string toString(T x) {
		ostringstream sout;
		sout << x;
		return sout.str();
	}

	bool isPalindrome(const string &s) {
		for (int i = 0; i + i <= (int)s.size(); i++) {
			if (s[i] != s[(int)s.size() - i - 1]) {
				return false;
			}
		}
		return true;
	}

	template <class T>
	bool isPrime(T x) {
		if (x == 2) {
			return true;
		}
		if (x < 2 || (~ x & 1)) {
			return false;
		}
		for (T i = 3; i * i <= x; i++) {
			if (x % i == 0) {
				return false;
			}
		}
		return true;
	}
}

int main() {
	int cnt = 10001, i;
	for (i = 2; cnt > 0; i++) {
		if (Euler::isPrime(i)) {
			cnt--;
		}
	}
	cout << i - 1 << endl;
}

