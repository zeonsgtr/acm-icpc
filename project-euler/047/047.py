#!/usr/bin/python3
import itertools

def count_factors(x):
    i = 2
    ret = 0
    while i * i <= x:
        if x % i == 0:
            ret = ret + 1
            while x % i == 0:
                x = x // i
        i = i + 1
    if x > 1:
        ret = ret + 1
    return ret

def print_factors(x):
    i = 2
    print(x)
    while i * i <= x:
        if x % i == 0:
            e = 0
            while x % i == 0:
                x = x // i
                e = e + 1
            print('(', i, ',', e, ')', end = ' ')
        i = i + 1
    if x > 1:
        print('(', x, ',', 1, ')')

i = 2
while True:
    print(i)
    if count_factors(i) != 4:
        i = i + 1
    elif count_factors(i + 1) != 4:
        i = i + 2
    elif count_factors(i + 2) != 4:
        i = i + 3
    elif count_factors(i + 3) != 4:
        i = i + 4
    else:
        print_factors(i)
        print_factors(i + 1)
        print_factors(i + 2)
        print_factors(i + 3)
        print(i)
        break
