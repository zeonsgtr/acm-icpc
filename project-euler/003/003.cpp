#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <sstream>
#include <bitset>
#include <numeric>
#include <climits>
#include <string>
#include <cctype>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <vector>
#include <queue>
#include <list>
#include <map>
#include <set>
using namespace std;

#define foreach(e, x) for (__typeof((x).begin()) e = (x).begin(); e != (x).end(); ++e)

typedef long long int64; 

int main() {
	int64 x = 600851475143LL;
	int64 result = 0;
	for (int64 i = 2; i * i <= x; i++) {
		if (x % i == 0) {
			result = i;
			for (; x % i == 0; x /= i);
		}
	}
	cout << max(x, result) << endl;
}

