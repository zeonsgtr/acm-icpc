#!/usr/bin/python3
import itertools
import fractions

n = 10 ** 5
T = [i * (i + 1) // 2 for i in range(1, n)]
P = [i * (3 * i - 1) // 2 for i in range(1, n)]
P = set(P)
H = [i * (2 * i - 1) for i in range(1, n)]
H = set(H)
for i in range(285, n - 1):
    if T[i] in P and T[i] in H:
        print(T[i])
        break
