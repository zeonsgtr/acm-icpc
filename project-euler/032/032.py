#!/usr/bin/python3
import itertools

a = []
for i in range(1, 10):
    a.append(i)
l = list(itertools.permutations(a))
r = 0
num = set()
for perm in l:
    for i in range(1, 8):
        a = 0
        for k in range(0, i):
            a = a * 10 + perm[k]
        for j in range(i + 1, 8):
            b = 0
            for k in range(i, j):
                b = b * 10 + perm[k]
            c = 0
            for k in range(j, 9):
                c = c * 10 + perm[k]
            if a * b == c and not c in num:
                num.add(c)
                print(c)
                r = r + c
print(r)
