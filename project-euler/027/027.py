#!/usr/bin/python3
import itertools

def is_prime(x):
    if x < 2:
        return False
    i = 2
    while i * i <= x:
        if x % i == 0:
            return False
        i = i + 1
    return True

length = result = 0
for a in range(-999, 1000):
    for b in range(-999, 1000):
        n = 0
        while True:
            if is_prime(n * n + a * n + b):
                n = n + 1
            else:
                break
        if n > length:
            length = n
            result = a * b
print(result)
