#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <sstream>
#include <bitset>
#include <numeric>
#include <climits>
#include <string>
#include <cctype>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <vector>
#include <queue>
#include <list>
#include <map>
#include <set>
using namespace std;

#define foreach(e, x) for (__typeof((x).begin()) e = (x).begin(); e != (x).end(); ++e)

typedef long long int64; 

int main() {
	int upper = 4000000;
	vector <int> f;
	f.push_back(1);
	f.push_back(2);
	int64 result = 2;
	while (true) {
		int s = f.size();
		int x = f[s - 1] + f[s - 2];
		if (x <= upper) {
			f.push_back(x);
			if (~ x & 1) {
				result += x;
			}
		} else {
			break;
		}
	}
	cout << result << endl;
}

