#!/usr/bin/python3
import itertools

result = 0
project = 0
for i in range(2, 1000):
    now = 1
    while now < i:
        now = now * 10
    now = now // 10
    remainders = {}
    j = 0
    while True:
        j = j + 1
        now = now * 10 % i
        if now in remainders:
            if j - remainders[now] > result:
                result = j - remainders[now]
                project = i
            break
        else:
            remainders[now] = j
print(result, project)
