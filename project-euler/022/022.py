#!/usr/bin/python3
in_file = open('names.txt', 'r')
s = in_file.readline().split(',')
for i in range(len(s)):
    s[i] = s[i][1 : len(s[i]) - 1]
s.sort()
r = 0
for i in range(len(s)):
    t = 0
    for j in range(len(s[i])):
        t = t + ord(s[i][j]) - ord('A') + 1
    r = r + t * (i + 1)
print(r)
