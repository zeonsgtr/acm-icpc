#!/usr/bin/python3
import itertools
import fractions

s = ""
n = 10 ** 6
now = 1
while len(s) < n:
    s = s + str(now)
    now = now + 1
r = 1
while n > 0:
    r = r * int(s[n - 1])
    n = n // 10
print(r)
