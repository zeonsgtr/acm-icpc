#!/usr/bin/python3
d = [0, 0]
for i in range(2, 10000):
    j = 1
    x = 0
    while j * j <= i:
        if i % j == 0:
            x = x + j
            if j * j != i and j != 1:
                x = x + i // j
        j = j + 1
    d.append(x)
s = 0
for i in range(2, 10000):
    if 2 <= d[i] and d[i] < 10000 and d[d[i]] == i and d[i] != i:
        s = s + i
print(s)
