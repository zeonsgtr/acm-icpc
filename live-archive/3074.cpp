#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define SIZE(x) ((int)(x).size())

const int N = 5;
const int RANK[14] = {0, 13, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};

enum PokerHand {
    royal_flush = 0, straight_flush = 1, four_of_a_kind = 2, full_house = 3,
    flush = 4, straight = 5, three_of_a_kind = 6, other = 7
};

struct Card {
    int number, suit;
    Card() {}
    Card(int a, int b) {
        number = a == 'A' ? 1 : a == 'T' ? 10 : a == 'J' ? 11 : a == 'Q' ? 12 : a == 'K' ? 13 : a - '0';
        suit = b == 'c' ? 0 : b == 'd' ? 1 : b == 'h' ? 2 : 3;
    }
};

bool card_number_greater(const Card& a, const Card &b) {
    return RANK[a.number] > RANK[b.number];
}

bool compare(const Card& a, const Card& b) {
    if (card_number_greater(a, b) || card_number_greater(b, a)) {
        return card_number_greater(a, b);
    }
    if (a.suit == '*') {
        return b.suit != '*';
    }
    if (b.suit == '*') {
        return false;
    }
    return a.suit > b.suit;
}

bool is_flush(Card a[]) {
    std::sort(a, a + N, card_number_greater);
    for (int i = 1; i < N; i++) {
        if (a[i].suit != a[i - 1].suit) {
            return false;
        }
    }
    return true;
}

bool is_straight(Card a[]) {
    std::sort(a, a + N, card_number_greater);
    if (a[0].number == 1 && a[1].number == 5 && a[2].number == 4 && a[3].number == 3 && a[4].number == 2) {
        std::rotate(a, a + 1, a + N);
        return true;
    }
    for (int i = 0; i < N - 1; i++) {
        if (RANK[a[i].number] != RANK[a[i + 1].number] + 1) {
            return false;
        }
    }
    return true;
}

bool is_straight_flush(Card a[]) {
    std::sort(a, a + N, card_number_greater);
    return is_flush(a) && is_straight(a);
}

bool is_royal_flush(Card a[]) { 
    std::sort(a, a + N, card_number_greater);
    return is_straight_flush(a) && a[0].number == 1;
}

bool same(Card a[], int l, int r) {
    for (int i = l + 1; i < r; i++) {
        if (a[i].number != a[i - 1].number) {
            return false;
        }
    }
    return true;
}

bool is_four_of_a_kind(Card a[]) {
    std::sort(a, a + N, card_number_greater);
    if (same(a, 1, N)) {
        std::rotate(a, a + 1, a + N);
    }
    return same(a, 0, N - 1);
}

bool is_full_house(Card a[]) {
    std::sort(a, a + N, card_number_greater);
    if (same(a, 0, 2) && same(a, 2, N)) {
        std::rotate(a, a + 2, a + N);
    }
    return same(a, 0, 3) && same(a, 3, N);
}

bool is_three_of_a_hand(Card a[]) {
    std::sort(a, a + N, card_number_greater);
    if (same(a, 2, N)) {
        std::rotate(a, a + 2, a + N);
    }
    if (same(a, 1, N - 1)) {
        std::rotate(a, a + 1, a + 4);
    }
    return same(a, 0, 3);
}

struct Hand {
    Hand() {}
    Card a[5];
    const Card& operator [](int x) const {
        return a[x];
    }
    Card& operator [](int x) {
        return a[x];
    }
    PokerHand id;
    Hand(Card t0, Card t1, Card t2, Card t3, Card t4) {
        a[0] = t0, a[1] = t1, a[2] = t2, a[3] = t3, a[4] = t4;
        if (is_royal_flush(a)) {
            id = royal_flush;
        } else if (is_straight_flush(a)) {
            id = straight_flush;
        } else if (is_four_of_a_kind(a)) {
            id = four_of_a_kind;
        } else if (is_full_house(a)) {
            id = full_house;
        } else if (is_flush(a)) {
            id = flush;
        } else if (is_straight(a)) {
            id = straight;
        } else if (is_three_of_a_hand(a)) {
            id = three_of_a_kind;
        } else {
            id = other;
        }
    }
};

bool hand_greater(const Hand& a, const Hand& b) {
    if (a.id != b.id) {
        return a.id < b.id;
    } else if (a.id == royal_flush) {
        return false;
    } else if (a.id == straight_flush) {
        return card_number_greater(a[0], b[0]);
    } else if (a.id == four_of_a_kind) {
        if (card_number_greater(a[0], b[0]) || card_number_greater(b[0], a[0])) {
            return card_number_greater(a[0], b[0]);
        }
        return card_number_greater(a[N - 1], b[N - 1]);
    } else if (a.id == full_house) {
        if (card_number_greater(a[0], b[0]) || card_number_greater(b[0], a[0])) {
            return card_number_greater(a[0], b[0]);
        }
        return card_number_greater(a[3], b[3]);
    } else if (a.id == flush) {
        for (int i = 0; i < N; i++) {
            if (card_number_greater(a[i], b[i]) || card_number_greater(b[i], a[i])) {
                return card_number_greater(a[i], b[i]);
            }
        }
        return false;
    } else if (a.id == straight) {
        return card_number_greater(a[0], b[0]);
    } else if (a.id == three_of_a_kind) {
        if (card_number_greater(a[0], b[0]) || card_number_greater(b[0], a[0])) {
            return card_number_greater(a[0], b[0]);
        }
        if (card_number_greater(a[3], b[3]) || card_number_greater(b[3], a[3])) {
            return card_number_greater(a[3], b[3]);
        }
        return card_number_greater(a[4], b[4]);
    }
    return false;
}

char buf[11];
bool used[14][4];
std::vector <Card> choose;
std::vector <Hand> all;
Card a[5];
Hand ans;

void dfs(int k) {
    if (k == 2) {
        choose.push_back(a[0]);
        choose.push_back(a[1]);
        for (int i1 = 0; i1 < 7; i1++) {
            for (int i2 = i1 + 1; i2 < 7; i2++) {
                for (int i3 = i2 + 1; i3 < 7; i3++) {
                    for (int i4 = i3 + 1; i4 < 7; i4++) {
                        for (int i5 = i4 + 1; i5 < 7; i5++) {
                            Hand h(choose[i1], choose[i2], choose[i3], choose[i4], choose[i5]);
                            if (!all.empty()) {
                                if (hand_greater(h, all[0])) {
                                    all.clear();
                                }
                            }
                            if (all.empty() || !hand_greater(all[0], h)) {
                                all.push_back(h);
                            }
                        }
                    }
                }
            }
        }
        choose.pop_back();
        choose.pop_back();
        return;
    }
    for (int i = 1; i <= 13; i++) {
        for (int j = 0; j < 4; j++) {
            if (!used[i][j]) {
                a[k].number = i, a[k].suit = j;
                used[i][j] = true;
                dfs(k + 1);
                used[i][j] = false;
            }
        }
    }
}

void merge(Hand& a, const Hand& b) {
    for (int i = 0; i < N; i++) {
        if (a[i].suit != b[i].suit) {
            a[i].suit = '*';
        }
    }
}

int main() {
    int test_count;
    scanf("%d", &test_count);
    while (test_count--) {
        memset(used, 0, sizeof(used));
        choose.clear();
        for (int i = 0; i < 5; i++) {
            scanf("%s", buf);
            Card c(buf[0], buf[1]);
            choose.push_back(c);
            used[c.number][c.suit] = true;
        }
        for (int i = 0; i < 2; i++) {
            scanf("%s", buf);
            Card c(buf[0], buf[1]);
            used[c.number][c.suit] = true;
        }
        all.clear();
        dfs(0);
        for (int i = 0; i < SIZE(all); i++) {
            std::sort(all[i].a, all[i].a + N, compare);
        }
        ans = all[0];
        for (int i = 1; i < SIZE(all); i++) {
            merge(ans, all[i]);
        }
        for (int i = 0; i < N; i++) {
            putchar(ans[i].number == 1 ? 'A' : ans[i].number == 10 ? 'T' : ans[i].number == 11 ? 'J' : 
                    ans[i].number == 12 ? 'Q' : ans[i].number == 13 ? 'K' : '0' + ans[i].number);
            putchar(ans[i].suit == 0 ? 'c' : ans[i].suit == 1 ? 'd' : ans[i].suit == 2 ? 'h' :
                    ans[i].suit == 3 ? 's' : '*');
            putchar(' ');
        }
        std::sort(ans.a, ans.a + 5, compare);
        puts(ans.id == royal_flush ? "ROYAL FLUSH" : ans.id == straight_flush ? "STRAIGHT FLUSH" :
                ans.id == four_of_a_kind ? "FOUR OF A KIND" : ans.id == full_house ? "FULL HOUSE" :
                ans.id == flush ? "FLUSH" : ans.id == straight ? "STRAIGHT" :
                ans.id == three_of_a_kind ? "THREE OF A KIND" : "");
    }
}

