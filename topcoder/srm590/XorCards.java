import java.util.*;

public class XorCards {
    long powers(int a, int b) {
        if (b == 0) {
            return 1L;
        }
        long tmp = powers(a, b >> 1);
        return tmp * tmp * ((b & 1) > 0 ? a : 1);
    }
    long calc(int s, int t, long[] b, long limit) {
        int i = s, j = 0, n = b.length, cnt = 0;
        long[] a = new long[N + 1];
        for (int k = s; k <= t; k++) {
            a[k] = (limit >> k & 1L) << n;
            for (int l = 0; l < n; l++) {
                a[k] |= (b[l] >> k & 1L) << l;
            }
        }
        while (i <= t && j < n) {
            if ((a[i] >> j & 1) == 0) {
                for (int k = i + 1; k <= t; k++) {
                    if ((a[k] >> j & 1) == 1) {
                        long tmp = a[i];
                        a[i] = a[k];
                        a[k] = tmp;
                        break;
                    }
                }
            }
            if ((a[i] >> j & 1) == 0) {
                cnt++;
                j++;
                continue;
            }
            for (int k = i + 1; k <= t; k++) {
                if ((a[k] >> j & 1) == 1) {
                    a[k] ^= a[i];
                }
            }
            i++;
            j++;
        }
        for (int k = i; k <= t; k++) {
            if ((a[k] >> n & 1) == 1) {
                return 0L;
            }
        }
        return powers(2, n - j + cnt);
    }
    final int N = 50;
    public long numberOfWays(long[] number, long limit) {
        long[] count = new long[N + 1];
        long result = 0;
        for (int i = 0; i <= N; i++) {
            count[i] = calc(i, N, number.clone(), limit);
            if (i == 0) {
                result += count[i];
            } else {
                if ((limit >> i - 1 & 1) == 1) {
                    result += count[i] - count[i - 1];
                }
            }
        }
        return result;
    }

//{{{
// BEGIN CUT HERE
   public static void main(String[] args) {
		if (args.length == 0) {
			XorCardsHarness.run_test(-1);
		} else {
			for (int i=0; i<args.length; ++i)
				XorCardsHarness.run_test(Integer.valueOf(args[i]));
		}
	}
// END CUT HERE
}

// BEGIN CUT HERE
class XorCardsHarness {
	public static void run_test(int casenum) {
		if (casenum != -1) {
			if (runTestCase(casenum) == -1)
				System.err.println("Illegal input! Test case " + casenum + " does not exist.");
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = runTestCase(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			System.err.println("No test cases run.");
		} else if (correct < total) {
			System.err.println("Some cases FAILED (passed " + correct + " of " + total + ").");
		} else {
			System.err.println("All " + total + " tests passed!");
		}
	}
	
	static boolean compareOutput(long expected, long result) { return expected == result; }
	static String formatResult(long res) {
		return String.format("%d", res);
	}
	
	static int verifyCase(int casenum, long expected, long received) { 
		System.err.print("Example " + casenum + "... ");
		if (compareOutput(expected, received)) {
			System.err.println("PASSED");
			return 1;
		} else {
			System.err.println("FAILED");
			System.err.println("    Expected: " + formatResult(expected)); 
			System.err.println("    Received: " + formatResult(received)); 
			return 0;
		}
	}

	static int runTestCase(int casenum) {
		switch(casenum) {
		case 0: {
			long[] number             = {1,2};
			long limit                = 2L;
			long expected__           = 3L;

			return verifyCase(casenum, expected__, new XorCards().numberOfWays(number, limit));
		}
		case 1: {
			long[] number             = {5,5};
			long limit                = 3L;
			long expected__           = 2L;

			return verifyCase(casenum, expected__, new XorCards().numberOfWays(number, limit));
		}
		case 2: {
			long[] number             = {1,2,3,4,5,6,7};
			long limit                = 5L;
			long expected__           = 96L;

			return verifyCase(casenum, expected__, new XorCards().numberOfWays(number, limit));
		}
		case 3: {
			long[] number             = {123, 456, 789, 147, 258, 369, 159, 357};
			long limit                = 500L;
			long expected__           = 125L;

			return verifyCase(casenum, expected__, new XorCards().numberOfWays(number, limit));
		}
		case 4: {
			long[] number             = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
			long limit                = 1000000000000000L;
			long expected__           = 4294967296L;

			return verifyCase(casenum, expected__, new XorCards().numberOfWays(number, limit));
		}
		case 5: {
			long[] number             = {1000000000000000L, 999999999999999L};
			long limit                = 65535L;
			long expected__           = 2L;

			return verifyCase(casenum, expected__, new XorCards().numberOfWays(number, limit));
		}

		// custom cases

        case 6: {
			long[] number             = {5L,3L};
			long limit                = 6L;
			long expected__           = 4;

			return verifyCase(casenum, expected__, new XorCards().numberOfWays(number, limit));
		}  
/*      case 7: {
			long[] number             = ;
			long limit                = L;
			long expected__           = L;

			return verifyCase(casenum, expected__, new XorCards().numberOfWays(number, limit));
		}*/
/*      case 8: {
			long[] number             = ;
			long limit                = L;
			long expected__           = L;

			return verifyCase(casenum, expected__, new XorCards().numberOfWays(number, limit));
		}*/
		default:
			return -1;
		}
	}
}

// END CUT HERE
//}}}
