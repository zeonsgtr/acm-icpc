import java.util.*;
import java.math.*;

public class GUMIAndSongsDiv1 {
    public int maxSongs(int[] duration, int[] tone, int T) {
        int n = tone.length;
        int result = 0;
        for (int i = 0; i < n; i++) {
            if (duration[i] <= T) {
                result = Math.max(result, 1);
            }
            for (int j = 0; j < n; j++) {
                if (i != j && tone[i] >= tone[j]) {
                    int t = T - (tone[i] - tone[j]) - duration[i] - duration[j];
                    if (t >= 0) {
                        int count = 2, m = 0;
                        int[] a = new int[n];
                        for (int k = 0; k < n; k++) {
                            if (k != i && k != j && tone[i] >= tone[k] && tone[k] >= tone[j]) {
                                a[m++] = duration[k];
                            }
                        }
                        Arrays.sort(a, 0, m);
                        for (int k = 0; k < m && t - a[k] >= 0; t -= a[k++], count++);
                        result = Math.max(result, count);
                    }
                }
            }
        }
        return result;
    }
    void debug(Object...os) {
        System.err.println(Arrays.deepToString(os));
    }
// BEGIN CUT HERE
//{{{
   public static void main(String[] args) {
		if (args.length == 0) {
			GUMIAndSongsDiv1Harness.run_test(-1);
		} else {
			for (int i=0; i<args.length; ++i)
				GUMIAndSongsDiv1Harness.run_test(Integer.valueOf(args[i]));
		}
	}
// END CUT HERE
}

// BEGIN CUT HERE
class GUMIAndSongsDiv1Harness {
	public static void run_test(int casenum) {
		if (casenum != -1) {
			if (runTestCase(casenum) == -1)
				System.err.println("Illegal input! Test case " + casenum + " does not exist.");
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = runTestCase(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			System.err.println("No test cases run.");
		} else if (correct < total) {
			System.err.println("Some cases FAILED (passed " + correct + " of " + total + ").");
		} else {
			System.err.println("All " + total + " tests passed!");
		}
	}
	
	static boolean compareOutput(int expected, int result) { return expected == result; }
	static String formatResult(int res) {
		return String.format("%d", res);
	}
	
	static int verifyCase(int casenum, int expected, int received) { 
		System.err.print("Example " + casenum + "... ");
		if (compareOutput(expected, received)) {
			System.err.println("PASSED");
			return 1;
		} else {
			System.err.println("FAILED");
			System.err.println("    Expected: " + formatResult(expected)); 
			System.err.println("    Received: " + formatResult(received)); 
			return 0;
		}
	}

	static int runTestCase(int casenum) {
		switch(casenum) {
		case 0: {
			int[] duration            = {3, 5, 4, 11};
			int[] tone                = {2, 1, 3, 1};
			int T                     = 17;
			int expected__            = 3;

			return verifyCase(casenum, expected__, new GUMIAndSongsDiv1().maxSongs(duration, tone, T));
		}
		case 1: {
			int[] duration            = {100, 200, 300};
			int[] tone                = {1, 2, 3};
			int T                     = 99;
			int expected__            = 0;

			return verifyCase(casenum, expected__, new GUMIAndSongsDiv1().maxSongs(duration, tone, T));
		}
		case 2: {
			int[] duration            = {1, 2, 3, 4};
			int[] tone                = {1, 1, 1, 1};
			int T                     = 100;
			int expected__            = 4;

			return verifyCase(casenum, expected__, new GUMIAndSongsDiv1().maxSongs(duration, tone, T));
		}
		case 3: {
			int[] duration            = {9, 11, 13, 17};
			int[] tone                = {2, 1, 3, 4};
			int T                     = 20;
			int expected__            = 1;

			return verifyCase(casenum, expected__, new GUMIAndSongsDiv1().maxSongs(duration, tone, T));
		}
		case 4: {
			int[] duration            = {87,21,20,73,97,57,12,80,86,97,98,85,41,12,89,15,41,17,68,37,21,1,9,65,4,  67,38,91,46,82,7,98,21,70,99,41,21,65,11,1,8,12,77,62,52,69,56,33,98,97};
			int[] tone                = {88,27,89,2,96,32,4,93,89,50,58,70,15,48,31,2,27,20,31,3,23,86,69,12,59,  61,85,67,77,34,29,3,75,42,50,37,56,45,51,68,89,17,4,47,9,14,29,59,43,3};
			int T                     = 212;
			int expected__            = 12;

			return verifyCase(casenum, expected__, new GUMIAndSongsDiv1().maxSongs(duration, tone, T));
		}

		// custom cases

/*      case 5: {
			int[] duration            = ;
			int[] tone                = ;
			int T                     = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new GUMIAndSongsDiv1().maxSongs(duration, tone, T));
		}*/
/*      case 6: {
			int[] duration            = ;
			int[] tone                = ;
			int T                     = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new GUMIAndSongsDiv1().maxSongs(duration, tone, T));
		}*/
/*      case 7: {
			int[] duration            = ;
			int[] tone                = ;
			int T                     = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new GUMIAndSongsDiv1().maxSongs(duration, tone, T));
		}*/
		default:
			return -1;
		}
	}
}

//}}}
// END CUT HERE
