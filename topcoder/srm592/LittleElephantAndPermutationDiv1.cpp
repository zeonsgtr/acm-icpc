 #include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <sstream>
#include <numeric>
#include <climits>
#include <string>
#include <cctype>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <vector>
#include <queue>
#include <list>
#include <map>
#include <set>
using namespace std;
 
#define foreach(e, x) for (__typeof((x).begin()) e = (x).begin(); e != (x).end(); ++e)
 
typedef long long int64; 
 
const int N = 55, M = N * N;
const int MOD = int(1e9) + 7;
 
int f[N][N][M];
 
inline void normalize(int& x) {
  if (x >= MOD) {
    x -= MOD;
  }
}
 
struct LittleElephantAndPermutationDiv1 {
  int getNumber(int n, int m) {
    memset(f, 0, sizeof(f));
    f[0][0][0] = 1;
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        for (int k = 0; k <= 2500; k++) {
          int value = f[i][j][k];
          if (value == 0) {
            continue;
          }
          normalize(f[i][j + 1][k] += value);
          normalize(f[i + 1][j + 1][k + j + 1] +=
            1LL * value * (n - i) % MOD);
          int left = 2 * j - 2 * i;
          if (left > 0) {
            normalize(f[i + 1][j + 1][k + j + 1] +=
              1LL * value * (n - i) % MOD * left % MOD);
          }
          if (left >= 2) {
            normalize(f[i + 2][j + 1][k + j + 1 + j + 1] +=
              1LL * value * (n - i) % MOD * (n - i - 1) % MOD
              * (left / 2) % MOD * (left / 2) % MOD);
          }
        }
      }
    }
    int ret = 0;
    for (int k = m; k <= 2500; k++) {
      normalize(ret += f[n][n][k]);
    }
    return ret;
  }
};
