#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <sstream>
#include <numeric>
#include <climits>
#include <string>
#include <cctype>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <vector>
#include <queue>
#include <list>
#include <map>
#include <set>
using namespace std;
 
#define foreach(e, x) for (__typeof((x).begin()) e = (x).begin(); e != (x).end(); ++e)
 
typedef long long int64; 
 
int calc(const vector <char>& v, int l, int r) {
  if (l >= (int)v.size()) {
    return 0;
  }
  int cover = 0;
  for (int i = l; i < r; i++) {
    cover |= 1 << (v[i] == 'R' ? 0 : v[i] == 'G' ? 1 : 2);
  }
  int ret = 0;
  for (int i = 0; i < 3; i++) {
    ret += cover >> i & 1;
  }
  return ret;
}
 
struct LittleElephantAndBalls {
  int getNumber(string S) {
    vector <char> v;
    int ret = 0;
    for (int j = 0; j < (int)S.size(); j++) {
      char x = S[j];
      int p = -1, max_points = 0;
      for (int i = 0; i < (int)v.size(); i++) {
        int now = calc(v, 0, i + 1) + calc(v, i + 1, (int)v.size());
        if (now > max_points || now == max_points && 1 < i && i < (int)v.size() - 1) {
          p = i;
          max_points = now;
        }
      }
      v.push_back('a');
      for (int i = (int)v.size() - 1; i > p + 1; i--) {
        v[i] = v[i - 1];
      }
      v[p + 1] = x;
      ret += max_points;
    }
    return ret;
  }
};
