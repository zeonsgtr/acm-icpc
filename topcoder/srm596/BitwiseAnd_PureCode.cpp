#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <sstream>
#include <numeric>
#include <climits>
#include <string>
#include <cctype>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <vector>
#include <queue>
#include <list>
#include <map>
#include <set>
using namespace std;

#define SIZE(x) ((int)(x).size())
#define ALL(x) (x).begin(), (x).end()
#define PB push_back
#define MP make_pair
#define foreach(e, x) for (__typeof((x).begin()) e = (x).begin(); e != (x).end(); ++e)

typedef long long int64; 

const int N = 60;

struct BitwiseAnd {
	vector<long long> lexSmallest(vector<long long> subset, int n) {
		vector <long long> ret(subset);
		vector <vector <int> > positions(n);
		vector <bool> ok(N, true);
		for (int i = 0; i < SIZE(subset); i++) {
			for (int j = i + 1; j < SIZE(subset); j++) {
				if ((subset[i] & subset[j]) == 0LL) {
					return vector <long long>();
				}
				for (int k = j + 1; k < SIZE(subset); k++) {
					if (subset[i] & subset[j] & subset[k]) {
						return vector <long long>();
					}
				}
			}
			for (int j = 0; j < N; j++) {
				if (subset[i] >> j & 1) {
					bool flag = true;
					for (int k = 0; k < SIZE(subset); k++) {
						if (k != i && (subset[k] >> j & 1)) {
							flag = false;
							break;
						}
					}
					if (flag) {
						positions[i].PB(j);
					}
					ok[j] = false;
				}
			}
			reverse(ALL(positions[i]));
		}
		vector <int> vec;
		for (int i = 0; i < N; i++) {
			if (ok[i]) {
				vec.push_back(i);
			}
		}
		if (SIZE(vec) < (n - SIZE(subset)) * (n - SIZE(subset) - 1) / 2) {
			return vector <long long>();
		}
		for (int i = SIZE(subset); i < n; i++) {
			int64 value = 0;
			for (int j = 0; j < SIZE(subset); j++) {
				if (positions[j].empty()) {
					return vector <long long>();
				}
				value |= 1LL << positions[j].back();
				positions[j].pop_back();
			}
			ret.push_back(value);
		}
		int k = 0;
		for (int i = SIZE(subset); i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				ret[i] |= 1LL << vec[k];
				ret[j] |= 1LL << vec[k];
				k++;
			}
		}
		sort(ALL(ret));
		return ret;
	}
};
