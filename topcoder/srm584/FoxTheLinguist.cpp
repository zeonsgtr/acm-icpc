#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <climits>
#include <string>
#include <cctype>
#include <vector>
#include <map>
#include <set>
using namespace std;

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())
#define PB push_back
#define MP make_pair
#define FOR(itr, x) for (__typeof((x).end()) itr = (x).begin(); itr != (x).end(); itr++)

typedef long long int64; 

const int N = 100;

struct Edge {
    int s, t, w;
    Edge(int s, int t, int w): s(s), t(t), w(w) {}
};

int n;
int weight[N];
int min_edge[N], visited[N], belong[N];
vector <Edge> v;

int optimum_arborescence(int root) {
    int result = 0, count;
    while (true) {
        fill(min_edge, min_edge + n, -1);
        for (int i = 0; i < SIZE(v); i++) {
            if (v[i].s != v[i].t && (min_edge[v[i].t] == -1 || v[i].w < v[min_edge[v[i].t]].w)) {
                min_edge[v[i].t] = i;
            }
        }
        for (int i = 0; i < n; i++) {
            if (i != root && min_edge[i] == -1) {
                return -1;
            }
        }
        fill(visited, visited + n, -1);
        fill(belong, belong + n, -1);
        count = 0;
        for (int i = 0, j; i < n; i++) {
            if (i != root) {
                result += v[min_edge[i]].w;
            }
            if (visited[i] == -1) {
                for (j = i; j != root && visited[j] == -1; j = v[min_edge[j]].s) {
                    visited[j] = i;
                }
                if (j != root && visited[j] == i) {
                    for (; belong[j] == -1; j = v[min_edge[j]].s) {
                        belong[j] = count;
                    }
                    count++;
                }
            }
        }
        if (count == 0) {
            break;
        }
        for (int i = 0; i < n; i++) {
            if (belong[i] == -1) {
                belong[i] = count++;
            }
            weight[i] = i == root ? 0 : v[min_edge[i]].w;
        }
        for (int i = 0; i < SIZE(v); i++) {
            if (belong[v[i].s] != belong[v[i].t]) {
                v[i].w -= weight[v[i].t];
            }
            v[i].s = belong[v[i].s];
            v[i].t = belong[v[i].t];
        }
        n = count;
        root = belong[root];
    }
    return result;
}

struct FoxTheLinguist {
    int id(int a, int b) {
        return a * 10 + b;
    }
    int minimalHours(int n, vector <string> courseInfo) {
        string buffer;
        for (int i = 0; i < SIZE(courseInfo); i++) {
            buffer += courseInfo[i];
        }
        istringstream sin(buffer);
        v.clear();
        string course;
        while (sin >> course) {
            int s = id(course[0] - 'A', course[1] - '0');
            int t = id(course[4] - 'A', course[5] - '0');
            int w;
            sscanf(course.substr(7, SIZE(course)).c_str(), "%d", &w);
            v.push_back(Edge(s, t, w));
        }
        ::n = n * 10 + 1;
        for (int i = 0; i < n; i++) {
            v.push_back(Edge(::n - 1, id(i, 0), 0));
            for (int j = 9; j > 0; j--) {
                v.push_back(Edge(id(i, j), id(i, j - 1), 0));
            }
        }
        return optimum_arborescence(::n - 1);
    }
};

//{{{ CUT
// BEGIN CUT HERE
namespace moj_harness {
	int run_test_case(int);
	void run_test(int casenum = -1, bool quiet = false) {
		if (casenum != -1) {
			if (run_test_case(casenum) == -1 && !quiet) {
				cerr << "Illegal input! Test case " << casenum << " does not exist." << endl;
			}
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = run_test_case(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			cerr << "No test cases run." << endl;
		} else if (correct < total) {
			cerr << "Some cases FAILED (passed " << correct << " of " << total << ")." << endl;
		} else {
			cerr << "All " << total << " tests passed!" << endl;
		}
	}
	
	int verify_case(int casenum, const int &expected, const int &received, clock_t elapsed) { 
		cerr << "Example " << casenum << "... "; 
		
		string verdict;
		vector<string> info;
		char buf[100];
		
		if (elapsed > CLOCKS_PER_SEC / 200) {
			sprintf(buf, "time %.2fs", elapsed * (1.0/CLOCKS_PER_SEC));
			info.push_back(buf);
		}
		
		if (expected == received) {
			verdict = "PASSED";
		} else {
			verdict = "FAILED";
		}
		
		cerr << verdict;
		if (!info.empty()) {
			cerr << " (";
			for (int i=0; i<(int)info.size(); ++i) {
				if (i > 0) cerr << ", ";
				cerr << info[i];
			}
			cerr << ")";
		}
		cerr << endl;
		
		if (verdict == "FAILED") {
			cerr << "    Expected: " << expected << endl; 
			cerr << "    Received: " << received << endl; 
		}
		
		return verdict == "PASSED";
	}

	int run_test_case(int casenum) {
		switch (casenum) {
		case 0: {
			int n                     = 1;
			string courseInfo[]       = {"A0->A9:1000 A0->A6:0300 A3->A9:0600"};
			int expected__            = 900;

			clock_t start__           = clock();
			int received__            = FoxTheLinguist().minimalHours(n, vector <string>(courseInfo, courseInfo + (sizeof courseInfo / sizeof courseInfo[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 1: {
			int n                     = 2;
			string courseInfo[]       = {"A0->A9:1000 B0->B9:1000 A1->B9:0300 B1->A9:0200"};
			int expected__            = 1200;

			clock_t start__           = clock();
			int received__            = FoxTheLinguist().minimalHours(n, vector <string>(courseInfo, courseInfo + (sizeof courseInfo / sizeof courseInfo[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 2: {
			int n                     = 3;
			string courseInfo[]       = {"C0->A6:00", "01 A3", "->B9:0001 A3->C6:000", "1",
" C3->A9:0001 A9->C9:0001 A0->A9:9999",
" B0->B9:9999 C0->C9:9999 A6->A9:9999"};
			int expected__            = 5;

			clock_t start__           = clock();
			int received__            = FoxTheLinguist().minimalHours(n, vector <string>(courseInfo, courseInfo + (sizeof courseInfo / sizeof courseInfo[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 3: {
			int n                     = 4;
			string courseInfo[]       = {"A0->A6:6666 A0->A9:9999",
" B0->B6:6666 B0->B9:9999",
" C0->C6:6666 C0->C9:9999",
" D0->D6:6666 D0->D9:9999",
" A6->B6:0666 B6->C6:0666",
" C6->D6:0666 D6->A6:0666",
" A9->B9:0099 B9->C9:0099",
" C9->D9:0099 D9->A9:0099"};
			int expected__            = 10296;

			clock_t start__           = clock();
			int received__            = FoxTheLinguist().minimalHours(n, vector <string>(courseInfo, courseInfo + (sizeof courseInfo / sizeof courseInfo[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 4: {
			int n                     = 1;
			string courseInfo[]       = {"A0->A9:9999 A0->A9:8888"};
			int expected__            = 8888;

			clock_t start__           = clock();
			int received__            = FoxTheLinguist().minimalHours(n, vector <string>(courseInfo, courseInfo + (sizeof courseInfo / sizeof courseInfo[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 5: {
			int n                     = 1;
			string courseInfo[]       = {"A9->A9:0000",
" A9->A0:0000"};
			int expected__            = -1;

			clock_t start__           = clock();
			int received__            = FoxTheLinguist().minimalHours(n, vector <string>(courseInfo, courseInfo + (sizeof courseInfo / sizeof courseInfo[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}

		// custom cases

/*      case 6: {
			int n                     = ;
			string courseInfo[]       = ;
			int expected__            = ;

			clock_t start__           = clock();
			int received__            = FoxTheLinguist().minimalHours(n, vector <string>(courseInfo, courseInfo + (sizeof courseInfo / sizeof courseInfo[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
/*      case 7: {
			int n                     = ;
			string courseInfo[]       = ;
			int expected__            = ;

			clock_t start__           = clock();
			int received__            = FoxTheLinguist().minimalHours(n, vector <string>(courseInfo, courseInfo + (sizeof courseInfo / sizeof courseInfo[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
/*      case 8: {
			int n                     = ;
			string courseInfo[]       = ;
			int expected__            = ;

			clock_t start__           = clock();
			int received__            = FoxTheLinguist().minimalHours(n, vector <string>(courseInfo, courseInfo + (sizeof courseInfo / sizeof courseInfo[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
		default:
			return -1;
		}
	}
}
 

int main(int argc, char *argv[]) {
	if (argc == 1) {
		moj_harness::run_test();
	} else {
		for (int i=1; i<argc; ++i)
			moj_harness::run_test(atoi(argv[i]));
	}
}
// END CUT HERE
//}}}
