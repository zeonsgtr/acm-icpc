#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <climits>
#include <string>
#include <cctype>
#include <vector>
#include <map>
#include <set>
#include <cmath>
using namespace std;

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())
#define PB push_back
#define MP make_pair
#define FOR(itr, x) for (__typeof((x).end()) itr = (x).begin(); itr != (x).end(); itr++)

typedef long long int64; 

const double EPS = 1e-8;

struct Edge {
    int s, t;
    double w;
    Edge(int s, int t, double w): s(s), t(t), w(w) {}
    bool operator <(const Edge& o) const {
        return w < o.w;
    }
};

double sqr(double x) {
    return x * x;
}

double dist(double x1, double y1, double x2, double y2) {
    return sqrt(sqr(x1 - x2) + sqr(y1 - y2));
}

int n;
int f[55];
double target;

int find(int x) {
    return f[x] == x ? x : f[x] = find(f[x]);
}

bool cmp(const Edge& a, const Edge& b) {
    return fabs(a.w - target) < fabs(b.w - target);
}

double calc(std::vector <Edge> v, double w) {
    target = w;
    std::sort(ALL(v), cmp);
    for (int i = 0; i < n; i++) {
        f[i] = i;
    }
    std::vector <double> choose;
    double mean = 0;
    for (int i = 0; i < SIZE(v); i++) {
        int a = find(v[i].s);
        int b = find(v[i].t);
        if (a != b) {
            f[a] = b;
            choose.push_back(v[i].w);
            mean += v[i].w;
        }
    }
    mean /= SIZE(choose);
    double ret = 0;
    for (int i = 0; i < SIZE(choose); i++) {
        ret += sqr(choose[i] - mean);
    }
    return sqrt(ret / SIZE(choose));
}

struct Egalitarianism2 {
    double minStdev(vector <int> x, vector <int> y) {
        n = SIZE(x);
        vector <Edge> e;
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                e.push_back(Edge(i, j, dist(x[i], y[i], x[j], y[j])));
            }
        }
        sort(ALL(e));
        double result = 1e30;
        std::vector <double> valve;
        for (int i = 0; i < SIZE(e); i++) {
            for (int j = i + 1; j < SIZE(e); j++) {
                valve.push_back((e[i].w + e[j].w) * .5);
            }
        }
        std::sort(ALL(valve));
        for (int i = 1; i < SIZE(valve); i++) {
            result = std::min(result, calc(e, (valve[i] + valve[i - 1]) * .5));
        }
        return result;
    }
};

//{{{ CUT
// BEGIN CUT HERE
namespace moj_harness {
	int run_test_case(int);
	void run_test(int casenum = -1, bool quiet = false) {
		if (casenum != -1) {
			if (run_test_case(casenum) == -1 && !quiet) {
				cerr << "Illegal input! Test case " << casenum << " does not exist." << endl;
			}
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = run_test_case(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			cerr << "No test cases run." << endl;
		} else if (correct < total) {
			cerr << "Some cases FAILED (passed " << correct << " of " << total << ")." << endl;
		} else {
			cerr << "All " << total << " tests passed!" << endl;
		}
	}
	
	static const double MAX_DOUBLE_ERROR = 1e-9; static bool topcoder_fequ(double expected, double result) { if (isnan(expected)) { return isnan(result); } else if (isinf(expected)) { if (expected > 0) { return result > 0 && isinf(result); } else { return result < 0 && isinf(result); } } else if (isnan(result) || isinf(result)) { return false; } else if (fabs(result - expected) < MAX_DOUBLE_ERROR) { return true; } else { double mmin = min(expected * (1.0 - MAX_DOUBLE_ERROR), expected * (1.0 + MAX_DOUBLE_ERROR)); double mmax = max(expected * (1.0 - MAX_DOUBLE_ERROR), expected * (1.0 + MAX_DOUBLE_ERROR)); return result > mmin && result < mmax; } }
	double moj_relative_error(double expected, double result) { if (isnan(expected) || isinf(expected) || isnan(result) || isinf(result) || expected == 0) return 0; return fabs(result-expected) / fabs(expected); }
	
	int verify_case(int casenum, const double &expected, const double &received, clock_t elapsed) { 
		cerr << "Example " << casenum << "... "; 
		
		string verdict;
		vector<string> info;
		char buf[100];
		
		if (elapsed > CLOCKS_PER_SEC / 200) {
			sprintf(buf, "time %.2fs", elapsed * (1.0/CLOCKS_PER_SEC));
			info.push_back(buf);
		}
		
		if (topcoder_fequ(expected, received)) {
			verdict = "PASSED";
			double rerr = moj_relative_error(expected, received); 
			if (rerr > 0) {
				sprintf(buf, "relative error %.3e", rerr);
				info.push_back(buf);
			}
		} else {
			verdict = "FAILED";
		}
		
		cerr << verdict;
		if (!info.empty()) {
			cerr << " (";
			for (int i=0; i<(int)info.size(); ++i) {
				if (i > 0) cerr << ", ";
				cerr << info[i];
			}
			cerr << ")";
		}
		cerr << endl;
		
		if (verdict == "FAILED") {
			cerr << "    Expected: " << expected << endl; 
			cerr << "    Received: " << received << endl; 
		}
		
		return verdict == "PASSED";
	}

	int run_test_case(int casenum) {
		switch (casenum) {
		case 0: {
			int x[]                   = {0,0,1,1};
			int y[]                   = {0,1,0,1};
			double expected__         = 0.0;

			clock_t start__           = clock();
			double received__         = Egalitarianism2().minStdev(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 1: {
			int x[]                   = {0,0,0};
			int y[]                   = {0,9,10};
			double expected__         = 0.5;

			clock_t start__           = clock();
			double received__         = Egalitarianism2().minStdev(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 2: {
			int x[]                   = {12,46,81,56};
			int y[]                   = {0,45,2,67};
			double expected__         = 6.102799971320872;

			clock_t start__           = clock();
			double received__         = Egalitarianism2().minStdev(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 3: {
			int x[]                   = {0,0,0,0,0,0,0};
			int y[]                   = {0,2,3,9,10,15,16};
			double expected__         = 0.9428090415820617;

			clock_t start__           = clock();
			double received__         = Egalitarianism2().minStdev(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 4: {
			int x[]                   = {167053, 536770, -590401, 507047, 350178, -274523, -584679, -766795, -664177, 267757, -291856, -765547, 604801, -682922, -404590, 468001, 607925, 503849, -499699, -798637};
			int y[]                   = {-12396, -66098, -56843, 20270, 81510, -23294, 10423, 24007, -24343, -21587, -6318, -7396, -68622, 56304, -85680, -14890, -38373, -25477, -38240, 11736};
			double expected__         = 40056.95946451678;

			clock_t start__           = clock();
			double received__         = Egalitarianism2().minStdev(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 5: {
			int x[]                   = {-306880, 169480, -558404, -193925, 654444, -300247, -456420, -119436, -620920, -470018, -914272, -691256, -49418, -21054, 603373, -23656, 891691, 258986, -453793, -782940};
			int y[]                   = {-77318, -632629, -344942, -361706, 191982, 349424, 676536, 166124, 291342, -268968, 188262, -537953, -70432, 156803, 166174, 345128, 58614, -671747, 508265, 92324};
			double expected__         = 36879.1512763429;

			clock_t start__           = clock();
			double received__         = Egalitarianism2().minStdev(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}

		// custom cases

/*      case 6: {
			int x[]                   = ;
			int y[]                   = ;
			double expected__         = ;

			clock_t start__           = clock();
			double received__         = Egalitarianism2().minStdev(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
/*      case 7: {
			int x[]                   = ;
			int y[]                   = ;
			double expected__         = ;

			clock_t start__           = clock();
			double received__         = Egalitarianism2().minStdev(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
/*      case 8: {
			int x[]                   = ;
			int y[]                   = ;
			double expected__         = ;

			clock_t start__           = clock();
			double received__         = Egalitarianism2().minStdev(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
		default:
			return -1;
		}
	}
}
 

int main(int argc, char *argv[]) {
	if (argc == 1) {
		moj_harness::run_test();
	} else {
		for (int i=1; i<argc; ++i)
			moj_harness::run_test(atoi(argv[i]));
	}
}
// END CUT HERE
//}}}
