#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <climits>
#include <string>
#include <cctype>
#include <vector>
#include <cmath>
#include <map>
#include <set>
using namespace std;

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())
#define PB push_back
#define MP make_pair
#define FOR(itr, x) for (__typeof((x).end()) itr = (x).begin(); itr != (x).end(); itr++)

typedef long long int64; 

inline double sqr(double x) {
    return x * x;
}

struct Point {
    double x, y;
    Point(double x, double y): x(x), y(y) {}
};

inline Point operator +(const Point& a, const Point& b) {
    return Point(a.x + b.x, a.y + b.y);
}

inline Point operator -(const Point& a, const Point& b) {
    return Point(a.x - b.x, a.y - b.y);
}

inline Point operator *(const Point& a, double l) {
    return Point(a.x * l, a.y * l);
}

inline Point operator /(const Point& a, double l) {
    return Point(a.x / l, a.y / l);
}

inline double det(const Point& a, const Point& b) {
    return a.x * b.y - a.y * b.x;
}

inline double dist(const Point& a, const Point& b) {
    return sqrt(sqr(a.x - b.x) + sqr(a.y - b.y));
}

struct Line {
    Point s, t;
    Line(const Point& s, const Point& t): s(s), t(t) {}
};

inline Point line_intersection(const Line& a, const Line& b) {
    double s1 = det(a.t - a.s, b.s - a.s);
    double s2 = det(a.t - a.s, b.t - a.s);
    return (b.s * s2 - b.t * s1) / (s2 - s1);
}

const int N = 70000 + 10;

double sum[N];

int solve(int n) {
    sum[0] = 0;
    std::vector <double> height(1, 0);
    std::vector <Point> v(1, Point(0, 0));
    for (int i = 1; i <= n; i++) {
        Line l1(Point(i, 0), Point(0, 1)); 
        Line l2(Point(0, 0), Point(n, 1));
        Point p = line_intersection(l1, l2);
        v.push_back(p);
        height.push_back(p.y);
    }
    for (int i = 1; i < n; i++) {
        sum[i] = sum[i - 1];
        if (i & 1) {
            double base = dist(v[i], Point(0, 1)) / dist(Point(i, 0), Point(0, 1));
            sum[i] += .5 * base * (height[i] - height[i - 1]);
            sum[i] += .5 * base * (height[i + 1] - height[i]);
        }
    }
    double answer = 0;
    for (int i = 0; i < n; i++) {
        answer += sum[i];
    }
    double h = n / dist(Point(0, 0), Point(n, 1));
    for (int i = 1; i <= n; i++) {
        if (i & 1) {
            answer += dist(v[i - 1], v[i]) * h;
        }
    }
    if (~ n & 1) {
        answer += 0.5 * n * (1.0 - v.back().y);
    }
    return int(answer + 1e-6);
}

struct TriangleXor {
	int theArea(int W) {
        return solve(W);
	}
};

//{{{ CUT
// BEGIN CUT HERE
namespace moj_harness {
	int run_test_case(int);
	void run_test(int casenum = -1, bool quiet = false) {
		if (casenum != -1) {
			if (run_test_case(casenum) == -1 && !quiet) {
				cerr << "Illegal input! Test case " << casenum << " does not exist." << endl;
			}
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = run_test_case(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			cerr << "No test cases run." << endl;
		} else if (correct < total) {
			cerr << "Some cases FAILED (passed " << correct << " of " << total << ")." << endl;
		} else {
			cerr << "All " << total << " tests passed!" << endl;
		}
	}
	
	int verify_case(int casenum, const int &expected, const int &received, clock_t elapsed) { 
		cerr << "Example " << casenum << "... "; 
		
		string verdict;
		vector<string> info;
		char buf[100];
		
		if (elapsed > CLOCKS_PER_SEC / 200) {
			sprintf(buf, "time %.2fs", elapsed * (1.0/CLOCKS_PER_SEC));
			info.push_back(buf);
		}
		
		if (expected == received) {
			verdict = "PASSED";
		} else {
			verdict = "FAILED";
		}
		
		cerr << verdict;
		if (!info.empty()) {
			cerr << " (";
			for (int i=0; i<(int)info.size(); ++i) {
				if (i > 0) cerr << ", ";
				cerr << info[i];
			}
			cerr << ")";
		}
		cerr << endl;
		
		if (verdict == "FAILED") {
			cerr << "    Expected: " << expected << endl; 
			cerr << "    Received: " << received << endl; 
		}
		
		return verdict == "PASSED";
	}

	int run_test_case(int casenum) {
		switch (casenum) {
		case 0: {
			int W                     = 1;
			int expected__            = 0;

			clock_t start__           = clock();
			int received__            = TriangleXor().theArea(W);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 1: {
			int W                     = 2;
			int expected__            = 1;

			clock_t start__           = clock();
			int received__            = TriangleXor().theArea(W);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 2: {
			int W                     = 3;
			int expected__            = 1;

			clock_t start__           = clock();
			int received__            = TriangleXor().theArea(W);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 3: {
			int W                     = 4;
			int expected__            = 2;

			clock_t start__           = clock();
			int received__            = TriangleXor().theArea(W);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 4: {
			int W                     = 5;
			int expected__            = 2;

			clock_t start__           = clock();
			int received__            = TriangleXor().theArea(W);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 5: {
			int W                     = 12345;
			int expected__            = 4629;

			clock_t start__           = clock();
			int received__            = TriangleXor().theArea(W);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}

		// custom cases

        case 6: {
			int W                     = 126;
			int expected__            = 78;

			clock_t start__           = clock();
			int received__            = TriangleXor().theArea(W);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}  
/*      case 7: {
			int W                     = ;
			int expected__            = ;

			clock_t start__           = clock();
			int received__            = TriangleXor().theArea(W);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
/*      case 8: {
			int W                     = ;
			int expected__            = ;

			clock_t start__           = clock();
			int received__            = TriangleXor().theArea(W);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
		default:
			return -1;
		}
	}
}
 

int main(int argc, char *argv[]) {
	if (argc == 1) {
		moj_harness::run_test();
	} else {
		for (int i=1; i<argc; ++i)
			moj_harness::run_test(atoi(argv[i]));
	}
}
// END CUT HERE
//}}}
