#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <climits>
#include <string>
#include <cctype>
#include <vector>
#include <map>
#include <set>
using namespace std;

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())
#define PB push_back
#define MP make_pair
#define FOR(itr, x) for (__typeof((x).end()) itr = (x).begin(); itr != (x).end(); itr++)

typedef long long int64; 

const int N = 2555;
const int M = N * 10;
const int INF = int(1e9);

int n, m, source, sink;
int first_edge[N], next_edge[M], to[M], c[M], edge_count;
int his[N], now[N], num[N], pre[N], d[N];

inline void add_edge(int s, int t, int x) {
    to[edge_count] = t;
    c[edge_count] = x;
    next_edge[edge_count] = first_edge[s];
    first_edge[s] = edge_count++;

    to[edge_count] = s;
    c[edge_count] = 0;
    next_edge[edge_count] = first_edge[t];
    first_edge[t] = edge_count++;
}

int sap() {
    int i, j, k, ret = 0, flow = INF;
    for (i = 0; i < n; i++) {
        now[i] = first_edge[i];
        num[i] = d[i] = 0;
    }
    num[0] = 0, i = source;
    while (d[source] < n) {
        his[i] = flow;
        for (k = now[i] = first_edge[i]; k != -1; k = next_edge[k]) {
            if (c[k] > 0 && d[i] == d[j = to[k]] + 1) {
                break;
            }
        }
        if (k > -1) {
            flow = min(flow, c[k]);
            now[i] = k;
            pre[j] = i;
            i = j;
            if (i == sink) {
                ret += flow;
                while (i != source) {
                    i = pre[i];
                    c[now[i]] -= flow;
                    c[now[i] ^ 1] += flow;
                }
                flow = INF;
            }
        } else {
            if (--num[d[i]] == 0) {
                break;
            }
            d[i] = n;
            for (k = now[i] = first_edge[i]; k != -1; k = next_edge[k]) {
                if (c[k] > 0 && d[to[k]] + 1 < d[i]) {
                    d[i] = d[to[now[i] = k]] + 1;
                }
            }
            ++num[d[i]];
            if (i != source) {
                i = pre[i];
                flow = his[i];
            }
        }
    }
    return ret;
}

struct FoxAndGo3 {
	int maxEmptyCells(vector <string> board) {
        int row = SIZE(board), col = SIZE(board[0]);
        source = row * col;
        sink = source + 1;
        n = sink + 1;
        std::fill(first_edge, first_edge + n, -1);
        edge_count = 0;
        int subtract = 0;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (board[i][j] == 'o') {
                    for (int dx = -1; dx <= 1; dx++) {
                        for (int dy = -1; dy <= 1; dy++) {
                            if (dx * dx + dy * dy == 1) {
                                int x = i + dx, y = j + dy;
                                if (0 <= x && x < row && 0 <= y && y < col) {
                                    add_edge(i * col + j, x * col + y, 1);
                                }
                            }
                        }
                    }
                    add_edge(source, i * col + j, 1);
                } else if (board[i][j] == '.') {
                    add_edge(i * col + j, sink, 1);
                } else {
                    subtract++;
                }
            }
        }
        return row * col - sap() - subtract;
	}
};

//{{{ CUT
// BEGIN CUT HERE
namespace moj_harness {
	int run_test_case(int);
	void run_test(int casenum = -1, bool quiet = false) {
		if (casenum != -1) {
			if (run_test_case(casenum) == -1 && !quiet) {
				cerr << "Illegal input! Test case " << casenum << " does not exist." << endl;
			}
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = run_test_case(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			cerr << "No test cases run." << endl;
		} else if (correct < total) {
			cerr << "Some cases FAILED (passed " << correct << " of " << total << ")." << endl;
		} else {
			cerr << "All " << total << " tests passed!" << endl;
		}
	}
	
	int verify_case(int casenum, const int &expected, const int &received, clock_t elapsed) { 
		cerr << "Example " << casenum << "... "; 
		
		string verdict;
		vector<string> info;
		char buf[100];
		
		if (elapsed > CLOCKS_PER_SEC / 200) {
			sprintf(buf, "time %.2fs", elapsed * (1.0/CLOCKS_PER_SEC));
			info.push_back(buf);
		}
		
		if (expected == received) {
			verdict = "PASSED";
		} else {
			verdict = "FAILED";
		}
		
		cerr << verdict;
		if (!info.empty()) {
			cerr << " (";
			for (int i=0; i<(int)info.size(); ++i) {
				if (i > 0) cerr << ", ";
				cerr << info[i];
			}
			cerr << ")";
		}
		cerr << endl;
		
		if (verdict == "FAILED") {
			cerr << "    Expected: " << expected << endl; 
			cerr << "    Received: " << received << endl; 
		}
		
		return verdict == "PASSED";
	}

	int run_test_case(int casenum) {
		switch (casenum) {
		case 0: {
			string board[]            = {"o.o",
 ".o.",
 "o.o"};
			int expected__            = 5;

			clock_t start__           = clock();
			int received__            = FoxAndGo3().maxEmptyCells(vector <string>(board, board + (sizeof board / sizeof board[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 1: {
			string board[]            = {"...",
 ".o.",
 "..."}
;
			int expected__            = 8;

			clock_t start__           = clock();
			int received__            = FoxAndGo3().maxEmptyCells(vector <string>(board, board + (sizeof board / sizeof board[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 2: {
			string board[]            = {"xxxxx",
 "xxoxx",
 "xo.ox",
 "xxoxx",
 "xxxxx"}
;
			int expected__            = 4;

			clock_t start__           = clock();
			int received__            = FoxAndGo3().maxEmptyCells(vector <string>(board, board + (sizeof board / sizeof board[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 3: {
			string board[]            = {".xox.",
 ".o.ox",
 "x.o.o",
 "ox.ox",
 ".ox.."}
 ;
			int expected__            = 12;

			clock_t start__           = clock();
			int received__            = FoxAndGo3().maxEmptyCells(vector <string>(board, board + (sizeof board / sizeof board[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 4: {
			string board[]            = {"o.o.o",
 ".ox..",
 "oxxxo",
 "..x..",
 "o.o.o"}
;
			int expected__            = 12;

			clock_t start__           = clock();
			int received__            = FoxAndGo3().maxEmptyCells(vector <string>(board, board + (sizeof board / sizeof board[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 5: {
			string board[]            = {"...",
 "...",
 "..."};
			int expected__            = 9;

			clock_t start__           = clock();
			int received__            = FoxAndGo3().maxEmptyCells(vector <string>(board, board + (sizeof board / sizeof board[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}

		// custom cases

/*      case 6: {
			string board[]            = ;
			int expected__            = ;

			clock_t start__           = clock();
			int received__            = FoxAndGo3().maxEmptyCells(vector <string>(board, board + (sizeof board / sizeof board[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
/*      case 7: {
			string board[]            = ;
			int expected__            = ;

			clock_t start__           = clock();
			int received__            = FoxAndGo3().maxEmptyCells(vector <string>(board, board + (sizeof board / sizeof board[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
/*      case 8: {
			string board[]            = ;
			int expected__            = ;

			clock_t start__           = clock();
			int received__            = FoxAndGo3().maxEmptyCells(vector <string>(board, board + (sizeof board / sizeof board[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
		default:
			return -1;
		}
	}
}
 

int main(int argc, char *argv[]) {
	if (argc == 1) {
		moj_harness::run_test();
	} else {
		for (int i=1; i<argc; ++i)
			moj_harness::run_test(atoi(argv[i]));
	}
}
// END CUT HERE
//}}}
