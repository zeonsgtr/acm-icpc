import java.util.*;
import java.math.*;

public class AstronomicalRecords {
    static final int INF = (int)1e9;
    public int minimalPlanets(int[] A, int[] B) {
        int n = A.length;
        int m = B.length;
        int[][][][] dp = new int[n + 1][m + 1][n + 1][m + 1];
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= m; j++) {
                for (int k = 0; k <= n; k++) {
                    for (int l = 0; l <= m; l++) {
                        dp[i][j][k][l] = INF;
                    }
                }
            }
        }
        dp[0][0][0][0] = 0;
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= m; j++) {
                for (int k = 0; k <= i; k++) {
                    for (int l = 0; l <= j; l++) {
                        if (i < n) {
                            dp[i + 1][j][k][l] = Math.min(dp[i + 1][j][k][l], dp[i][j][k][l] + 1);
                        }
                        if (j < m) {
                            dp[i][j + 1][k][l] = Math.min(dp[i][j + 1][k][l], dp[i][j][k][l] + 1);
                        }
                        if (i < n && j < m && k == 0 && l == 0) {
                            dp[i + 1][j + 1][i + 1][j + 1] = Math.min(dp[i + 1][j + 1][i + 1][j + 1], dp[i][j][k][l] + 1);
                        }
                        if (i < n && j < m && k > 0 && l > 0 && 1L * A[k - 1] * B[j] == 1L * B[l - 1] * A[i]) {
                            dp[i + 1][j + 1][k][l] = Math.min(dp[i + 1][j + 1][k][l], dp[i][j][k][l] + 1);
                        }
                    }
                }
            }
        }
        int result = n + m;
        for (int k = 0; k <= n; k++) {
            for (int l = 0; l <= m; l++) {
                result = Math.min(result, dp[n][m][k][l]);
            }
        }
        return result;
    }

//{{{
// BEGIN CUT HERE
   public static void main(String[] args) {
		if (args.length == 0) {
			AstronomicalRecordsHarness.run_test(-1);
		} else {
			for (int i=0; i<args.length; ++i)
				AstronomicalRecordsHarness.run_test(Integer.valueOf(args[i]));
		}
	}
// END CUT HERE
}

// BEGIN CUT HERE
class AstronomicalRecordsHarness {
	public static void run_test(int casenum) {
		if (casenum != -1) {
			if (runTestCase(casenum) == -1)
				System.err.println("Illegal input! Test case " + casenum + " does not exist.");
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = runTestCase(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			System.err.println("No test cases run.");
		} else if (correct < total) {
			System.err.println("Some cases FAILED (passed " + correct + " of " + total + ").");
		} else {
			System.err.println("All " + total + " tests passed!");
		}
	}
	
	static boolean compareOutput(int expected, int result) { return expected == result; }
	static String formatResult(int res) {
		return String.format("%d", res);
	}
	
	static int verifyCase(int casenum, int expected, int received) { 
		System.err.print("Example " + casenum + "... ");
		if (compareOutput(expected, received)) {
			System.err.println("PASSED");
			return 1;
		} else {
			System.err.println("FAILED");
			System.err.println("    Expected: " + formatResult(expected)); 
			System.err.println("    Received: " + formatResult(received)); 
			return 0;
		}
	}

	static int runTestCase(int casenum) {
		switch(casenum) {
		case 0: {
			int[] A                   = {1,2,1,2,1};
			int[] B                   = {2,1,2,1,2};
			int expected__            = 6;

			return verifyCase(casenum, expected__, new AstronomicalRecords().minimalPlanets(A, B));
		}
		case 1: {
			int[] A                   = {1,2,3,4};
			int[] B                   = {2,4,6,8};
			int expected__            = 4;

			return verifyCase(casenum, expected__, new AstronomicalRecords().minimalPlanets(A, B));
		}
		case 2: {
			int[] A                   = {2,3,2,3,2,3,2};
			int[] B                   = {600,700,600,700,600,700,600};
			int expected__            = 10;

			return verifyCase(casenum, expected__, new AstronomicalRecords().minimalPlanets(A, B));
		}
		case 3: {
			int[] A                   = {1,2,3,4,5,6,7,8,9};
			int[] B                   = {6,7,8,9,10,11,12};
			int expected__            = 12;

			return verifyCase(casenum, expected__, new AstronomicalRecords().minimalPlanets(A, B));
		}
		case 4: {
			int[] A                   = {100000000,200000000};
			int[] B                   = {200000000,100000000};
			int expected__            = 3;

			return verifyCase(casenum, expected__, new AstronomicalRecords().minimalPlanets(A, B));
		}

		// custom cases

/*      case 5: {
			int[] A                   = ;
			int[] B                   = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new AstronomicalRecords().minimalPlanets(A, B));
		}*/
/*      case 6: {
			int[] A                   = ;
			int[] B                   = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new AstronomicalRecords().minimalPlanets(A, B));
		}*/
/*      case 7: {
			int[] A                   = ;
			int[] B                   = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new AstronomicalRecords().minimalPlanets(A, B));
		}*/
		default:
			return -1;
		}
	}
}

// END CUT HERE
//}}}
