import java.util.*;

public class HexagonalBoard {
    final static int[][] DELTA = {{0, -1}, {0, 1}, {-1, 0}, {1, 0}, {-1, 1}, {1, -1}};
    int n;
    boolean dfs(int x, int y, int c, int[][] color, String[] board) {
        color[x][y] = c;
        for (int k = 0; k < 6; k++) {
            int tx = x + DELTA[k][0];
            int ty = y + DELTA[k][1];
            if (0 <= tx && tx < n && 0 <= ty && ty < n && board[tx].charAt(ty) == 'X') {
                if (color[tx][ty] == -1) {
                    if (!dfs(tx, ty, c ^ 1, color, board)) {
                        return false;
                    }
                } else {
                    if (1 - color[tx][ty] != color[x][y]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    public int minColors(String[] board) {
        n = board.length;
        boolean[] flag = new boolean[]{false, false};
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (board[i].charAt(j) == 'X') {
                    flag[0] = true;
                    for (int k = 0; k < 6; k++) {
                        int x = i + DELTA[k][0];
                        int y = j + DELTA[k][1];
                        if (0 <= x && x < n && 0 <= y && y < n && board[x].charAt(y) == 'X') {
                            flag[1] = true;
                        }
                    }
                }
            }
        }
        if (!flag[0]) {
            return 0;
        }
        if (!flag[1]) {
            return 1;
        }
        int[][] color = new int[n][n];
        for (int i = 0; i < n; i++) {
            Arrays.fill(color[i], -1);
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (color[i][j] == -1 && board[i].charAt(j) == 'X') {
                    if (!dfs(i, j, 0, color, board)) {
                        return 3;
                    }
                }
            }
        }
        return 2;
    }

//{{{
// BEGIN CUT HERE
   public static void main(String[] args) {
		if (args.length == 0) {
			HexagonalBoardHarness.run_test(-1);
		} else {
			for (int i=0; i<args.length; ++i)
				HexagonalBoardHarness.run_test(Integer.valueOf(args[i]));
		}
	}
// END CUT HERE
}

// BEGIN CUT HERE
class HexagonalBoardHarness {
	public static void run_test(int casenum) {
		if (casenum != -1) {
			if (runTestCase(casenum) == -1)
				System.err.println("Illegal input! Test case " + casenum + " does not exist.");
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = runTestCase(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			System.err.println("No test cases run.");
		} else if (correct < total) {
			System.err.println("Some cases FAILED (passed " + correct + " of " + total + ").");
		} else {
			System.err.println("All " + total + " tests passed!");
		}
	}
	
	static boolean compareOutput(int expected, int result) { return expected == result; }
	static String formatResult(int res) {
		return String.format("%d", res);
	}
	
	static int verifyCase(int casenum, int expected, int received) { 
		System.err.print("Example " + casenum + "... ");
		if (compareOutput(expected, received)) {
			System.err.println("PASSED");
			return 1;
		} else {
			System.err.println("FAILED");
			System.err.println("    Expected: " + formatResult(expected)); 
			System.err.println("    Received: " + formatResult(received)); 
			return 0;
		}
	}

	static int runTestCase(int casenum) {
		switch(casenum) {
		case 0: {
			String[] board            = {"---",  "---",  "---"}  ;
			int expected__            = 0;

			return verifyCase(casenum, expected__, new HexagonalBoard().minColors(board));
		}
		case 1: {
			String[] board            = {"-X--",  "---X",  "----",  "-X--"};
			int expected__            = 1;

			return verifyCase(casenum, expected__, new HexagonalBoard().minColors(board));
		}
		case 2: {
			String[] board            = {"XXXX",  "---X",  "---X",  "---X"};
			int expected__            = 2;

			return verifyCase(casenum, expected__, new HexagonalBoard().minColors(board));
		}
		case 3: {
			String[] board            = {"XX-XX--" ,"-XX-XXX" ,"X-XX--X" ,"X--X-X-" ,"XX-X-XX" ,"-X-XX-X" ,"-XX-XX-"};
			int expected__            = 3;

			return verifyCase(casenum, expected__, new HexagonalBoard().minColors(board));
		}

		// custom cases

/*      case 4: {
			String[] board            = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new HexagonalBoard().minColors(board));
		}*/
/*      case 5: {
			String[] board            = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new HexagonalBoard().minColors(board));
		}*/
/*      case 6: {
			String[] board            = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new HexagonalBoard().minColors(board));
		}*/
		default:
			return -1;
		}
	}
}

// END CUT HERE
//}}}
