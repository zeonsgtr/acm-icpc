#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <climits>
#include <string>
#include <cctype>
#include <vector>
#include <map>
#include <set>
using namespace std;

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())
#define PB push_back
#define MP make_pair
#define FOR(itr, x) for (__typeof((x).end()) itr = (x).begin(); itr != (x).end(); itr++)

typedef long long int64; 

const int N = 10000 + 10;
const int M = 300000 + 10;
const int INF = int(1e9);

int n, m;
int source, target;
int first_edge[N], next_edge[M], to[M], edge_count;
int capacity[M], cost[M];

inline void add_edge(int s, int t, int a, int b) {
    to[edge_count] = t;
    next_edge[edge_count] = first_edge[s];
    capacity[edge_count] = a;
    cost[edge_count] = b;
    first_edge[s] = edge_count++;

    to[edge_count] = s;
    next_edge[edge_count] = first_edge[t];
    capacity[edge_count] = 0;
    cost[edge_count] = -b;
    first_edge[t] = edge_count++;
}

int result;
int dist[N], queue[N], head, tail;
bool in[N];
int pre[N], flow[N], which[N];

bool spfa() {
    for (int i = 0; i < n; i++) {
        dist[i] = INF;
        in[i] = false;
    }
    head = tail = 0;
    in[queue[++tail] = source] = true;
    dist[source] = 0;
    flow[source] = INF;
    while (head != tail) {
        int x = queue[++head == N ? head = 0 : head];
        in[x] = false;
        for (int itr = first_edge[x]; itr != -1; itr = next_edge[itr]) {
            int y = to[itr];
            if (capacity[itr] > 0 && dist[x] + cost[itr] < dist[y]) {
                dist[y] = dist[x] + cost[itr];
                pre[y] = x;
                flow[y] = std::min(flow[x], capacity[itr]);
                which[y] = itr;
                if (!in[y]) {
                    in[queue[++tail == N ? tail = 0 : tail] = y] = true;
                }
            }
        }
    }
    if (dist[target] == INF) {
        return false;
    }
    result += dist[target] * flow[target];
    for (int i = target; i != source; i = pre[i]) {
        capacity[which[i]] -= flow[target];
        capacity[which[i] ^ 1] += flow[target];
    }
    return true;
}

struct SpecialCells {
    int guess(vector <int> x, vector <int> y) {
        std::set <std::pair <int, int> > points;
        for (int i = 0; i < SIZE(x); i++) {
            points.insert(std::make_pair(x[i], y[i]));
        }
        std::sort(ALL(x));
        std::sort(ALL(y));
        std::vector <int> lx, ly;
        lx.push_back(x[0]);
        ly.push_back(y[0]);
        for (int i = 1; i < SIZE(x); i++) {
            if (x[i] != x[i - 1]) {
                lx.push_back(x[i]);
            }
        }
        for (int i = 1; i < SIZE(y); i++) {
            if (y[i] != y[i - 1]) {
                ly.push_back(y[i]);
            }
        }
        source = SIZE(lx) + SIZE(ly) + 1;
        target = source + 1;
        n = target + 1;
        edge_count = 0;
        std::fill(first_edge, first_edge + n, -1);
        for (int i = 0, j, k = 0; i < SIZE(x); i = j, k++) {
            for (j = i; j < SIZE(x) && x[i] == x[j]; j++);
            add_edge(source, k, j - i, 0);
            for (int l = 0; l < SIZE(ly); l++) {
                add_edge(k, l + SIZE(lx), 1, points.count(std::make_pair(x[i], ly[l])));
            }
        }
        for (int i = 0, j, k = 0; i < SIZE(y); i = j, k++) {
            for (j = i; j < SIZE(y) && y[i] == y[j]; j++);
            add_edge(k + SIZE(lx), target, j - i, 0);
        }
        result = 0;
        while (spfa());
        return result;
    }
};

//{{{ CUT
// BEGIN CUT HERE
namespace moj_harness {
	int run_test_case(int);
	void run_test(int casenum = -1, bool quiet = false) {
		if (casenum != -1) {
			if (run_test_case(casenum) == -1 && !quiet) {
				cerr << "Illegal input! Test case " << casenum << " does not exist." << endl;
			}
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = run_test_case(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			cerr << "No test cases run." << endl;
		} else if (correct < total) {
			cerr << "Some cases FAILED (passed " << correct << " of " << total << ")." << endl;
		} else {
			cerr << "All " << total << " tests passed!" << endl;
		}
	}
	
	int verify_case(int casenum, const int &expected, const int &received, clock_t elapsed) { 
		cerr << "Example " << casenum << "... "; 
		
		string verdict;
		vector<string> info;
		char buf[100];
		
		if (elapsed > CLOCKS_PER_SEC / 200) {
			sprintf(buf, "time %.2fs", elapsed * (1.0/CLOCKS_PER_SEC));
			info.push_back(buf);
		}
		
		if (expected == received) {
			verdict = "PASSED";
		} else {
			verdict = "FAILED";
		}
		
		cerr << verdict;
		if (!info.empty()) {
			cerr << " (";
			for (int i=0; i<(int)info.size(); ++i) {
				if (i > 0) cerr << ", ";
				cerr << info[i];
			}
			cerr << ")";
		}
		cerr << endl;
		
		if (verdict == "FAILED") {
			cerr << "    Expected: " << expected << endl; 
			cerr << "    Received: " << received << endl; 
		}
		
		return verdict == "PASSED";
	}

	int run_test_case(int casenum) {
		switch (casenum) {
		case 0: {
			int x[]                   = {1,2};
			int y[]                   = {1,2};
			int expected__            = 0;

			clock_t start__           = clock();
			int received__            = SpecialCells().guess(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 1: {
			int x[]                   = {1,1,2};
			int y[]                   = {1,2,1};
			int expected__            = 3;

			clock_t start__           = clock();
			int received__            = SpecialCells().guess(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 2: {
			int x[]                   = {1,2,1,2,1,2};
			int y[]                   = {1,2,3,1,2,3};
			int expected__            = 6;

			clock_t start__           = clock();
			int received__            = SpecialCells().guess(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 3: {
			int x[]                   = {1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9};
			int y[]                   = {1,2,3,1,2,3,1,2,3,1,2,3,1,2,3,1,2,3};
			int expected__            = 9;

			clock_t start__           = clock();
			int received__            = SpecialCells().guess(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 4: {
			int x[]                   = {1,100000};
			int y[]                   = {1,100000};
			int expected__            = 0;

			clock_t start__           = clock();
			int received__            = SpecialCells().guess(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}

		// custom cases

        case 5: {
			int x[]                   = {83427, 95369, 20060, 60493, 38336, 90028, 90028, 55737, 55737, 41422, 20060, 41422, 90028, 60493, 38336, 13927, 20060, 83427, 95369, 90028, 41422, 38336, 13927, 13927, 83427, 95369, 90028, 41422, 60493, 38336, 60493, 38336, 60493, 60493};
			int y[]                   = {2568, 2568, 16650, 80541, 85387, 80541, 16650, 97764, 89173, 80541, 80541, 2363, 2363, 16650, 80541, 97764, 89173, 2363, 5212, 97764, 85387, 2363, 89173, 5212, 89173, 80541, 89173, 2568, 89173, 16650, 5212, 68691, 2363, 2568};
			int expected__            = 8;

			clock_t start__           = clock();
			int received__            = SpecialCells().guess(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}  
        case 6: {
			int x[]                   = {0, 0, 0, 1, 2, 2, 2};
			int y[]                   = {0, 1, 2, 3, 0, 1, 2};
			int expected__            = 5;

			clock_t start__           = clock();
			int received__            = SpecialCells().guess(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}  
/*      case 7: {
			int x[]                   = ;
			int y[]                   = ;
			int expected__            = ;

			clock_t start__           = clock();
			int received__            = SpecialCells().guess(vector <int>(x, x + (sizeof x / sizeof x[0])), vector <int>(y, y + (sizeof y / sizeof y[0])));
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
		default:
			return -1;
		}
	}
}
 

int main(int argc, char *argv[]) {
	if (argc == 1) {
		moj_harness::run_test();
	} else {
		for (int i=1; i<argc; ++i)
			moj_harness::run_test(atoi(argv[i]));
	}
}
// END CUT HERE
//}}}
