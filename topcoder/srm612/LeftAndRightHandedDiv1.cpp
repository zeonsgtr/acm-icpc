#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <climits>
#include <string>
#include <cctype>
#include <vector>
#include <map>
#include <set>
using namespace std;

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())
#define PB push_back
#define MP make_pair
#define FOR(itr, x) for (__typeof((x).end()) itr = (x).begin(); itr != (x).end(); itr++)

typedef long long int64; 

const int MAX_N = int(1e6) + 10;
const int64 INF = 1LL << 60;

int N;
int a[MAX_N], T[MAX_N];
int cnt[MAX_N];
int64 sum[MAX_N], ans[MAX_N];

int countNumberL(int s, int t) {
    return s == 0 ? cnt[t] : cnt[t] - cnt[s - 1];
}

int64 solve() {
    int64 result = INF;

    sum[N] = 0;
    for (int i = N - 1; i >= 0; i--) {
        sum[i] = sum[i + 1];
        if (a[i] == 0) {
            sum[i] += N - i - 1;
        }
    }
    cnt[0] = a[0] == 0;
    for (int i = 1; i < N; i++) {
        cnt[i] = cnt[i - 1] + (a[i] == 0);
    }
    int64 value = 0;
    int s = cnt[N - 1];
    for (int i = 0, j = 0, k = 0; i < N; i++) {
        for (j = std::max(j, i); j < N && j - i - k < N - j + i - (s - k - (a[j] == 0)) - 1; j++) {
            if (a[j] == 0) {
                value += j - i - k;
                k++;
            }
        }
        if (a[i] == 0) {
            int d = countNumberL(j, N - 1);
            ans[i] = value + 1LL * d * i + sum[j];
            int t = i > 0 ? cnt[i - 1] : 0;
            ans[i] -= 1LL * d * (2 * t + d - 1) / 2;
            //printf("%d %lld\n", i, ans[i]);
            k--;
            value += k;
        }
        //printf("i = %d, k = %d, value = %lld\n", i, k, value);
        value -= k;
        //printf("i = %d, k = %d, value = %lld\n", i, k, value);
    }
    //puts("================");

    std::reverse(a, a + N);
    sum[N] = 0;
    for (int i = N - 1; i >= 0; i--) {
        sum[i] = sum[i + 1];
        if (a[i] == 0) {
            sum[i] += N - i - 1;
        }
    }
    cnt[0] = a[0] == 0;
    for (int i = 1; i < N; i++) {
        cnt[i] = cnt[i - 1] + (a[i] == 0);
    }
    value = 0;
    s = cnt[N - 1];
    for (int i = 0, j = 0, k = 0; i < N; i++) {
        for (j = std::max(j, i); j < N && j - i - k < N - j + i - (s - k - (a[j] == 0)) - 1; j++) {
            if (a[j] == 0) {
                value += j - i - k;
                k++;
            }
        }
        if (a[i] == 0) {
            int64 tmp;
            int d = countNumberL(j, N - 1);
            tmp = value + 1LL * d * i + sum[j];
            int t = i > 0 ? cnt[i - 1] : 0;
            tmp -= 1LL * d * (2 * t + d - 1) / 2;
            result = std::min(result, tmp + ans[N - i - 1]);
            //printf("%d %lld\n", N - i - 1, tmp + ans[N - i - 1]);
            k--;
            value += k;
        }
        value -= k;
    }

    std::reverse(a, a + N);
    return result;
}

struct LeftAndRightHandedDiv1 {
    long long countSwaps(string Y, int A, int B, int C, int D, int N) {
        ::N = N;
        T[0] = A;
        for (int i = 1; i <= N; i++) {
            T[i] = (1LL * T[i - 1] * B + C) % D;
        }
        for (int i = 0; i < N; i++) {
            a[i] = Y[T[i] % SIZE(Y)] == 'L' ? 0 : 1;
        }
        int64 result = solve();
        for (int i = 0; i < N; i++) {
            a[i] ^= 1;
        }
        result = std::min(result, solve());
        return result;
    }
};

//{{{ CUT
// BEGIN CUT HERE
namespace moj_harness {
	int run_test_case(int);
	void run_test(int casenum = -1, bool quiet = false) {
		if (casenum != -1) {
			if (run_test_case(casenum) == -1 && !quiet) {
				cerr << "Illegal input! Test case " << casenum << " does not exist." << endl;
			}
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = run_test_case(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			cerr << "No test cases run." << endl;
		} else if (correct < total) {
			cerr << "Some cases FAILED (passed " << correct << " of " << total << ")." << endl;
		} else {
			cerr << "All " << total << " tests passed!" << endl;
		}
	}
	
	int verify_case(int casenum, const long long &expected, const long long &received, clock_t elapsed) { 
		cerr << "Example " << casenum << "... "; 
		
		string verdict;
		vector<string> info;
		char buf[100];
		
		if (elapsed > CLOCKS_PER_SEC / 200) {
			sprintf(buf, "time %.2fs", elapsed * (1.0/CLOCKS_PER_SEC));
			info.push_back(buf);
		}
		
		if (expected == received) {
			verdict = "PASSED";
		} else {
			verdict = "FAILED";
		}
		
		cerr << verdict;
		if (!info.empty()) {
			cerr << " (";
			for (int i=0; i<(int)info.size(); ++i) {
				if (i > 0) cerr << ", ";
				cerr << info[i];
			}
			cerr << ")";
		}
		cerr << endl;
		
		if (verdict == "FAILED") {
			cerr << "    Expected: " << expected << endl; 
			cerr << "    Received: " << received << endl; 
		}
		
		return verdict == "PASSED";
	}

	int run_test_case(int casenum) {
		switch (casenum) {
		case 0: {
			string Y                  = "LR";
			int A                     = 1;
			int B                     = 0;
			int C                     = 1;
			int D                     = 2;
			int N                     = 1;
			long long expected__      = 0;

			clock_t start__           = clock();
			long long received__      = LeftAndRightHandedDiv1().countSwaps(Y, A, B, C, D, N);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 1: {
			string Y                  = "LRLR";
			int A                     = 1;
			int B                     = 1;
			int C                     = 2;
			int D                     = 3;
			int N                     = 4;
			long long expected__      = 0;

			clock_t start__           = clock();
			long long received__      = LeftAndRightHandedDiv1().countSwaps(Y, A, B, C, D, N);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 2: {
			string Y                  = "LRRLLR";
			int A                     = 2;
			int B                     = 3;
			int C                     = 4;
			int D                     = 5;
			int N                     = 6;
			long long expected__      = 1;

			clock_t start__           = clock();
			long long received__      = LeftAndRightHandedDiv1().countSwaps(Y, A, B, C, D, N);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 3: {
			string Y                  = "LRRLRLLRLRLRLLRLR";
			int A                     = 12;
			int B                     = 15;
			int C                     = 3;
			int D                     = 22;
			int N                     = 10;
			long long expected__      = 2;

			clock_t start__           = clock();
			long long received__      = LeftAndRightHandedDiv1().countSwaps(Y, A, B, C, D, N);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 4: {
			string Y                  = "LLLLLLLLL";
			int A                     = 0;
			int B                     = 1;
			int C                     = 2;
			int D                     = 3;
			int N                     = 1000000;
			long long expected__      = 0;

			clock_t start__           = clock();
			long long received__      = LeftAndRightHandedDiv1().countSwaps(Y, A, B, C, D, N);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 5: {
			string Y                  = "RLRRRLLLRLRLLRLRLLRLRLLLLLRRLRLRLRLRLLRRRLRRLLLLLRRRLRRLRLRRLLLLRLRRLRLRRLRRLRLLRRLRLRRRLRLLRLLLLRLLLLRLLRRLLRRRRLLRLLRLRLRRLL";
			int A                     = 48658960;
			int B                     = 221863772;
			int C                     = 385355602;
			int D                     = 393787970;
			int N                     = 980265;
			long long expected__      = 59619692663LL;

			clock_t start__           = clock();
			long long received__      = LeftAndRightHandedDiv1().countSwaps(Y, A, B, C, D, N);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}

		// custom cases

/*      case 6: {
			string Y                  = ;
			int A                     = ;
			int B                     = ;
			int C                     = ;
			int D                     = ;
			int N                     = ;
			long long expected__      = ;

			clock_t start__           = clock();
			long long received__      = LeftAndRightHandedDiv1().countSwaps(Y, A, B, C, D, N);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
/*      case 7: {
			string Y                  = ;
			int A                     = ;
			int B                     = ;
			int C                     = ;
			int D                     = ;
			int N                     = ;
			long long expected__      = ;

			clock_t start__           = clock();
			long long received__      = LeftAndRightHandedDiv1().countSwaps(Y, A, B, C, D, N);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
/*      case 8: {
			string Y                  = ;
			int A                     = ;
			int B                     = ;
			int C                     = ;
			int D                     = ;
			int N                     = ;
			long long expected__      = ;

			clock_t start__           = clock();
			long long received__      = LeftAndRightHandedDiv1().countSwaps(Y, A, B, C, D, N);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
		default:
			return -1;
		}
	}
}
 

int main(int argc, char *argv[]) {
	if (argc == 1) {
		moj_harness::run_test();
	} else {
		for (int i=1; i<argc; ++i)
			moj_harness::run_test(atoi(argv[i]));
	}
}
// END CUT HERE
//}}}
