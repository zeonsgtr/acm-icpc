import java.util.*;

public class LittleElephantAndIntervalsDiv1 {
    public long getNumber(int M, int[] L, int[] R) {
		int[] v = new int[L.length * 2];
		int n = 0;
		for (int i = 0; i < L.length; i++) {
			v[n++] = L[i];
			v[n++] = R[i] + 1;
			R[i]++;
		}
		Arrays.sort(v, 0, n);
		int m = 1;
		for (int i = 1; i < n; i++) {
			if (v[i] != v[i - 1]) {
				v[m++] = v[i];
			}
		}
		n = m;
		boolean[] used = new boolean[n];
		long result = 1;
		for (int i = L.length - 1; i >= 0; i--) {
			int cnt = 0;
			for (int j = 0; j < n - 1; j++) {
				if (L[i] <= v[j] && v[j + 1] <= R[i] && !used[j]) {
					used[j] = true;
					cnt++;
				}
			}
			if (cnt > 0) {
				result *= 2;
			}
		}
		return result;
    }

//{{{
}

//}}}


// Powered by FileEdit
// Powered by moj 4.16 [modified TZTester]
// Powered by CodeProcessor
