import java.util.*;

public class LittleElephantAndRGB {
    public long getNumber(String[] list, int minGreen) {
        StringBuilder buff = new StringBuilder();
        for (int i = 0; i < list.length; i++) {
            buff = buff.append(list[i]);
        }
        int n = buff.length();
        char[] s = new char[n];
        for (int i = 0; i < n; i++) {
            s[i] = buff.charAt(i);
        }
        boolean[][] ok = new boolean[n][n];
        int[][] prefix = new int[n][n];
        int[][] suffix = new int[n][n];
        for (int i = 0; i < n; i++) {
            prefix[i][i] = suffix[i][i] = s[i] == 'G' ? 1 : 0;
            ok[i][i] = s[i] == 'G' && minGreen == 1 ? true : false;
        }
        for (int k = 1; k < n; k++) {
            for (int i = 0; i + k < n; i++) {
                int j = i + k;
                ok[i][j] = ok[i + 1][j] || ok[i][j - 1];
                prefix[i][j] = s[i] == 'G' ? 1 + prefix[i + 1][j] : 0;
                suffix[i][j] = s[j] == 'G' ? 1 + suffix[i][j - 1] : 0;
                ok[i][j] |= prefix[i][j] >= minGreen || suffix[i][j] >= minGreen;
            }
        }
        int[] invalidCount = new int[n + 1];
        int[] validCount = new int[n + 1];
        int[] sum = new int[n + 1];
        for (int i = 1; i < n; i++) {
            for (int j = i; j < n; j++) {
                if (ok[i][j]) {
                    validCount[i]++;
                } else {
                    invalidCount[i]++;
                    sum[prefix[i][j]]++;
                }
            }
        }
        for (int i = n - 1; i >= 0; i--) {
            invalidCount[i] += invalidCount[i + 1];
            validCount[i] += validCount[i + 1];
            sum[i] += sum[i + 1];
        }
        long result = 0;
        int[] pre = new int[n + 1];
        for (int i = 0; i < n - 1; i++) {
            for (int j = i; j >= 0; j--) {
                if (ok[j][i]) {
                    result += validCount[i + 1] + invalidCount[i + 1];
                } else {
                    result += validCount[i + 1];
                    if (minGreen - suffix[j][i] <= n) {
                        result += sum[minGreen - suffix[j][i]];
                    }
                }
            }
            Arrays.fill(pre, 0);
            for (int j = i + 1; j < n; j++) {
                if (!ok[i + 1][j]) {
                    pre[prefix[i + 1][j]]++;
                }
            }
            for (int j = n - 1; j >= 0; j--) {
                pre[j] += pre[j + 1];
                sum[j] -= pre[j];
            }
        }
        return result;
    }

//{{{
// BEGIN CUT HERE
   public static void main(String[] args) {
		if (args.length == 0) {
			LittleElephantAndRGBHarness.run_test(-1);
		} else {
			for (int i=0; i<args.length; ++i)
				LittleElephantAndRGBHarness.run_test(Integer.valueOf(args[i]));
		}
	}
// END CUT HERE
}

// BEGIN CUT HERE
class LittleElephantAndRGBHarness {
	public static void run_test(int casenum) {
		if (casenum != -1) {
			if (runTestCase(casenum) == -1)
				System.err.println("Illegal input! Test case " + casenum + " does not exist.");
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = runTestCase(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			System.err.println("No test cases run.");
		} else if (correct < total) {
			System.err.println("Some cases FAILED (passed " + correct + " of " + total + ").");
		} else {
			System.err.println("All " + total + " tests passed!");
		}
	}
	
	static boolean compareOutput(long expected, long result) { return expected == result; }
	static String formatResult(long res) {
		return String.format("%d", res);
	}
	
	static int verifyCase(int casenum, long expected, long received) { 
		System.err.print("Example " + casenum + "... ");
		if (compareOutput(expected, received)) {
			System.err.println("PASSED");
			return 1;
		} else {
			System.err.println("FAILED");
			System.err.println("    Expected: " + formatResult(expected)); 
			System.err.println("    Received: " + formatResult(received)); 
			return 0;
		}
	}

	static int runTestCase(int casenum) {
		switch(casenum) {
		case 0: {
			String[] list             = {"GRG"};
			int minGreen              = 2;
			long expected__           = 1L;

			return verifyCase(casenum, expected__, new LittleElephantAndRGB().getNumber(list, minGreen));
		}
		case 1: {
			String[] list             = {"GG", "GG"};
			int minGreen              = 3;
			long expected__           = 9L;

			return verifyCase(casenum, expected__, new LittleElephantAndRGB().getNumber(list, minGreen));
		}
		case 2: {
			String[] list             = {"GRBGRBBRG"};
			int minGreen              = 2;
			long expected__           = 11L;

			return verifyCase(casenum, expected__, new LittleElephantAndRGB().getNumber(list, minGreen));
		}
		case 3: {
			String[] list             = {"RRBRBBRRR", "R", "B"};
			int minGreen              = 1;
			long expected__           = 0L;

			return verifyCase(casenum, expected__, new LittleElephantAndRGB().getNumber(list, minGreen));
		}
		case 4: {
			String[] list             = {"GRGGGRBRGG", "GGGGGGGG", "BRGRBRB"};
			int minGreen              = 4;
			long expected__           = 12430L;

			return verifyCase(casenum, expected__, new LittleElephantAndRGB().getNumber(list, minGreen));
		}

		// custom cases

/*      case 5: {
			String[] list             = ;
			int minGreen              = ;
			long expected__           = L;

			return verifyCase(casenum, expected__, new LittleElephantAndRGB().getNumber(list, minGreen));
		}*/
/*      case 6: {
			String[] list             = ;
			int minGreen              = ;
			long expected__           = L;

			return verifyCase(casenum, expected__, new LittleElephantAndRGB().getNumber(list, minGreen));
		}*/
/*      case 7: {
			String[] list             = ;
			int minGreen              = ;
			long expected__           = L;

			return verifyCase(casenum, expected__, new LittleElephantAndRGB().getNumber(list, minGreen));
		}*/
		default:
			return -1;
		}
	}
}

// END CUT HERE
//}}}
