import java.util.*;
import java.math.*;

public class GearsDiv1 {
    final int INF = (int)1e9;
    int n, m, edgeCout, source, target;
    int[] firstEdge, nextEdge, to, c;
    int[] his, num, pre, d, now;
    void addEdge(int s, int t, int x) {
        to[edgeCout] = t;
        c[edgeCout] = x;
        nextEdge[edgeCout] = firstEdge[s];
        firstEdge[s] = edgeCout++;
        to[edgeCout] = s;
        c[edgeCout] = 0;
        nextEdge[edgeCout] = firstEdge[t];
        firstEdge[t] = edgeCout++;
    }
    int minimumCut() {
        int i, j, k, result = 0, flow = INF;
        for (i = 0; i < n; i++) {
            now[i] = firstEdge[i];
            num[i] = d[i] = 0;
        }
        num[0] = n;
        i = source;
        while (d[source] < n) {
            his[i] = flow;
            for (k = now[i]; k != -1; k = nextEdge[k]) {
                if (c[k] > 0 && d[i] == d[to[k]] + 1) {
                    break;
                }
            }
            if (k != -1) {
                j = to[k];
                now[i] = k;
                pre[j] = i;
                flow = Math.min(flow, c[k]);
                i = j;
                if (i == target) {
                    result += flow;
                    while (i != source) {
                        i = pre[i];
                        c[now[i]] -= flow;
                        c[now[i] ^ 1] += flow;
                    }
                }
            } else {
                if (num[d[i]]-- == 0) {
                    break;
                }
                d[i] = n;
                for (k = now[i] = firstEdge[i]; k != -1; k = nextEdge[k]) {
                    if (c[k] > 0 && d[to[k]] + 1 < d[i]) {
                        d[i] = d[to[now[i] = k]] + 1;
                    }
                }
                num[d[i]]++;
                if (i != source) {
                    i = pre[i];
                    flow = his[i];
                }
            }
        }
        return result;
    }
    int solve(String color, String[] graph, char left, char right) {
        n = color.length() + 2;
        edgeCout = 0;
        firstEdge = new int[n];
        Arrays.fill(firstEdge, -1);
        m = 2 * n * n;
        nextEdge = new int[m];
        to = new int[m];
        c = new int[m];
        his = new int[n];
        num = new int[n + 1];
        pre = new int[n];
        d = new int[n];
        now = new int[n];
        source = n - 2;
        target = n - 1;
        for (int i = 0; i < color.length(); i++) {
            if (color.charAt(i) == left) {
                addEdge(source, i, 1);
                for (int j = 0; j < color.length(); j++) {
                    if (color.charAt(j) == right && graph[i].charAt(j) == 'Y') {
                        addEdge(i, j, 1);
                    }
                }
            }
            if (color.charAt(i) == right) {
                addEdge(i, target, 1);
            }
        }
        return minimumCut();
    }
    public int getmin(String color, String[] graph) {
        return Math.min(solve(color, graph, 'R', 'G'), Math.min(
            solve(color, graph, 'G', 'B'), solve(color, graph, 'R', 'B')));
    }
    void debug(Object...os) {
        System.err.println(Arrays.deepToString(os));
    }
//{{{
// BEGIN CUT HERE
   public static void main(String[] args) {
		if (args.length == 0) {
			GearsDiv1Harness.run_test(-1);
		} else {
			for (int i=0; i<args.length; ++i)
				GearsDiv1Harness.run_test(Integer.valueOf(args[i]));
		}
	}
// END CUT HERE
}

// BEGIN CUT HERE
class GearsDiv1Harness {
	public static void run_test(int casenum) {
		if (casenum != -1) {
			if (runTestCase(casenum) == -1)
				System.err.println("Illegal input! Test case " + casenum + " does not exist.");
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = runTestCase(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			System.err.println("No test cases run.");
		} else if (correct < total) {
			System.err.println("Some cases FAILED (passed " + correct + " of " + total + ").");
		} else {
			System.err.println("All " + total + " tests passed!");
		}
	}
	
	static boolean compareOutput(int expected, int result) { return expected == result; }
	static String formatResult(int res) {
		return String.format("%d", res);
	}
	
	static int verifyCase(int casenum, int expected, int received) { 
		System.err.print("Example " + casenum + "... ");
		if (compareOutput(expected, received)) {
			System.err.println("PASSED");
			return 1;
		} else {
			System.err.println("FAILED");
			System.err.println("    Expected: " + formatResult(expected)); 
			System.err.println("    Received: " + formatResult(received)); 
			return 0;
		}
	}

	static int runTestCase(int casenum) {
		switch(casenum) {
		case 0: {
			String color              = "RGB";
			String[] graph            = {"NYY","YNY","YYN"};
			int expected__            = 1;

			return verifyCase(casenum, expected__, new GearsDiv1().getmin(color, graph));
		}
		case 1: {
			String color              = "RGBR";
			String[] graph            = {"NNNN","NNNN","NNNN","NNNN"};
			int expected__            = 0;

			return verifyCase(casenum, expected__, new GearsDiv1().getmin(color, graph));
		}
		case 2: {
			String color              = "RGBR";
			String[] graph            = {"NYNN","YNYN","NYNY","NNYN"};
			int expected__            = 1;

			return verifyCase(casenum, expected__, new GearsDiv1().getmin(color, graph));
		}
		case 3: {
			String color              = "RRRRRGRRBGRRGBBGGGBRRRGBRGRRGG";
			String[] graph            = {"NNNNNYNNNYNNYNNNYNNNNNNNNYNNYY",  "NNNNNNNNYNNNYNYNNYNNNNYNNYNNYY",  "NNNNNYNNNNNNNNNNNNYNNNNNNYNNNY",  "NNNNNNNNNYNNYNNYYYNNNNYNNYNNNN",  "NNNNNNNNNYNNYNNYYYNNNNYNNNNNNN",  "YNYNNNYYYNNYNYYNNNNNYYNYNNYYNN",  "NNNNNYNNNNNNNNNYYYNNNNYNNYNNYY",  "NNNNNYNNNNNNNNNYNNNNNNNNNNNNYN",  "NYNNNYNNNYNNYNNYYYNNNNYNNYNNYY",  "YNNYYNNNYNNNNYYNNNYNYYNYNNNNNN",  "NNNNNNNNNNNNYNNYNYNNNNYNNNNNNY",  "NNNNNYNNNNNNYNNYYYNNNNNNNNNNYN",  "YYNYYNNNYNYYNYYNNNYNYNNYNNNNNN",  "NNNNNYNNNYNNYNNYYYNNNNYNNYNYYY",  "NYNNNYNNNYNNYNNYYYNNNNYNNYNNYY",  "NNNYYNYYYNYYNYYNNNYNYNNYYNYYNN",  "YNNYYNYNYNNYNYYNNNYNNNNYYNNYNN",  "NYNYYNYNYNYYNYYNNNNYYNNYYNYNNN",  "NNYNNNNNNYNNYNNYYNNNNNYNNYNNNY",  "NNNNNNNNNNNNNNNNNYNNNNYNNYNNNY",  "NNNNNYNNNYNNYNNYNYNNNNYNNNNNYY",  "NNNNNYNNNYNNNNNNNNNNNNYNNNNNNN",  "NYNYYNYNYNYNNYYNNNYYYYNYYNYNNN",  "NNNNNYNNNYNNYNNYYYNNNNYNNNNNNY",  "NNNNNNNNNNNNNNNYYYNNNNYNNYNNYY",  "YYYYNNYNYNNNNYYNNNYYNNNNYNYYNN",  "NNNNNYNNNNNNNNNYNYNNNNYNNYNNYN",  "NNNNNYNNNNNNNYNYYNNNNNNNNYNNYY",  "YYNNNNYYYNNYNYYNNNNNYNNNYNYYNN",  "YYYNNNYNYNYNNYYNNNYYYNNYYNNYNN"};
			int expected__            = 3;

			return verifyCase(casenum, expected__, new GearsDiv1().getmin(color, graph));
		}

		// custom cases

/*      case 4: {
			String color              = ;
			String[] graph            = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new GearsDiv1().getmin(color, graph));
		}*/
/*      case 5: {
			String color              = ;
			String[] graph            = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new GearsDiv1().getmin(color, graph));
		}*/
/*      case 6: {
			String color              = ;
			String[] graph            = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new GearsDiv1().getmin(color, graph));
		}*/
		default:
			return -1;
		}
	}
}

// END CUT HERE
//}}}
