import java.util.*;

public class GooseTattarrattatDiv1 {
    int m;
    boolean[][] g;
    void dfs(int x, boolean[] visited, int[] vector) {
        visited[x] = true;
        vector[m++] = x;
        for (int y = 0; y < 26; y++) {
            if (!visited[y] && g[x][y]) {
                dfs(y, visited, vector);
            }
        }
    }
    public int getmin(String S) {
        int n = S.length();
        int[] size = new int[26];
        for (int i = 0; i < n; i++) {
            size[S.charAt(i) - 'a']++;
        }
        g = new boolean[26][26];
        for (int i = 0; i + i < n; i++) {
            g[S.charAt(i) - 'a'][S.charAt(n - i - 1) - 'a'] = true;
            g[S.charAt(n - i - 1) - 'a'][S.charAt(i) - 'a'] = true;
        }
        int[] vector = new int[26];
        boolean[] visited = new boolean[26];
        int result = 0;
        for (int i = 0; i < 26; i++) {
            if (!visited[i] && size[i] > 0) {
                m = 0;
                dfs(i, visited, vector);
                for (int j = 0; j < m; j++) {
                    for (int k = j + 1; k < m; k++) {
                        if (size[vector[k]] < size[vector[j]]) {
                            int tmp = vector[j];
                            vector[j] = vector[k];
                            vector[k] = tmp;
                        }
                    }
                }
                int sum = 0;
                for (int j = 0; j < m - 1; j++) {
                    result += size[vector[j]];
                }
            }
        }
        return result;
    }

//{{{
// BEGIN CUT HERE
   public static void main(String[] args) {
		if (args.length == 0) {
			GooseTattarrattatDiv1Harness.run_test(-1);
		} else {
			for (int i=0; i<args.length; ++i)
				GooseTattarrattatDiv1Harness.run_test(Integer.valueOf(args[i]));
		}
	}
// END CUT HERE
}

// BEGIN CUT HERE
class GooseTattarrattatDiv1Harness {
	public static void run_test(int casenum) {
		if (casenum != -1) {
			if (runTestCase(casenum) == -1)
				System.err.println("Illegal input! Test case " + casenum + " does not exist.");
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = runTestCase(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			System.err.println("No test cases run.");
		} else if (correct < total) {
			System.err.println("Some cases FAILED (passed " + correct + " of " + total + ").");
		} else {
			System.err.println("All " + total + " tests passed!");
		}
	}
	
	static boolean compareOutput(int expected, int result) { return expected == result; }
	static String formatResult(int res) {
		return String.format("%d", res);
	}
	
	static int verifyCase(int casenum, int expected, int received) { 
		System.err.print("Example " + casenum + "... ");
		if (compareOutput(expected, received)) {
			System.err.println("PASSED");
			return 1;
		} else {
			System.err.println("FAILED");
			System.err.println("    Expected: " + formatResult(expected)); 
			System.err.println("    Received: " + formatResult(received)); 
			return 0;
		}
	}

	static int runTestCase(int casenum) {
		switch(casenum) {
		case 0: {
			String S                  = "geese";
			int expected__            = 2;

			return verifyCase(casenum, expected__, new GooseTattarrattatDiv1().getmin(S));
		}
		case 1: {
			String S                  = "tattarrattat";
			int expected__            = 0;

			return verifyCase(casenum, expected__, new GooseTattarrattatDiv1().getmin(S));
		}
		case 2: {
			String S                  = "xyyzzzxxx";
			int expected__            = 2;

			return verifyCase(casenum, expected__, new GooseTattarrattatDiv1().getmin(S));
		}
		case 3: {
			String S                  = "xrepayuyubctwtykrauccnquqfuqvccuaakylwlcjuyhyammag";
			int expected__            = 11;

			return verifyCase(casenum, expected__, new GooseTattarrattatDiv1().getmin(S));
		}
		case 4: {
			String S                  = "abaabb";
			int expected__            = 3;

			return verifyCase(casenum, expected__, new GooseTattarrattatDiv1().getmin(S));
		}

		// custom cases

/*      case 5: {
			String S                  = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new GooseTattarrattatDiv1().getmin(S));
		}*/
/*      case 6: {
			String S                  = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new GooseTattarrattatDiv1().getmin(S));
		}*/
/*      case 7: {
			String S                  = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new GooseTattarrattatDiv1().getmin(S));
		}*/
		default:
			return -1;
		}
	}
}

// END CUT HERE
//}}}
