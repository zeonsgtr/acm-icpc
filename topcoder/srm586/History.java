import java.util.*;
import java.math.*;

public class History {
    static final int INF = Integer.MAX_VALUE / 2;
    int n;
    int[][] l;
    int edgeCount, nodeCount;
    int[] firstEdge, nextEdge, to, weight;
    void addEdge(int s, int t, int w) {
        to[edgeCount] = t;
        weight[edgeCount] = w;
        nextEdge[edgeCount] = firstEdge[s];
        firstEdge[s] = edgeCount++;
    }
    int[] queue, count, dist;
    boolean[] visited;
    int front, rear;
    int nextPos(int x) {
        if (x + 1 == nodeCount) {
            x = 0;
        } else {
            x = x + 1;
        }
        return x;
    }
    void push(int x) {
        if (!visited[x]) {
            visited[x] = true;
            count[x]++;
            queue[rear = nextPos(rear)] = x;
        }
    }
    char spfa() {
        edgeCount = 0;
        queue = new int[nodeCount];
        count = new int[nodeCount];
        dist = new int[nodeCount];
        visited = new boolean[nodeCount];
        Arrays.fill(dist, INF);
        front = rear = 0;
        dist[n] = 0;
        push(n);
        while (front != rear) {
            int x = queue[front = nextPos(front)];
            if (count[x] > n) {
                return 'N';
            }
            visited[x] = false;
            for (int itr = firstEdge[x]; itr != -1; itr = nextEdge[itr]) {
                int y = to[itr];
                if (dist[x] + weight[itr] < dist[y]) {
                    dist[y] = dist[x] + weight[itr];
                    push(y);
                }
            }
        }
        return 'Y';
    }
    char solve(String[] battles) {
        nodeCount = n + 1;
        firstEdge = new int[nodeCount];
        Arrays.fill(firstEdge, -1);
        int maxEdge = 2 * (battles.length + 1) + n;
        nextEdge = new int[maxEdge];
        weight = new int[maxEdge];
        to = new int[maxEdge];
        for (int itr = 0; itr < battles.length; itr++) {
            String battle = battles[itr];
            int a = battle.charAt(0) - 'A';
            int i = battle.charAt(1) - '0';
            int b = battle.charAt(3) - 'A';
            int j = battle.charAt(4) - '0';
            addEdge(b, a, -l[a][i] + l[b][j + 1] - 1);
            addEdge(a, b, -l[b][j] + l[a][i + 1] - 1);
        }
        for (int i = 0; i < n; i++) {
            addEdge(n, i, 0);
        }
        return spfa();
    }
    public String verifyClaims(String[] dynasties, String[] battles, String[] queries) {
        n = dynasties.length;
        l = new int[n][];
        for (int i = 0; i < n; i++) {
            StringTokenizer tokenizer = new StringTokenizer(dynasties[i]);
            int m = 0;
            int[] buffer = new int[11];
            while (tokenizer.hasMoreTokens()) {
                buffer[m++] = Integer.parseInt(tokenizer.nextToken());
            }
            l[i] = new int[m];
            for (int j = 0; j < m; j++) {
                l[i][j] = buffer[j] - buffer[0];
            }
        }
        StringBuilder all = new StringBuilder();
        for (int i = 0; i < battles.length; i++) {
            all.append(battles[i]);
        }
        int size = 1;
        for (int i = 0; i < all.length(); i++) {
            if (all.charAt(i) == ' ') {
                size++;
            }
        }
        int m = 0;
        String[] tmp = new String[size + 1];
        StringTokenizer tokenizer = new StringTokenizer(new String(all));
        while (tokenizer.hasMoreTokens()) {
            tmp[m++] = tokenizer.nextToken();
        }
        StringBuilder answer = new StringBuilder();
        for (int i = 0; i < queries.length; i++) {
            tmp[size] = queries[i];
            answer.append(solve(tmp));
        }
        return new String(answer);
    }
    void debug(Object...os) {
        System.err.println(Arrays.deepToString(os));
    }
// BEGIN CUT HERE
//{{{
   public static void main(String[] args) {
		if (args.length == 0) {
			HistoryHarness.run_test(-1);
		} else {
			for (int i=0; i<args.length; ++i)
				HistoryHarness.run_test(Integer.valueOf(args[i]));
		}
	}
// END CUT HERE
}

// BEGIN CUT HERE
class HistoryHarness {
	public static void run_test(int casenum) {
		if (casenum != -1) {
			if (runTestCase(casenum) == -1)
				System.err.println("Illegal input! Test case " + casenum + " does not exist.");
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = runTestCase(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			System.err.println("No test cases run.");
		} else if (correct < total) {
			System.err.println("Some cases FAILED (passed " + correct + " of " + total + ").");
		} else {
			System.err.println("All " + total + " tests passed!");
		}
	}
	
	static boolean compareOutput(String expected, String result) { return expected.equals(result); }
	static String formatResult(String res) {
		return String.format("\"%s\"", res);
	}
	
	static int verifyCase(int casenum, String expected, String received) { 
		System.err.print("Example " + casenum + "... ");
		if (compareOutput(expected, received)) {
			System.err.println("PASSED");
			return 1;
		} else {
			System.err.println("FAILED");
			System.err.println("    Expected: " + formatResult(expected)); 
			System.err.println("    Received: " + formatResult(received)); 
			return 0;
		}
	}

	static int runTestCase(int casenum) {
		switch(casenum) {
		case 0: {
			String[] dynasties        = {"1 2 4",  "1 2 3"};
			String[] battles          = {"A1-B0"};
			String[] queries          = {"A0-B0",  "A0-B1",  "A1-B0",  "A1-B1"};
			String expected__         = "NNYY";

			return verifyCase(casenum, expected__, new History().verifyClaims(dynasties, battles, queries));
		}
		case 1: {
			String[] dynasties        = {"1000 2000 3000 10000",  "600 650 2000",  "1 1001 20001"};
			String[] battles          = {"B1-C0 A0-B0 A2-C1 B1-C1"};
			String[] queries          = {"A0-B1",  "A1-B1",  "A2-B1",  "C0-A0",  "B0-A2",  "C1-B0"};
			String expected__         = "YYYYNN";

			return verifyCase(casenum, expected__, new History().verifyClaims(dynasties, battles, queries));
		}
		case 2: {
			String[] dynasties        = {"1 4 5",  "10 13 17"};
			String[] battles          = {"A0-B0 A0-B0 B0-A0"};
			String[] queries          = {"A1-B0",  "A0-B1",  "A1-B1"};
			String expected__         = "YYY";

			return verifyCase(casenum, expected__, new History().verifyClaims(dynasties, battles, queries));
		}
		case 3: {
			String[] dynasties        = {"1 5 6",  "1 2 5"};
			String[] battles          = {"A0",  "-B0 A",  "1-B1"};
			String[] queries          = {"A0-B0",  "A1-B0",  "A0-B1",  "A1-B1"};
			String expected__         = "YNYY";

			return verifyCase(casenum, expected__, new History().verifyClaims(dynasties, battles, queries));
		}
		case 4: {
			String[] dynasties        = {"2294 7344","366 384 449 965 1307 1415","307 473 648 688 1097","1145 1411 1569 2606","87 188 551 598 947 998 1917 1942"} ;
			String[] battles          = {"A0-B4 B4-E2 B3-E2 D2-E4 A0-E4 B1-C3 A0-E3 A0-E6 D0","-E2 B2-E1 B4-E3 B4-D0 D0-E3 A0-D1 B2-C3 B1-C3 B4-E","3 D0-E1 B3-D0 B3-E2"} ;
			String[] queries          = {"A0-C2","E6-C2","A0-E4","B3-C1","C0-D2","B0-C1","D1-C3","C3-D0","C1-E3","D1-A0"};
			String expected__         = "YNYNNYNNNY";

			return verifyCase(casenum, expected__, new History().verifyClaims(dynasties, battles, queries));
		}

		// custom cases

/*      case 5: {
			String[] dynasties        = ;
			String[] battles          = ;
			String[] queries          = ;
			String expected__         = ;

			return verifyCase(casenum, expected__, new History().verifyClaims(dynasties, battles, queries));
		}*/
/*      case 6: {
			String[] dynasties        = ;
			String[] battles          = ;
			String[] queries          = ;
			String expected__         = ;

			return verifyCase(casenum, expected__, new History().verifyClaims(dynasties, battles, queries));
		}*/
/*      case 7: {
			String[] dynasties        = ;
			String[] battles          = ;
			String[] queries          = ;
			String expected__         = ;

			return verifyCase(casenum, expected__, new History().verifyClaims(dynasties, battles, queries));
		}*/
		default:
			return -1;
		}
	}
}

//}}}
// END CUT HERE
