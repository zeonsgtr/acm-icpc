import java.util.*;
import java.math.*;

public class PiecewiseLinearFunction {
    public int maximumSolutions(int[] Y) {
        int n = Y.length, m;
        for (int i = 0; i < n; i++) {
            Y[i] *= 2;
        }
        int[] values = new int[n * 2];
        for (int i = 0; i < n; i++) {
            values[i] = Y[i];
        }
        Arrays.sort(values, 0, n);
        for (int i = m = 1; i < n; i++) {
            if (values[i] != values[i - 1]) {
                values[m++] = values[i];
            }
        }
        for (int i = 0; i < m - 1; i++) {
            values[i + m] = (int)((1L * values[i] + values[i + 1]) / 2);
        }
        m = m + m - 1;
        int result = 0;
        for (int i = 0; i < m; i++) {
            TreeSet<Integer> set = new TreeSet<Integer>();
            int count = 0;
            for (int j = 1; j < n; j++) {
                if (Y[j - 1] == Y[j]) {
                    return -1;
                }
                if (Math.min(Y[j - 1], Y[j]) < values[i] && values[i] < Math.max(Y[j - 1], Y[j])) {
                    count++;
                } else if (Y[j - 1] == values[i]) {
                    set.add(j - 1);
                } else if (Y[j] == values[i]) {
                    set.add(j);
                }
            }
            result = Math.max(result, set.size() + count);
        }
        return result;
    }
    void debug(Object...os) {
        System.err.println(Arrays.deepToString(os));
    }
// BEGIN CUT HERE
//{{{
   public static void main(String[] args) {
		if (args.length == 0) {
			PiecewiseLinearFunctionHarness.run_test(-1);
		} else {
			for (int i=0; i<args.length; ++i)
				PiecewiseLinearFunctionHarness.run_test(Integer.valueOf(args[i]));
		}
	}
// END CUT HERE
}

// BEGIN CUT HERE
class PiecewiseLinearFunctionHarness {
	public static void run_test(int casenum) {
		if (casenum != -1) {
			if (runTestCase(casenum) == -1)
				System.err.println("Illegal input! Test case " + casenum + " does not exist.");
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = runTestCase(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			System.err.println("No test cases run.");
		} else if (correct < total) {
			System.err.println("Some cases FAILED (passed " + correct + " of " + total + ").");
		} else {
			System.err.println("All " + total + " tests passed!");
		}
	}
	
	static boolean compareOutput(int expected, int result) { return expected == result; }
	static String formatResult(int res) {
		return String.format("%d", res);
	}
	
	static int verifyCase(int casenum, int expected, int received) { 
		System.err.print("Example " + casenum + "... ");
		if (compareOutput(expected, received)) {
			System.err.println("PASSED");
			return 1;
		} else {
			System.err.println("FAILED");
			System.err.println("    Expected: " + formatResult(expected)); 
			System.err.println("    Received: " + formatResult(received)); 
			return 0;
		}
	}

	static int runTestCase(int casenum) {
		switch(casenum) {
		case 0: {
			int[] Y                   = {3, 2};
			int expected__            = 1;

			return verifyCase(casenum, expected__, new PiecewiseLinearFunction().maximumSolutions(Y));
		}
		case 1: {
			int[] Y                   = {4, 4};
			int expected__            = -1;

			return verifyCase(casenum, expected__, new PiecewiseLinearFunction().maximumSolutions(Y));
		}
		case 2: {
			int[] Y                   = {1, 4, -1, 2};
			int expected__            = 3;

			return verifyCase(casenum, expected__, new PiecewiseLinearFunction().maximumSolutions(Y));
		}
		case 3: {
			int[] Y                   = {2, 1, 2, 1, 3, 2, 3, 2};
			int expected__            = 5;

			return verifyCase(casenum, expected__, new PiecewiseLinearFunction().maximumSolutions(Y));
		}
		case 4: {
			int[] Y                   = {125612666, -991004227, 0, 6, 88023, -1000000000, 1000000000, -1000000000, 1000000000};
			int expected__            = 6;

			return verifyCase(casenum, expected__, new PiecewiseLinearFunction().maximumSolutions(Y));
		}

		// custom cases

        case 5: {
			int[] Y                   = {1, 2, 1, 2, 1};
			int expected__            = 4;

			return verifyCase(casenum, expected__, new PiecewiseLinearFunction().maximumSolutions(Y));
		}  
/*      case 6: {
			int[] Y                   = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new PiecewiseLinearFunction().maximumSolutions(Y));
		}*/
/*      case 7: {
			int[] Y                   = ;
			int expected__            = ;

			return verifyCase(casenum, expected__, new PiecewiseLinearFunction().maximumSolutions(Y));
		}*/
		default:
			return -1;
		}
	}
}

//}}}
// END CUT HERE
