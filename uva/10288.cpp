#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

typedef long long int64;

int64 gcd(int64 a, int64 b) {
    return b ? gcd(b, a % b) : a;
}

int length(int64 x) {
    return x == 0 ? 0 : length(x / 10) + 1;
}

int main() {
    int n;
    while (scanf("%d", &n) == 1) {
        int64 a = 0, b = 1;
        for (int i = 1; i <= n; i++) {
            a = a * i + b;
            b = b * i;
            int64 g = gcd(a, b);
            a /= g;
            b /= g;
        }
        a *= n;
        int64 g = gcd(a, b);
        a /= g;
        b /= g;
        if (a % b == 0) {
            printf("%lld\n", a / b);
        } else {
            int64 t = a / b;
            a %= b;
            printf("%s%lld\n", std::string(length(t) + 1, ' ').c_str(), a);
            printf("%lld %s\n", t, std::string(length(b), '-').c_str());
            printf("%s %lld\n", std::string(length(t), ' ').c_str(), b);
        }
    }
}

