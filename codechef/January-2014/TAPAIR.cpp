#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <map>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())
#define FOR(itr, x) for (__typeof((x).end()) itr = (x).begin(); itr != (x).end(); itr++)

typedef long long int64;
typedef unsigned long long Integer;
typedef std::pair <int, int> Pair;

const int N = 100000 + 10;
const int M = 300000 + 10;
const int MAGIC = (int)1e6 + 3;

int n, m;
int depth[N], ancestor_edge[N];
int vertex_count, cut_edge_count;
bool visited[N];
std::vector <int> adj[N], id[N];
std::vector <Integer> all;
Integer value[N], power[M];
int64 result;

void dfs(int x, int parent, int d) {
    depth[x] = d;
    value[x] = 0ULL;
    visited[x] = true;
    vertex_count++;
    for (int itr = 0; itr < SIZE(adj[x]); itr++) {
        int y = adj[x][itr];
        if (y == parent) {
            continue;
        }
        if (visited[y]) {
            if (depth[y] < depth[x]) {
                ancestor_edge[x]++;
                value[x] += power[id[x][itr]];
                value[y] -= power[id[x][itr]];
            } else {
                ancestor_edge[x]--;
            }
        } else {
            dfs(y, x, d + 1);
            ancestor_edge[x] += ancestor_edge[y];
            value[x] += value[y];
        }
    }
    if (x > 0) {
        if (ancestor_edge[x] == 0) {
            cut_edge_count++;
        } else {
            result += ancestor_edge[x] == 1;
            all.push_back(value[x]);
        }
    }
}

int main() {
    power[0] = 1ULL;
    for (int i = 1; i < M; i++) {
        power[i] = power[i - 1] * MAGIC;
    }

    scanf("%d%d", &n, &m);
    for (int i = 0; i < m; i++) {
        int s, t;
        scanf("%d%d", &s, &t);
        s--, t--;
        adj[s].push_back(t);
        adj[t].push_back(s);
        id[s].push_back(i + 1);
        id[t].push_back(i + 1);
    }
    std::fill(visited, visited + n, false);
    dfs(0, -1, 0);
    if (vertex_count < n) {
        printf("%lld\n", 1LL * m * (m - 1) / 2);
        return 0;
    }
    result += 1LL * cut_edge_count * (m - 1);
    result -= 1LL * cut_edge_count * (cut_edge_count - 1) / 2;
    std::sort(ALL(all));
    for (int i = 0, j = 0; i < SIZE(all); i = j) {
        for (j = i; j < SIZE(all) && all[j] == all[i]; j++);
        result += 1LL * (j - i) * (j - i - 1) / 2;
    }
    printf("%lld\n", result);
}

