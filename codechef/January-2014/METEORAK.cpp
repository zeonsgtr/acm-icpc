#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

const int N = 1000 + 10;

int n, m, k;
bool map[N][N];
int height[N][N];
int maximum[N][N];
int answer[N][N];
int stack[N], top;
int l[N], r[N];

void solve() {
    for (int i = 1; i <= n; i++) {
        top = 0;
        for (int j = 1; j <= m; j++) {
            height[i][j] = map[i][j] ? height[i - 1][j] + 1 : 0;
            l[j] = j;
            for (; top > 0 && height[i][stack[top]] >= height[i][j]; top--) {
                l[j] = l[stack[top]];
            }
            stack[++top] = j;
        }
        top = 0;
        for (int j = m; j >= 1; j--) {
            r[j] = j;
            for (; top > 0 && height[i][stack[top]] >= height[i][j]; top--) {
                r[j] = r[stack[top]];
            }
            stack[++top] = j;
            if (height[i][j] > 0) {
                int u = i - height[i][j] + 1;
                int v = i;
                maximum[u][v] = std::max(maximum[u][v], r[j] - l[j] + 1);
            }
        }
    }
    for (int i = 1; i <= n; i++) {
        for (int j = n; j >= i; j--) {
            maximum[i][j] = std::max(maximum[i][j], maximum[i - 1][j]);
            maximum[i][j] = std::max(maximum[i][j], maximum[i][j + 1]);
            answer[i][j] = maximum[i][j] * (j - i + 1);
        }
    }
    for (int i = n; i >= 1; i--) {
        for (int j = i; j <= n; j++) {
            answer[i][j] = std::max(answer[i][j], answer[i + 1][j]);
            answer[i][j] = std::max(answer[i][j], answer[i][j - 1]);
        }
    }
}

int main() {
    scanf("%d%d%d", &n, &m, &k);
    memset(map, true, sizeof(map));
    for (int i = 0; i < k; i++) {
        int x, y;
        scanf("%d%d", &x, &y);
        map[x][y] = false;
    }
    solve();
    int q;
    scanf("%d", &q);
    while (q--) {
        int s, t;
        scanf("%d%d", &s, &t);
        printf("%d\n", answer[s][t]);
    }
}

