#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const int N = int(1e5) + 10;

std::vector <int> values;

struct Node {
    int sum;
    Node* children[2];
    Node(Node* a, Node* b, int s) {
        children[0] = a;
        children[1] = b;
        sum = s;
    }
    Node* insert(int, int, int);
    int query(int, int, int);
};

Node *root[N], *null;

Node* Node::insert(int s, int t, int x) {
    if (s == t) {
        return new Node(null, null, sum + x);
    }
    int mid = s + t >> 1;
    if (x <= values[mid]) {
        return new Node(children[0]->insert(s, mid, x), children[1], sum + x);
    }
    return new Node(children[0], children[1]->insert(mid + 1, t, x), sum + x);
}

int Node::query(int s, int t, int x) {
    if (s == t) {
        return values[s] <= x ? sum : 0;
    }
    int mid = s + t >> 1;
    if (values[mid] < x) {
        return children[0]->sum + children[1]->query(mid + 1, t, x);
    }
    return children[0]->query(s, mid, x);
}

int n, m, a[N];

int main() {
    scanf("%d", &n);
    for (int i = 1; i <= n; i++) {
        scanf("%d", a + i);
        values.push_back(a[i]);
    }
    std::sort(ALL(values));
    values.erase(std::unique(ALL(values)), values.end());
    null = new Node(NULL, NULL, 0);
    null->children[0] = null->children[1] = null;
    root[0] = null;
    for (int i = 1; i <= n; i++) {
        root[i] = root[i - 1]->insert(0, SIZE(values) - 1, a[i]);
    }
    scanf("%d", &m);
    for (int i = 0; i < m; i++) {
        int s, t;
        scanf("%d%d", &s, &t);
        int ans = 0;
        while (true) {
            int sum = root[t]->query(0, SIZE(values) - 1, ans + 1)
                - root[s - 1]->query(0, SIZE(values) - 1, ans + 1);
            if (sum >= ans + 1) {
                ans = sum;
            } else {
                break;
            }
        }
        printf("%d\n", ans + 1);
    }
}

