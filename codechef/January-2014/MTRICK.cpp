#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

typedef long long int64;

int64 mod;

int64 multiply_mod(int64 x, int64 y) {
    int64 ret = 0, val = x % mod;
    for (; y > 0; y >>= 1) {
        if ((y & 1) && (ret += val) >= mod) {
            ret -= mod;
        }
        if ((val += val) >= mod) {
            val -= mod;
        }
    }
    return ret;
}

const int N = 1000 + 10;

struct Node {
    int64 add, mul;
};

int n;
int64 value[N], a, b;
char buf[N];
Node tree[N * 4];

void build(int k, int s, int t) {
    tree[k].add = 0;
    tree[k].mul = 1;
    if (s == t) {
        return;
    }
    int mid = s + t >> 1;
    build(k << 1, s, mid);
    build(k << 1 | 1, mid + 1, t);
}

void maintain(int k, int64 mul, int64 add) {
    tree[k].mul = multiply_mod(tree[k].mul, mul);
    tree[k].add = multiply_mod(tree[k].add, mul);
    if ((tree[k].add += add) >= mod) {
        tree[k].add %= mod;
    }
}

void push_down(int k) {
    if (tree[k].mul != 1 || tree[k].add != 0) {
        maintain(k << 1, tree[k].mul, tree[k].add);
        maintain(k << 1 | 1, tree[k].mul, tree[k].add);
        tree[k].mul = 1;
        tree[k].add = 0;
    }
}

void modify(int k, int s, int t, int l, int r, int ctrl) {
    if (r < s || l > t) {
        return;
    }
    if (l <= s && t <= r) {
        maintain(k, (ctrl == 1 ? b : 1), (ctrl == 0 ? a : 0));
        return;
    }
    push_down(k);
    int mid = s + t >> 1;
    modify(k << 1, s, mid, l, r, ctrl);
    modify(k << 1 | 1, mid + 1, t, l, r, ctrl);
}

int64 query(int k, int s, int t, int p) {
    if (s == t) {
        return (multiply_mod(value[p], tree[k].mul) + tree[k].add) % mod;
    }
    push_down(k);
    int mid = s + t >> 1;
    return p <= mid ? query(k << 1, s, mid, p) : query(k << 1 | 1, mid + 1, t, p);
}

int main() {
    int test_count;
    scanf("%d", &test_count);
    while (test_count--) {
        scanf("%d", &n);
        for (int i = 1; i <= n; i++) {
            scanf("%lld", value + i);
        }
        scanf("%lld%lld%lld", &a, &b, &mod);
        build(1, 1, n);
        scanf("%s", buf + 1);
        int begin = 1, end = n;
        for (int i = 1; i <= n; i++) {
            if (buf[i] == 'R') {
                std::swap(begin, end);
            } else {
                modify(1, 1, n, std::min(begin, end), std::max(begin, end), buf[i] == 'M');
            }
            printf("%lld%c", query(1, 1, n, begin), i < n ? ' ' : '\n');
            begin += begin <= end ? 1 : -1;
        }
    }
}

