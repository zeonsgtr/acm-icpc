#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

char buf[1111111];

int main() {
    gets(buf);
    while (gets(buf)) {
        int n = strlen(buf);
        bool have = false;
        for (int i = 0; i + 3 <= n; i++) {
            if (buf[i] == '0' && buf[i + 1] == '1' && buf[i + 2] == '0' || buf[i] == '1' && buf[i + 1] == '0' && buf[i + 2] == '1') {
                have = true;
                break;
            }
        }
        puts(!have ? "Bad" : "Good");
    }
}

