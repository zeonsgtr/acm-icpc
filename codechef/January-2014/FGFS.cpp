#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <map>

#define SIZE(x) ((int)(x).size())

typedef std::pair <int, int> Pair;

int n, m;
std::map <int, std::vector <Pair> > map;

bool cmp(const Pair& a, const Pair& b) {
    return a.second < b.second || a.second == b.second && a.first < b.first;
}

int solve(std::vector <Pair> v) {
    std::sort(v.begin(), v.end(), cmp);
    std::vector <int> dp(SIZE(v), 1);
    int result = 1;
    for (int i = 1; i < SIZE(v); i++) {
        int lower = 0, upper = i - 1, pos = -1;
        while (lower <= upper) {
            int mid = lower + upper >> 1;
            if (v[mid].second <= v[i].first) {
                pos = mid;
                lower = mid + 1;
            } else {
                upper = mid - 1;
            }
        }
        if (pos != -1) {
            dp[i] = std::max(dp[i], dp[pos] + 1);
        }
        result = dp[i] = std::max(dp[i], dp[i - 1]);
    }
    return result;
}

int main() {
    int test_count;
    scanf("%d", &test_count);
    while (test_count--) {
        map.clear();
        scanf("%d%d", &n, &m);
        for (int i = 0; i < n; i++) {
            int s, t, p;
            scanf("%d%d%d", &s, &t, &p);
            map[p].push_back(Pair(s, t));
        }
        int result = 0;
        for (std::map <int, std::vector <Pair> >::iterator itr = map.begin(); itr != map.end(); itr++) {
            result += solve(itr->second);
        }
        printf("%d\n", result);
    }
}

