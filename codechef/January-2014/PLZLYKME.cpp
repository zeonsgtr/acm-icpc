#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

int main() {
    int test_count;
    scanf("%d", &test_count);
    while (test_count--) {
        int l, d, s, c;
        scanf("%d%d%d%d", &l, &d, &s, &c);
        long long t = s;
        for (int i = 2; i <= d && t < l; i++, t *= c + 1);
        puts(t >= l ? "ALIVE AND KICKING" : "DEAD AND ROTTING");
    }
}

