#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

const int MOD = int(1e9) + 7;

int pow_mod(int a, int b) {
	int ret = 1;
	for (; b; b >>= 1, a = 1LL * a * a % MOD) {
		if (b & 1) {
			ret = 1LL * ret * a % MOD;
		}
	}
	return ret;
}

int main() {
	int test_count;
	scanf("%d", &test_count);
	while (test_count--) {
		int n;
		scanf("%d", &n);
		int p = 0;
		for (int i = 25; i >= 0; i--) {
			p = (1LL * p * 10) % (MOD - 1);
			if (n >> i & 1) {
				p = (p + 1) % (MOD - 1);
			}
		}
		p = pow_mod(2, p);
		printf("%d\n", int(1LL * p * p % MOD));
	}
}

