#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <bitset>

#define SIZE(x) ((int)(x).size())
#define ALL(x) (x).begin(), (x).end()

typedef long long int64;

const int64 U = int64(1e12);
const int N = int(1e6) + 10;

int n;
bool isprime[N];
std::vector <int64> values, primes;
std::bitset <N> v;

int main() {
    std::fill(isprime + 2, isprime + N, true);
    for (int i = 2; i < N; i++) {
        if (isprime[i]) {
            primes.push_back(i);
            for (int j = i + i; j < N; j += i) {
                isprime[j] = false;
            }
        }
    }
    for (int i = 2; i < N; i++) {
        if (isprime[i]) {
            int64 t = 1LL * i * i;
            int k = 3;
            for (; t <= U; t *= i, k++) {
                if (isprime[k]) {
                    values.push_back(t);
                }
            }
        }
    }

    int test_count;
    scanf("%d", &test_count);
    while (test_count--) {
        int64 l, r;
        std::cin >> l >> r;
        int ans = 0;
        v.set();
        for (int i = 0; i < SIZE(values); i++) {
            if (l <= values[i] && values[i] <= r) {
                ans++;
            }
        }
        for (int i = 0; i < SIZE(primes); i++) {
            if (primes[i] <= r) {
                int64 p = primes[i] * primes[i];
                if (p < l) {
                    p = (l + primes[i] - 1) / primes[i] * primes[i];
                }
                for (; p <= r; p += primes[i]) {
                    v[p - l] = false;
                }
            }
        }
        for (int64 i = l; i <= r; i++) {
            if (v[i - l] && i > 1) {
                ans++;
            }
        }
        printf("%d\n", ans);
    }
}

