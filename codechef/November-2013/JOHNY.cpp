#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

const int N = 1000;

int n, k;
std::pair <int, int> a[N];

int main() {
    int testCount;
    scanf("%d", &testCount);
    while (testCount--) {
        scanf("%d", &n);
        for (int i = 0; i < n; i++) {
            scanf("%d", &a[i].first);
            a[i].second = i;
        }
        std::sort(a, a + n);
        scanf("%d", &k);
        k--;
        for (int i = 0; i < n; i++) {
            if (a[i].second == k) {
                printf("%d\n", i + 1);
                break;
            }
        }
    }
}

