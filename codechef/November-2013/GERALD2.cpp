#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <set>

const int N = 200000 + 10;
const int L = 19;

struct AjacentList {
    int first_edge[N], next_edge[N << 1], to[N << 1], edge_count;
    bool valid[N << 1];
    void clear(int n) {
        std::fill(first_edge, first_edge + n, -1);
        edge_count = 0;
    }
    void add_edge(int s, int t) {
        to[edge_count] = t;
        valid[edge_count] = true;
        next_edge[edge_count] = first_edge[s];
        first_edge[s] = edge_count++;
    }
    void set_false(int itr) {
        valid[itr] = false;
    }
}adj;

int n, m;
int first_edge[N], next_edge[N * L], to[N * L], which[N * L], edge_count;
int heap_count, start_id[N];
int depth[N], go[N][L];
std::set <int> heap[N * L];

inline int new_heap() {
    heap[heap_count].clear();
    return heap_count++;
}

void dfs(int x, int parent) {
    std::fill(go[x] + 1, go[x] + L, -1);
    go[x][0] = parent;
    for (int i = 0; go[x][i] != -1 && go[go[x][i]][i] != -1; i++) {
        go[x][i] = go[go[x][i]][i];
    }
}

int queue[N], dist[N], prev[N], idx[N], son_count[N], length[N], rear;
int visited[N], timestamp;

int bfs(int src) {
    rear = 0;
    timestamp++;
    visited[queue[rear++] = src] = timestamp;
    idx[x] = 0;
    son_count[x] = 1;
    for (int front = 0; front < rear; front++) {
        int x = queue[front];
        for (int itr = adj.first_edge[x]; itr != -1; itr = adj.next_edge[itr]) {
            int y = to[itr];
            if (adj.valid[itr] && visited[y] != timestamp) {
                idx[y] = x == src ? son_count[src]++ : idx[x];
                visited[y] = timestamp;
                queue[rear++] = y;
                prev[y] = x;
            }
        }
    }
    return queue[rear - 1];
}

void divide(int root) {
    for (int i = dist[root = bfs(bfs(root))] / 2; i > 0; i--, root = prev[root]);
    start_id[root] = heap_count;
    for (int i = 0; i < son_count[root]; i++, new_heap());
}

int main() {
    int test_count;
    scanf("%d", &test_count);
    while (test_count--) {
        scanf("%d%d", &n, &m);
        adj.clear(n);
        for (int i = 1; i < n; i++) {
            int j;
            scanf("%d", &j);
            j--;
            adj.add_edge(i, j);
            adj.add_edge(j, i);
        }
        heap_count = 0;
        std::fill(first_edge, first_edge + n, -1);
        edge_count = 0;
        dfs(0, -1);
        divide(0);
    }
}

