#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

inline bool ok(long long x) {
    for (; x > 0; x /= 10) {
        int t = x % 10;
        if (t != 0 && t != 1 && t != 4 && t != 9) {
            return false;
        }
    }
    return true;
}

int main() {
    std::vector <long long> v;
    long long upper = (long long)(1e10);
    for (long long i = 1; i * i <= upper; i++) {
        if (ok(i * i)) {
            v.push_back(i * i);
        }
    }

    int test_count;
    scanf("%d", &test_count);
    while (test_count--) {
        long long a, b;
        std::cin >> a >> b;
        int answer = 0;
        for (int i = 0; i < (int)v.size(); i++) {
            if (a <= v[i] && v[i] <= b) {
                answer++;
            }
        }
        printf("%d\n", answer);
    }
}

