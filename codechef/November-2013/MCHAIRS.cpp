#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

int main() {
    int test_count;
    std::cin >> test_count;
    while (test_count--) {
        int n, mod = int(1e9) + 7;
        scanf("%d", &n);
        int ans = 1, a = 2;
        for (; n; n >>= 1, a = 1LL * a * a % mod) {
            if (n & 1) {
                ans = 1LL * ans * a % mod;
            }
        }
        printf("%d\n", (ans + mod - 1) % mod);
    }
}

