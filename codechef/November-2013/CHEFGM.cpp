#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define SIZE(x) ((int)(x).size())
#define ALL(x) (x).begin(), (x).end()

typedef long long int64;

int n;
int64 sum[2];

int main() {
    int test_count;
    scanf("%d", &test_count);
    while (test_count--) {
        sum[0] = sum[1] = 0LL;
        scanf("%d", &n);
        for (int i = 0; i < n; i++) {
            std::vector <int> v;
            int m;
            scanf("%d", &m);
            while (m--) {
                int x;
                scanf("%d", &x);
                v.push_back(x);
            }
            std::sort(ALL(v));
            v.erase(std::unique(ALL(v)), v.end());
            for (int i = 0; i < SIZE(v); i++) {
                v[i] &= 1;
            }
            int64 val = 1LL << 45;
            bool flag = true;
            for (int j = 0; j < SIZE(v); j++) {
                if (flag) {
                    if (v[j] == v[0]) {
                        sum[v[j]] += val;
                    } else {
                        flag = false;
                    }
                }
                if (!flag) {
                    val >>= 1;
                    sum[v[j]] += val;
                }
            }
        }
        //std::cout << sum[0] << ' ' << sum[1] << std::endl;
        if (sum[0] > sum[1]) {
            puts("EVEN");
        } else if (sum[0] < sum[1]) {
            puts("ODD");
        } else {
            puts("DON'T PLAY");
        }
    }
}

