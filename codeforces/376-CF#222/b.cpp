#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <set>

const int N = int(1e5) + 10;

int n, m, s;
int a[N], b[N], c[N], d[N];
int bug_id[N], student_id[N];
int answer[N];

struct Comparator {
    bool operator ()(int i, int j) {
        return c[i] < c[j] || c[i] == c[j] && i < j;
    }
};

bool cmp1(int i, int j) {
    return a[i] > a[j];
}

bool cmp2(int i, int j) {
    return b[i] > b[j];
}

int count[N];

bool check(int max) {
    std::fill(count, count + n, 0);
    std::set <int, Comparator> set;
    int residual = s;
    for (int i = 0, j = 0; i < m; i++) {
        for (; j < n && b[student_id[j]] >= a[bug_id[i]]; j++) {
            set.insert(student_id[j]);
        }
        if (set.empty()) {
            return false;
        }
        int k = *set.begin();
        if (++count[k] == max) {
            set.erase(k);
        }
        answer[bug_id[i]] = k;
        residual -= c[k];
        if (residual < 0) {
            return false;
        }
        c[k] = 0;
    }
    return true;
}

int main() {
    scanf("%d%d%d", &n, &m, &s);
    for (int i = 0; i < m; i++) {
        bug_id[i] = i;
        scanf("%d", a + i);
    }
    for (int i = 0; i < n; i++) {
        student_id[i] = i;
        scanf("%d", b + i);
    }
    for (int i = 0; i < n; i++) {
        scanf("%d", c + i);
    }
    std::sort(bug_id, bug_id + m, cmp1);
    std::sort(student_id, student_id + n, cmp2);
    int answer = -1, lower = 1, upper = m; 
    while (lower <= upper) {
        int mid = lower + upper >> 1;
        memcpy(d, c, sizeof(int) * n);
        if (check(mid)) {
            answer = mid;
            upper = mid - 1;
        } else {
            lower = mid + 1;
        }
        memcpy(c, d, sizeof(int) * n);
    }
    if (answer == -1) {
        puts("NO");
    } else {
        puts("YES");
        check(answer);
        for (int i = 0; i < m; i++) {
            printf("%d%c", ::answer[i] + 1, i < m - 1 ? ' ' : '\n');
        }
    }
}

