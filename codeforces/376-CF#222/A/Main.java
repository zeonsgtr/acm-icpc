import java.util.*;
import java.io.*;
import java.math.*;

public class Main {
    int n, m;
    boolean[][] visited;
    int solve(int x, int y, int k, char[][] map) {
        if (k == 0) {
            return 0;
        }
        visited[x][y] = true;
        for (int dx = -1; dx <= 1; dx++) {
            for (int dy = -1; dy <= 1; dy++) {
                if (dx * dx + dy * dy == 1) {
                    int tx = x + dx;
                    int ty = y + dy;
                    if (0 <= tx && tx < n && 0 <= ty && ty < m && !visited[tx][ty] && map[tx][ty] == '.') {
                        k = solve(tx, ty, k, map);
                    }
                }
            }
        }
        if (k > 0) {
            map[x][y] = 'X';
            k--;
        }
        return k;
    }
    void run() {
        InputReader in = new InputReader(System.in);
        PrintWriter out = new PrintWriter(System.out);
        n = in.nextInt();
        m = in.nextInt();
        int k = in.nextInt();
        char[][] map = new char[n][];
        visited = new boolean[n][m];
        for (int i = 0; i < n; i++) {
            map[i] = in.next().toCharArray();
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (map[i][j] == '.') {
                    k = solve(i, j, k, map);
                }
            }
        }
        for (int i = 0; i < n; i++) {
            out.println(map[i]);
        }
        out.close();
    }
    public static void main(String[] args) {
        new Main().run();
    }
    void debug(Object...os) {
        System.out.println(Arrays.deepToString(os));
    }
}

class InputReader {
    BufferedReader buff;
    StringTokenizer tokenizer;
    InputReader(InputStream stream) {
        buff = new BufferedReader(new InputStreamReader(stream));
        tokenizer = null;
    }
    boolean hasNext() {
        while (tokenizer == null || !tokenizer.hasMoreTokens()) {
            try {
                tokenizer = new StringTokenizer(buff.readLine());
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }
    String next() {
        if (!hasNext()) {
            throw new RuntimeException();
        }
        return tokenizer.nextToken();
    }
    int nextInt() {
        return Integer.parseInt(next());
    }
    long nextLong() {
        return Long.parseLong(next());
    }
    double nextDouble() {
        return Double.parseDouble(next());
    }
    BigInteger nextBigInteger() {
        return new BigInteger(next());
    }
}

