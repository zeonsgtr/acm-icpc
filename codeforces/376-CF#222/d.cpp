#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define SIZE(x) ((int)(x).size())
#define ALL(x) (x).begin(), (x).end()

typedef std::pair <int, int> Pair;

const int N = int(1e5) + 10;

struct Node {
    int delta;
    Pair max;
};

int n;
int l[N], r[N], v[N];
std::vector <int> values;
std::vector <int> ievents[N * 3], devents[N * 3];
Node tree[3 * N * 4];

void update(int k) {
    tree[k].max = std::max(tree[k << 1].max, tree[k << 1 | 1].max);
}

void build(int k, int s, int t) {
    if (s == t) {
        tree[k].delta = tree[k].max.first = 0;
        tree[k].max.second = values[s];
        return;
    }
    int mid = s + t >> 1;
    build(k << 1, s, mid);
    build(k << 1 | 1, mid + 1, t);
    update(k);
}

void maintain(int k, int x) {
    tree[k].delta += x;
    tree[k].max.first += x;
}

void push_down(int k) {
    if (tree[k].delta) {
        maintain(k << 1, tree[k].delta);
        maintain(k << 1 | 1, tree[k].delta);
        tree[k].delta = 0;
    }
}

void modify(int k, int s, int t, int l, int r, int d) {
    if (r < values[s] || l > values[t]) {
        return;
    }
    if (l <= values[s] && values[t] <= r) {
        maintain(k, d);
        return;
    }
    push_down(k);
    int mid = s + t >> 1;
    modify(k << 1, s, mid, l, r, d);
    modify(k << 1 | 1, mid + 1, t, l, r, d);
    update(k);
}

Pair query(int k, int s, int t, int l, int r) {
    if (r < values[s] || l > values[t]) {
        return Pair(-1, -1);
    }
    if (l <= values[s] && values[t] <= r) {
        return tree[k].max;
    }
    int mid = s + t >> 1;
    return std::max(query(k << 1, s, mid, l, r), query(k << 1 | 1, mid + 1, t, l, r));
}

int main() {
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d%d%d", l + i, v + i, r + i);
        values.push_back(l[i]);
        values.push_back(r[i]);
        values.push_back(v[i]);
    }
    std::sort(ALL(values));
    values.erase(std::unique(ALL(values)), values.end());
    build(1, 0, SIZE(values) - 1);
    for (int i = 0; i < n; i++) {
        devents[std::lower_bound(ALL(values), v[i]) - values.begin()].push_back(i);
        ievents[std::lower_bound(ALL(values), l[i]) - values.begin()].push_back(i);
    }
    int result = -1, begin, end;
    for (int i = 0; i < SIZE(values); i++) {
        for (int j = 0; j < SIZE(ievents[i]); j++) {
            int k = ievents[i][j];
            modify(1, 0, SIZE(values) - 1, v[k], r[k], 1);
        }
        Pair answer = query(1, 0, SIZE(values) - 1, values[i], values.back());
        if (answer.first > result) {
            result = answer.first;
            begin = values[i];
            end = answer.second;
        }
        for (int j = 0; j < SIZE(devents[i]); j++) {
            int k = devents[i][j];
            modify(1, 0, SIZE(values) - 1, v[k], r[k], -1);
        }
    }
    std::vector <int> solution;
    for (int i = 0; i < n; i++) {
        if (l[i] <= begin && begin <= v[i] && v[i] <= end && end <= r[i]) {
            solution.push_back(i);
        }
    }
    printf("%d\n", result);
    for (int i = 0; i < SIZE(solution); i++) {
        printf("%d%c", solution[i] + 1, i < SIZE(solution) - 1 ? ' ' : '\n');
    }
}

