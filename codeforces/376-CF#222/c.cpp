#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define SIZE(x) ((int)(x).size())

const int M = 100;
const int N = 20;
const int INF = int(1e9);

int n, m, a[M], b[N];
char ctrl[N];
int memory[1 << N];
bool visited[1 << N];

int dp(int mask) {
    if (visited[mask]) {
        return memory[mask];
    }
    visited[mask] = true;
    int k = m - __builtin_popcount(mask);
    if (b[k] == 1) {
        memory[mask] = -INF;
        for (int i = 0; i < m; i++) {
            if (mask >> i & 1) {
                memory[mask] = std::max(memory[mask], dp(mask ^ 1 << i) + (ctrl[k] == 'p') * a[i]);
            }
        }
    } else {
        memory[mask] = INF;
        for (int i = 0; i < m; i++) {
            if (mask >> i & 1) {
                memory[mask] = std::min(memory[mask], dp(mask ^ 1 << i) - (ctrl[k] == 'p') * a[i]);
            }
        }
    }
    return memory[mask];
}

int main() {
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d", a + i);
    }
    std::sort(a, a + n, std::greater <int>());
    scanf("%d", &m);
    for (int i = 0; i < m; i++) {
        scanf(" %c %d", ctrl + i, b + i);
    }
    visited[0] = true;
    printf("%d\n", dp((1 << m) - 1));
}

