#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const int N = int(1e5) + 10;
const int M = 20;

int n, m, d, a[N];
int cnt[M];
bool invalid[1 << M];

int main() {
    scanf("%d%d%d", &n, &m, &d);
    for (int i = 0; i < m; i++) {
        int s;
        scanf("%d", &s);
        while (s--) {
            int x;
            scanf("%d", &x);
            a[x] = i;
        }
    }
    int mask = 0;
    for (int i = 1; i <= n; i++) {
        if (cnt[a[i]]++ == 0) {
            mask ^= 1 << a[i];
        }
        if (i >= d) {
            invalid[mask] = true;
            if (--cnt[a[i - d + 1]] == 0) {
                mask ^= 1 << a[i - d + 1];
            }
        }
    }
    int answer = m;
    for (mask = 1; mask < 1 << m; mask++) {
        for (int i = 0; i < m && !invalid[mask]; i++) {
            if (mask >> i & 1) {
                invalid[mask] |= invalid[mask ^ 1 << i];
            }
        }
        if (!invalid[mask]) {
            answer = std::min(answer, m - __builtin_popcount(mask));
        }
    }
    printf("%d\n", answer);
}

