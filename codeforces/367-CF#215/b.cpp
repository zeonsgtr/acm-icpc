#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <map>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const int N = 2 * int(1e5) + 10;

int n, m, p, a[N];
std::vector <int> sequence[N], solution;
std::map <int, int> count;

int main() {
    scanf("%d%d%d", &n, &m, &p);
    for (int i = 0; i < n; i++) {
        scanf("%d", a + i);
        sequence[i % p].push_back(i);
    }
    for (int i = 0; i < m; i++) {
        int x;
        scanf("%d", &x);
        count[x]++;
    }
    for (int i = 0; i < p; i++) {
        if (SIZE(sequence[i]) >= m) {
            int cnt = 0;
            std::map <int, int> map = count;
            for (int j = 0; j < m - 1; j++) {
                int x = a[sequence[i][j]];
                if (--map[x] == 0) {
                    cnt++;
                }
            }
            for (int j = m - 1; j < SIZE(sequence[i]); j++) {
                int x = a[sequence[i][j]];
                if (--map[x] == 0) {
                    cnt++;
                }
                if (cnt == SIZE(count)) {
                    solution.push_back(sequence[i][j - m + 1]);
                }
                x = a[sequence[i][j - m + 1]];
                if (map[x]++ == 0) {
                    cnt--;
                }
            }
        }
    }
    printf("%d\n", SIZE(solution));
    std::sort(ALL(solution));
    for (int i = 0; i < SIZE(solution); i++) {
        printf("%d%c", solution[i] + 1, i < SIZE(solution) - 1 ? ' ' : '\n');
    }
}

