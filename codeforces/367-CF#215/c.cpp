#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

int n, m;
std::vector <int> v;

long long calc(int x) {
    return x & 1 ? 1LL * x * (x - 1) / 2 + 1 : 1LL * x * (x - 1) / 2 + x / 2;
}

int main() {
    scanf("%d%d", &n, &m);
    for (int i = 0; i < m; i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        v.push_back(b);
    }
    std::sort(ALL(v), std::greater <int>());
    long long ans = 0, sum = 0;
    for (int i = 1; i <= m; i++) {
        sum += v[i - 1];
        if (calc(i) <= n) {
            ans = sum;
        }
    }
    std::cout << ans << std::endl;
}

