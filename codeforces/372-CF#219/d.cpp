#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <set>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const int N = int(1e5) + 10;
const int D = 18;

int n, m;
std::vector <int> adj[N];
std::vector <int> sa;
int order[N], depth[N], go[N][D];

void dfs(int x, int parent) {
    order[x] = SIZE(sa);
    sa.push_back(x);
    depth[x] = parent == -1 ? 0 : depth[parent] + 1;
    go[x][0] = parent;
    for (int i = 0; go[x][i] != -1 && go[go[x][i]][i] != -1; i++) {
        go[x][i + 1] = go[go[x][i]][i];
    }
    for (int itr = 0; itr < SIZE(adj[x]); itr++) {
        int y = adj[x][itr];
        if (y != parent) {
            dfs(y, x);
        }
    }
}

int lca(int a, int b) {
    if (depth[a] < depth[b]) {
        std::swap(a, b);
    }
    for (int i = D - 1; i >= 0; i--) {
        if (depth[a] - depth[b] >> i & 1) {
            a = go[a][i];
        }
    }
    for (int i = D - 1; i >= 0; i--) {
        if (go[a][i] != go[b][i]) {
            a = go[a][i];
            b = go[b][i];
        }
    }
    return a == b ? a : go[a][0];
}

std::set <int> set;

int add(int x) {
    if (set.empty()) {
        return 0;
    }
    std::set <int> ::iterator itr = set.lower_bound(order[x]);
    int r = sa[itr == set.end() ? *set.begin() : *itr];
    int l = sa[itr == set.begin() ? *set.rbegin(): *--itr];
    return depth[x] - depth[lca(l, x)] - depth[lca(r, x)] + depth[lca(l, r)];
}

int main() {
    scanf("%d%d", &n, &m);
    for (int i = 1; i < n; i++) {
        int s, t;
        scanf("%d%d", &s, &t);
        s--, t--;
        adj[s].push_back(t);
        adj[t].push_back(s);
    }
    memset(go, -1, sizeof(go));
    dfs(0, -1);
    int answer = 0;
    int component_size = 0;
    for (int i = 0, j = 0; i < n; i++) {
        for (; j < n && component_size + add(j) < m; j++) {
            component_size += add(j);
            set.insert(order[j]);
        }
        answer = std::max(answer, j - i);
        set.erase(order[i]);
        component_size -= add(i);
    }
    printf("%d\n", answer);
}

