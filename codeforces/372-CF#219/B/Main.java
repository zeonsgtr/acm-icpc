import java.util.*;
import java.io.*;

public class Main {
    int submatrixValue(int[][] s, int x1, int y1, int x2, int y2) {
        return s[x1][y1] - s[x2 + 1][y1] - s[x1][y2 + 1] + s[x2 + 1][y2 + 1];
    }
    void run() {
        InputReader in = new InputReader(System.in);
        PrintWriter out = new PrintWriter(System.out);
        int n = in.nextInt();
        int m = in.nextInt();
        int q = in.nextInt();
        int[][] sum = new int[n + 1][m + 1];
        for (int i = 0; i < n; i++) {
            String s = in.next();
            for (int j = 0; j < m; j++) {
                sum[i][j] = s.charAt(j) == '1' ? 1 : 0;
            }
        }
        for (int i = n - 1; i >= 0; i--) {
            for (int j = m - 1; j >= 0; j--) {
                sum[i][j] += sum[i][j + 1];
            }
        }
        for (int i = n - 1; i >= 0; i--) {
            for (int j = m - 1; j >= 0; j--) {
                sum[i][j] += sum[i + 1][j];
            }
        }
        long[][][][] answer = new long[n + 1][m + 1][n + 1][m + 1];
        long[][][][] count = new long[n][m][n][m];
        for (int x1 = 0; x1 < n; x1++) {
            for (int y1 = 0; y1 < m; y1++) {
                long[][] a = count[x1][y1];
                for (int x2 = x1; x2 < n; x2++) {
                    for (int y2 = y1; y2 < m; y2++) {
                        if (submatrixValue(sum, x1, y1, x2, y2) == 0) {
                            a[x2][y2]++;
                        }
                        if (x2 > x1) {
                            a[x2][y2] += a[x2 - 1][y2];
                        }
                        if (y2 > y1) {
                            a[x2][y2] += a[x2][y2 - 1];
                        }
                        if (x2 > x1 && y2 > y1) {
                            a[x2][y2] -= a[x2 - 1][y2 - 1];
                        }
                    }
                }
            }
        }
        for (int x1 = n - 1; x1 >= 0; x1--) {
            for (int y1 = m - 1; y1 >= 0; y1--) {
                for (int x2 = x1; x2 < n; x2++) {
                    for (int y2 = y1; y2 < m; y2++) {
                        answer[x1][y1][x2][y2] = answer[x1 + 1][y1][x2][y2] + answer[x1][y1 + 1][x2][y2]
                            - answer[x1 + 1][y1 + 1][x2][y2] + count[x1][y1][x2][y2];
                    }
                }
            }
        }
        for (int i = 0; i < q; i++) {
            int x1 = in.nextInt() - 1;
            int y1 = in.nextInt() - 1;
            int x2 = in.nextInt() - 1;
            int y2 = in.nextInt() - 1;
            out.println(answer[x1][y1][x2][y2]);
        }
        out.close();
    }
    public static void main(String[] args) {
        new Main().run();
    }
}

class InputReader {
    BufferedReader buff;
    StringTokenizer tokenizer;
    InputReader(InputStream stream) {
        buff = new BufferedReader(new InputStreamReader(stream));
        tokenizer = null;
    }
    boolean hasNext() {
        while (tokenizer == null || !tokenizer.hasMoreTokens()) {
            try {
                tokenizer = new StringTokenizer(buff.readLine());
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }
    String next() {
        if (!hasNext()) {
            throw new RuntimeException();
        }
        return tokenizer.nextToken();
    }
    int nextInt() {
        return Integer.parseInt(next());
    }
}
