import java.util.*;
import java.io.*;
import java.math.*;

public class Main {
    void run() {
        InputReader in = new InputReader(System.in);
        PrintWriter out = new PrintWriter(System.out);
        int n = in.nextInt();
        int[] a = new int[n];
        boolean[] v = new boolean[n];
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }
        Arrays.sort(a);
        int answer = 0;
        for (int i = 0, j = n / 2; i < n / 2; i++, j++) {
            for (; j < n && a[i] * 2 > a[j]; j++);
            if (j == n) {
                break;
            }
            answer = i + 1;
        }
        out.println(n - answer);
        out.close();
    }
    public static void main(String[] args) {
        new Main().run();
    }
    void debug(Object...os) {
        System.out.println(Arrays.deepToString(os));
    }
}

class InputReader {
    BufferedReader buff;
    StringTokenizer tokenizer;
    InputReader(InputStream stream) {
        buff = new BufferedReader(new InputStreamReader(stream));
        tokenizer = null;
    }
    boolean hasNext() {
        while (tokenizer == null || !tokenizer.hasMoreTokens()) {
            try {
                tokenizer = new StringTokenizer(buff.readLine());
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }
    String next() {
        if (!hasNext()) {
            throw new RuntimeException();
        }
        return tokenizer.nextToken();
    }
    int nextInt() {
        return Integer.parseInt(next());
    }
    long nextLong() {
        return Long.parseLong(next());
    }
    double nextDouble() {
        return Double.parseDouble(next());
    }
    BigInteger nextBigInteger() {
        return new BigInteger(next());
    }
}
