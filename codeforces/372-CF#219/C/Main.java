import java.util.*;
import java.io.*;
import java.math.*;

public class Main {
    static final long INF = Long.MAX_VALUE / 4;
    void run() {
        InputReader in = new InputReader(System.in);
        PrintWriter out = new PrintWriter(System.out);
        int n = in.nextInt();
        int m = in.nextInt();
        int d = in.nextInt();
        int[] a = new int[m];
        int[] b = new int[m];
        int[] t = new int[m];
        long result = 0;
        for (int i = 0; i < m; i++) {
            a[i] = in.nextInt() - 1;
            b[i] = in.nextInt();
            t[i] = in.nextInt();
            result += b[i];
        }
        long[][] dp = new long[2][n];
        for (int i = 0; i < m; i++) {
            long[] tmp = dp[0];
            dp[0] = dp[1];
            dp[1] = tmp;
            Arrays.fill(dp[1], INF);
            long maxLength = i == 0 ? 0 : 1L * (t[i] - t[i - 1]) * d;
            int[] queue = new int[n];
            int front = 0;
            int rear = -1;
            for (int j = 0; j < n; j++) {
                for (; front <= rear && dp[0][queue[rear]] >= dp[0][j]; rear--);
                queue[++rear] = j;
                for (; front <= rear && j - queue[front] > maxLength; front++);
                dp[1][j] = Math.min(dp[1][j], dp[0][queue[front]] + Math.abs(a[i] - j));
            }
            front = 0;
            rear = -1;
            for (int j = n - 1; j >= 0; j--) {
                for (; front <= rear && dp[0][queue[rear]] >= dp[0][j]; rear--);
                queue[++rear] = j;
                for (; front <= rear && queue[front] - j > maxLength; front++);
                dp[1][j] = Math.min(dp[1][j], dp[0][queue[front]] + Math.abs(a[i] - j));
            }
        }
        long min = INF;
        for (int i = 0; i < n; i++) {
            min = Math.min(min, dp[1][i]);
        }
        out.println(result - min);
        out.close();
    }
    public static void main(String[] args) {
        new Main().run();
    }
    void debug(Object...os) {
        System.out.println(Arrays.deepToString(os));
    }
}

class InputReader {
    BufferedReader buff;
    StringTokenizer tokenizer;
    InputReader(InputStream stream) {
        buff = new BufferedReader(new InputStreamReader(stream));
        tokenizer = null;
    }
    boolean hasNext() {
        while (tokenizer == null || !tokenizer.hasMoreTokens()) {
            try {
                tokenizer = new StringTokenizer(buff.readLine());
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }
    String next() {
        if (!hasNext()) {
            throw new RuntimeException();
        }
        return tokenizer.nextToken();
    }
    int nextInt() {
        return Integer.parseInt(next());
    }
    long nextLong() {
        return Long.parseLong(next());
    }
    double nextDouble() {
        return Double.parseDouble(next());
    }
    BigInteger nextBigInteger() {
        return new BigInteger(next());
    }
}

