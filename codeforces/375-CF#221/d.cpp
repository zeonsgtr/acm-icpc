#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include <map>

#define index INDEX
#define SIZE(x) ((int)(x).size())
#define FOR(itr, x) for (__typeof((x).end()) itr = (x).begin(); itr != (x).end(); itr++)

const int N = 100000 + 10;
const int M = N * 20;

int n, m;
int stack[M], top;
int lc[M], rc[M], size[M], key[M], aux[M];

inline void l_rotate(int& x) {
    int y = rc[x];
    rc[x] = lc[y];
    lc[y] = x;
    size[y] = size[x];
    size[x] = size[lc[x]] + size[rc[x]] + 1;
    x = y;
}

inline void r_rotate(int& x) {
    int y = lc[x];
    lc[x] = rc[y];
    rc[y] = x;
    size[y] = size[x];
    size[x] = size[lc[x]] + size[rc[x]] + 1;
    x = y;
}

void insert(int& k, int x) {
    if (!k) {
        k = stack[top--];
        lc[k] = rc[k] = 0;
        size[k] = 1;
        key[k] = x;
        aux[k] = rand() * rand();
        return;
    }
    size[k]++;
    if (x < key[k]) {
        insert(lc[k], x);
        if (aux[lc[k]] < aux[k]) {
            r_rotate(k);
        }
    } else {
        insert(rc[k], x);
        if (aux[rc[k]] < aux[k]) {
            l_rotate(k);
        }
    }
}

int remove(int& k, int x) {
    size[k]--;
    if (key[k] == x || x > key[k] && rc[k] == 0) {
        if (!lc[k] || !rc[k]) {
            stack[++top] = k;
            int ret = key[k];
            k = lc[k] + rc[k];
            return ret;
        }
        key[k] = remove(lc[k], x + 1);
        return key[k];
    }
    return remove(x < key[k] ? lc[k] : rc[k], x);
}

int count(int root, int s) {
    int result = 0;
    while (root > 0) {
        if (key[root] >= s) {
            result += size[rc[root]] + 1;
            root = lc[root];
        } else {
            root = rc[root];
        }
    }
    return result;
}

typedef std::pair <int, int> PII;
std::map <int, int> map[N];

std::vector <int> adj[N];
int answer[N];
int color[N];
int root[N], index[N];
std::vector <PII> qvec[N];

void solve(int x, int parent) {
    index[x] = x;
    insert(root[x], map[index[x]][color[x]] = 1);
    for (int i = 0; i < SIZE(adj[x]); i++) {
        int y = adj[x][i];
        if (y != parent) {
            solve(y, x);
            int u = x, v = y;
            if (size[root[index[u]]] > size[root[index[v]]]) {
                std::swap(u, v);
            }
            int a = index[u];
            int b = index[v];
            std::map <int, int>& begin = map[a];
            std::map <int, int>& end = map[b];
            FOR (itr, begin) {
                int color = itr->first;
                int number = itr->second;
                if (!end.count(color)) {
                    end[color] = number;
                    insert(root[b], number);
                } else {
                    remove(root[b], end[color]);
                    insert(root[b], end[color] += number);
                }
            }
            index[u] = index[v];
        }
    }
    int r = root[index[x]];
    for (int i = 0; i < SIZE(qvec[x]); i++) {
        answer[qvec[x][i].first] = count(r, qvec[x][i].second);
    }
}

int main() {
    srand(time(0));
    top = 0;
    for (int i = 1; i < M; i++) {
        stack[++top] = i;
    }
    scanf("%d%d", &n, &m);
    for (int i = 0; i < n; i++) {
        scanf("%d", color + i);
    }
    for (int i = 1; i < n; i++) {
        int s, t;
        scanf("%d%d", &s, &t);
        s--, t--;
        adj[s].push_back(t);
        adj[t].push_back(s);
    }
    for (int i = 0; i < m; i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        a--;
        qvec[a].push_back(PII(i, b));
    }
    solve(0, -1);
    for (int i = 0; i < m; i++) {
        printf("%d\n", answer[i]);
    }
}

