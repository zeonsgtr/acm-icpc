#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

typedef long long int64;

const int N = 5000 + 10;

int n, m;
char buf[N];
int count[N][N];

int main() {
    scanf("%d%d", &n, &m);
    for (int i = 0; i < n; i++) {
        scanf("%s", buf);
        for (int j = 0, k = 0; j < m; j = k) {
            for (k = j; k < m && buf[k] == '1'; k++);
            if (j == k) {
                k++;
            } else {
                int u = j + 1;
                int v = k;
                count[u][u]++;
                count[v + 1][v + 1]++;
                count[u][v + 1]--;
                count[v + 1][u]--;
            }
        }
    }
    int result = 0;
    for (int i = 1; i <= m; i++) {
        for (int j = 1; j <= m; j++) {
            count[i][j] += count[i - 1][j] + count[i][j - 1] - count[i - 1][j - 1];
            if (i <= j) {
                result = std::max(result, count[i][j] * (j - i + 1));
            }
        }
    }
    printf("%d\n", result);
}

