#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define SIZE(x) ((int)(x).size())

const int N = 21, M = 8;
const int INF = int(1e9);
const int DELTA[4][2] = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

int n, m, s, bomb, startx, starty;
char map[N][N];
int id[N][N];
int delta[N][N];
int v[N][N][1 << M];
int queue[N * N * (1 << M)][3];
std::vector <int> value;

int main() {
    scanf("%d%d", &n, &m);
    for (int i = 0; i < n; i++) {
        scanf("%s", map[i]);
        for (int j = 0; j < n; j++) {
            if (isdigit(map[i][j])) {
                map[i][j]--;
            } else if (map[i][j] == 'S') {
                startx = i;
                starty = j;
                map[i][j] = '.';
            }
        }
    }
    if (true) {
        int x;
        while (scanf("%d", &x) == 1) {
            value.push_back(x);
        }
    }
    s = SIZE(value);
    memset(id, -1, sizeof(id));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (map[i][j] == 'B') {
                id[i][j] = s++;
                bomb |= 1 << id[i][j];
            }
        }
    }
    memset(delta, -1, sizeof(delta));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (map[i][j] == '.') {
                delta[i][j] = 0;
                for (int k = 0; k < j; k++) {
                    if (isdigit(map[i][k])) {
                        delta[i][j] |= 1 << map[i][k] - '0';
                    } else if (map[i][k] == 'B') {
                        delta[i][j] |= 1 << id[i][k];
                    }
                }
            }
        }
    }
    int rear = 0;
    memset(v, -1, sizeof(v));
    v[startx][starty][0] = 0;
    queue[rear][0] = startx;
    queue[rear][1] = starty;
    queue[rear][2] = 0;
    rear++;
    for (int front = 0; front < rear; front++) {
        int x = queue[front][0];
        int y = queue[front][1];
        int mask = queue[front][2];
        for (int k = 0; k < 4; k++) {
            int tx = x + DELTA[k][0];
            int ty = y + DELTA[k][1];
            if (tx < 0 || tx >= n || ty < 0 || ty >= m || map[tx][ty] != '.') {
                continue;
            }
            int tmask = mask;
            if (k == 0) {
                tmask ^= delta[tx][ty];
            } else if (k == 1) {
                tmask ^= delta[x][y];
            }
            if (v[tx][ty][tmask] == -1) {
                v[tx][ty][tmask] = v[x][y][mask] + 1;
                queue[rear][0] = tx;
                queue[rear][1] = ty;
                queue[rear][2] = tmask;
                rear++;
            }
        }
    }
    int result = 0;
    for (int mask = 0; mask < 1 << s; mask++) {
        if (v[startx][starty][mask] > -1) {
            int sum = 0;
            for (int k = 0; k < s; k++) {
                if (mask >> k & 1) {
                    sum = k < SIZE(value) ? sum + value[k] : -INF;
                }
            }
            result = std::max(result, sum - v[startx][starty][mask]);
        }
    }
    printf("%d\n", result);
}

