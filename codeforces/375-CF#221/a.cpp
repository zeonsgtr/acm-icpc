#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

const int N = int(1e6) + 100;

int n;
char buf[N];
int count[10];

int main() {
    gets(buf);
    n = strlen(buf);
    for (int i = 0; i < n; i++) {
        count[buf[i] - '0']++;
    }
    count[1]--;
    count[6]--;
    count[8]--;
    count[9]--;
    int val = 0;
    for (int i = 9; i > 0; i--) {
        printf("%s", std::string(count[i], char('0' + i)).c_str());
        for (; count[i] > 0; count[i]--) {
            val = (val * 10 + i) % 7;
        }
    }
    int a[4] = {1, 6, 8, 9};
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            for (int k = 0; k < 4; k++) {
                for (int l = 0; l < 4; l++) {
                    if (i != j && i != k && i != l && j != k && j != l && k != l) {
                        if ((val * 10000 + a[i] * 1000 + a[j] * 100 + a[k] * 10 + a[l]) % 7 == 0) {
                            printf("%d%d%d%d", a[i], a[j], a[k], a[l]);
                            puts(std::string(count[0], '0').c_str());
                            return 0;
                        }
                    }
                }
            }
        }
    }
}

