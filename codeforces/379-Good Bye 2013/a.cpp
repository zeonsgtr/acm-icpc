#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

int main() {
    int a, b, c = 0, d = 0;
    scanf("%d%d", &a, &b);
    while (true) {
        d += a;
        c += a;
        a = c / b;
        c %= b;
        if (a == 0) {
            break;
        }
    }
    printf("%d\n", d);
}

