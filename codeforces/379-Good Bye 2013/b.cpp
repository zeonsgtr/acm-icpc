#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

const int N = 333;

int n;
int a[N];

int main() {
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d", a + i);
        if (i > 0) {
            putchar('R');
        }
        if (a[i] > 0) {
            putchar('P');
            a[i]--;
        }
        for (; a[i] > 0; a[i]--) {
            if (i > 0) {
                putchar('L');
                putchar('R');
            } else {
                putchar('R');
                putchar('L');
            }
            putchar('P');
        }
    }
    puts("");
}

