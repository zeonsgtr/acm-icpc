#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <map>

#define SIZE(x) ((int)(x).size())

const int N = 3 * int(1e5) + 10;
const int INF = int(1e9) + N;

int n, ans[N];
std::pair <int, int> a[N];

int main() {
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i].first);
        a[i].second = i;
    }
    std::sort(a, a + n);
    std::vector <int> v;
    for (int i = 0, j; i < n; i = j) {
        for (j = i; j < n && a[j].first == a[i].first; j++) {
            v.push_back(j);
        }
        int delta = std::min(SIZE(v), j < n ? a[j].first - a[i].first : INF);
        for (; delta > 0; delta--, v.pop_back()) {
            ans[a[v.back()].second] = a[i].first + delta - 1;
        }
    }
    for (int i = 0; i < n; i++) {
        printf("%d%c", ans[i], i < n - 1 ? ' ' : '\n');
    }
}

