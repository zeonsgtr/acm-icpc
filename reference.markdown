# November-2013
## Yandex.Algorithm Open 2011 Qualification 1 - E. Pairs
-   tags: dp, tree
-   thinking complexity: 1
-   coding complexity: 3

## srm596 - IncrementAndDoubling(250pt)
-   tags: wit, bit
-   thinking complexity: 2, without seckill
-   coding complexity: 0

## srm596 - BitwiseAnd(500pt)
-   tags: wit, bit, greedy
-   thinking complexity: 3, without seckill
-   coding complexity: 1

## srm595 - LittleElephantAndIntervalsDiv1(250pt)
-   tags: counting
-   thinking complexity: 0
-   coding complexity: 0

## srm595 - LittleElephantAndRGB(500pt)
-   tags: counting
-   thinking complexity: 1
-   coding complexity: 2

## live-archive - 3074. Know When to Hold'em
-   tags: implementation, poker
-   thinking complexity: 0
-   coding complexity: 3

## 2010-Chengdu-Onsite - battle over citys
-   tags: tree, data structure
-   thinking complexity: 4
-   coding complexity: 4

## srm594 - AstronomicalRecords(250pt)
-   tags: dp
-   thinking complexity: 2
-   coding complexity: 0

## srm594 - FoxAndGo3(550pt)
-   tags: network flow
-   thinking complexity: 2
-   coding complexity: 1

## srm593 - HexagonalBoard(250pt)
-   tags: coloring, graph, wit
-   thinking complexity: -1
-   coding complexity: 0

## srm593 - MayTheBestPetWin(450pt)
-   tags: wit, math, optimize
-   thinking complexity: -1
-   coding complexity: 0

## srm590 - FoxAndChess(250pt)
-   tags: greedy
-   thinking complexity: 1
-   coding complexity: 0

## srm590 - XorCards(500pt)
-   tags: counting, gaussian elimination
-   thinking complexity: 4
-   coding complexity: 1.5

## srm589 - GooseTattarrattatDiv1(250pt)
-   tags: greedy, wit
-   thinking complexity: 2
-   coding complexity: 0

## srm589 - GearsDiv1(450pt)
-   tags: network flow
-   thinking complexity: 1
-   coding complexity: 0-1

## srm588 - GUMIAndSongsDiv1(250pt)
-   tags: greedy, wit
-   thinking complexity: 2
-   coding complexity: 0

## srm588 - KeyDungeonDiv1(450pt)
-   tags: dp, **big trick**
-   thinking complexity: 0
-   coding complexity: 0

## srm587 - JumpFurther(250pt)
-   tags: greedy, wit
-   thinking complexity: 0
-   coding complexity: 0

## srm587 - TriangleXor(550pt)
-   tags: math, computational geometry, wit
-   thinking complexity: 2-3
-   coding complexity: 0-1.5

## srm586 - PiecewiseLinearFunction(250pt)
-   tags: wit, **small trick**
-   thinking complexity: 0
-   coding complexity: 0

## srm586 - History(500pt)
-   tags: differential constraints, inequalities
-   thinking complexity: 1
-   coding complexity: 1

## srm591 - BinPacking(275pt)
-   tags: tree, wit
-   thinking complexity: 1
-   coding complexity: 0

## srm591 - FoxAndFencing(500pt)
-   tags: counting, math
-   thinking complexity: -1
-   coding complexity: 1.5

## srm584 - Egalitarianism(250pt)
-   tags: floyd
-   thinking complexity: 1
-   coding complexity: 0

## srm584 - FoxTheLinguist(900pt)
-   tags: optimum arborescence, Steiner-tree
-   thinking complexity: -1
-   coding complexity: 1

## srm583 - TravelOnMars(250pt)
-   tags: shortest path, bellman-ford
-   thinking complexity: 0
-   coding complexity: 0

## srm583 - TurnOnLamps(500pt)
-   tags: wit, greedy, tree, dp
-   thinking complexity: 1
-   coding complexity: 1.5

## srm583 - RandomPaingtingOnABoard(950pt)
-   tags: probability, math
-   thinking complexity: -1
-   coding complexity: 1

## cf219(372) - A. Counting Kangaroos is Fun
-   tags: greedy
-   thinking complexity: 2
-   coding complexity: 0

## cf219(372) - B. Counting Rectangles is Fun
-   tags: inclusion-exculison, counting
-   thinking complexity: 2
-   coding complexity: 1

## cf219(372) - C. Watching Fireworks is Fun
-   tags: dp, monotone queue
-   thinking complexity: 2
-   coding complexity: 1

## uva - 10288. Coupons
-   tags: probability
-   thinking complexity: -1
-   coding complexity: N/A

## bzoj - 1095. 捉迷藏(ZJOI2007)
-   tags: tree, data structure, bracket sequence
-   thinking complexity: -1
-   coding complexity: 1.5

## 05集训队 - 树的统计(何林)
-   tags: tree, dfs sequence
-   thinking complexity: -1
-   coding complexity: 1

## cf221(375) - A. Divisible Seven
-   tags: brute force
-   thinking complexity: 2
-   coding complexity: 0

## cf221(375) - B. Maximum Submatrix 2
-   tags: wit
-   thinking complexity: 1
-   coding complexity: 0

## cf221(375) - C. Circling Round Treasures
-   tags: dp, bfs
-   thinking complexity: 1
-   coding complexity: 1

## cf221(375) - D. Tree and Queries
-   tags: tree, data structure, brute force
-   thinking complexity: 3
-   coding complexity: 2

# January-2014

## cf222(376) - A. Maze
-   tags: wit, dfs
-   thinking complexity: 0
-   coding complexity: 0

## cf222(376) - B. Preparing for the Contest
-   tags: binary search, greedy
-   thinking complexity: 1
-   coding complexity: 1

## cf222(376) - C. Captains Mode
-   tags: dp, game
-   thinking complexity: 2
-   coding complexity: 0

## cf222(376) - D. Developing Game
-   tags: data structure, two pointers, wit
-   thinking complexity: 3
-   coding complexity: 1.5

## cf219(372) - D. Choosing Subtree is Fun
-   tags: two pointers, tree, wit
-   thinking complexity: -1
-   coding complexity: 1

## cf215(367) - B. Sereja and Anagrams
-   tags: wit
-   thinking complexity: 2
-   coding complexity: 0

## cf215(367) - C. Sereja and the Arrangement of Numbers
-   tags: Eulerian circuit
-   thinking complexity: 2
-   coding complexity: 0

## cf215(367) - D. Sereja and Sets
-   tags: mask, wit
-   thinking complexity: -1
-   coding complexity: 0
-   这题其实并不难, 但我感觉自己貌似是掉入了思维陷阱

# February-2014

## poj1743 - Musical Theme
-   tags: suffix array, string
-   thinking complexity: 1
-   coding complexity: 1
-   trick: n = 1的时候suffix array求height会出错, 这是因为rank没有normalize的原因.

## poj3261 - Milk Patterns
-   tags: suffix array, string
-   thinking complexity: 0
-   coding complexity: 1
-   trick: 尼玛, 一般情况下后缀数组的长度至少为2, 因而会在后面加个空字符, 这样就没问题了, 但是上个题, 呵呵.

## spoj - DISUBSTR
-   tags: suffix array, string
-   thinking complexity: 0
-   coding complexity: 1

## ural192 - Panlindrome
-   tags: suffix array, string
-   thinking complexity: 0
-   coding complexity: 1

## poj2406 - Power Strings
-   tags: string, kmp, suffix array
-   thinking complexity: 1
-   coding complexity: 0
-   remark: 倍增被卡.

## spoj - REPEATS
-   tags: suffix array, string
-   thinking complexity: -1
-   coding complexity: 1

## poj3415 - Common Substrings
-   tags: suffix array, string, counting
-   thinking complexity: 3
-   coding complexity: 1.5
-   trick: 这道题的难点在于想清楚, 而且有后缀数组的一个常见错误, 即分不清楚栈里面放的是什么.

# March-2014 

## poj3294 - Life Forms
-   tags: suffix array, string
-   thinking complexity: 0
-   coding complexity: 1
-   remark: 现在再看这题已经无比简单

## spoj - PHRASES
-   tags: suffix array, string
-   thinking complexity: 0
-   coding complexity: 1

## srm611 - LCMSet(250pt)
-   tags: math, wit
-   thinking complexity: 1
-   coding complexity: 0

## srm611 - Egalitarianism2(550pt)
-   tags: mst, variance, greedy
-   thinking complexity: -1
-   coding complexity: 0
-   identifier: TRtIF, TP, GD

## (N/A)srm612 - EmoticonsDiv1(250pt)
-   tags: dp
-   thinking complexity: 0
-   coding complexity: 0
-   identifier: F

## srm612 - LeftAndRightHandedDiv1(900pt)
-   tags: two pointers, wit
-   thinking complexity: 1
-   coding complexity: 1.5
-   identifier: F

## srm612 - SpecialCells(450pt)
-   tags: flow
-   thinking complexity: 2
-   coding complexity: 1
-   identifier: F
-   remark: 尼玛，看错题了，还TM能过pretests，瞎狗眼，450果然是水比题。

# standards
## thinking complexity 
- -1, didn't think out
- 0, seckill
- 1, thinking about 5 minutes
- 2, thinking about 10 minutes
- 3, thinking about 20 minutes
- 4, thinking for a long time

## coding complexity
- 0, too easy, without thinking, and the code length is within 100 lines
- 1, too easy, without thinking, and the code length is within 150 lines
- 1.5, need to think clearly, and easy to code, or maybe think clearly would make the code very short
- 2, think a little, or the code length is about 150 lines
- 3, will meet trouble if don't thinking clearly, or the code length is about 200 lines
- 4, require very carefully prepare, and the code length is about or over 250 lines

# some topics to investigate
## TP, threshold property 
counter = I

## GD, great difficulty
counter = I
