#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <map>

#define SIZE(x) ((int)(x).size())
#define FOR(itr, x) for (__typeof((x).end()) itr = (x).begin(); itr != (x).end(); itr++)

struct Point;
typedef std::map <Point, int> Map;

const int N = 9;
const int DELTA[6][2] = {
	{1, 0}, {-1, 0}, {0, 1}, {0, -1}, {1, 1}, {-1, -1}
};

int frontier[N][2];

struct Point {
	int x, y;
	Point() {}
	Point(int x, int y): x(x), y(y) {}
	bool operator <(const Point& o) const {
		return x < o.x || x == o.x && y < o.y;
	}
	bool in() const {
		if (y < 0 || y >= N) {
			return false;
		}
		return frontier[y][0] <= x && x < frontier[y][1];
	}
};

Map map; //0 <-> white, 1 <-> black
int dead_count[2];

bool have(Map& map, int x, int y, int c) {
	return Point(x, y).in() && map.count(Point(x, y)) && map[Point(x, y)] == c;
}

void read_positions() {
	static char buf[9];
	map.clear();
	dead_count[0] = dead_count[1] = 14;
	for (int y = N - 1; y >= 0; y--) {
		for (int x = frontier[y][0]; x < frontier[y][1]; x++) {
			scanf("%s", buf);
			if (buf[0] == 'W') {
				dead_count[0]--;
				map[Point(x, y)] = 0;
			} else if (buf[0] == 'B') {
				dead_count[1]--;
				map[Point(x, y)] = 1;
			}
		}
	}
}

void rotate(Map& map) {
	Map tmp;
	FOR (itr, map) {
		tmp[Point(-(itr->first.y - itr->first.x - 4), itr->first.x)] = itr->second;
	}
	map = tmp;
}

bool win(int s, Map map, int which) {
	bool ok = false;
	for (int times = 0; times < 6; times++) {
		for (int y = 0; y < N && !ok; y++) {
			for (int x = frontier[y][0]; x < frontier[y][1] && !ok; x++) {
				for (int z = 1; z < s && !ok; z++) {
					if (x + s + z == frontier[y][1]) {
						ok = true;
						for (int d = 0; d < s + z && ok; d++) {
							ok &= have(map, x + d, y, which ^ (d < s));
						}
					}
				}
			}
		}
		rotate(map);
	}
	return ok;
}

bool check(Map map, int& cnt) {
	cnt++;
	int sum = 14;
	FOR (itr, map) {
		sum -= itr->second == 0 && itr->first.in();
	}
	return sum == 5 && (win(2, map, 0) || win(3, map, 0));
}

bool lose(Map map) {
	int counter = 0;
	bool ok = true;
	for (int times = 0; times < 6 && ok; times++) {
		Map tmp;
		for (int s = 1; s <= 3; s++) {
			for (int y = 0; y < N; y++) {
				for (int x = frontier[y][0]; x + s < frontier[y][1]; x++) {
					bool flag = true;
					for (int k = 0; k < s && flag; k++) {
						flag = have(map, x + k, y, 0);
					}
					if (!flag) {
						continue;
					}
					int cnt[2] = {};
					for (int k = s; have(map, x + k, y, 0) || have(map, x + k, y, 1); k++) {
						cnt[have(map, x + k, y, 1)]++;
					}
					if (cnt[0] > 0 || cnt[1] >= s) {
						continue;
					}
					tmp = map;
					for (int k = 0; have(map, x + k, y, 0) || have(map, x + k, y, 1); k++) {
						tmp.erase(Point(x + k, y));
					}
					for (int k = 0; have(map, x + k, y, 0) || have(map, x + k, y, 1); k++) {
						tmp[Point(x + k + 1, y)] = have(map, x + k, y, 1);
					}
					ok &= check(tmp, counter);
				}
			}
			for (int d = 0; d < 2; d++) {
				for (int y = 0; y < N; y++) {
					for (int x = frontier[y][0]; x + s < frontier[y][1]; x++) {
						int cnt = 0;
						bool flag = true;
						for (int k = 0; k < s && flag; k++) {
							flag &= have(map, x + k, y, 0);
							flag &= Point(x + k + d, y + 1).in();
							cnt += have(map, x + k + d, y + 1, 0) || have(map, x + k + d, y + 1, 1);
						}
						if (!flag || cnt > 0) {
							continue;
						}
						tmp = map;
						for (int k = 0; k < s; k++) {
							tmp.erase(Point(x + k, y));
							tmp[Point(x + k + d, y + 1)] = 0;
						}
						ok &= check(map, counter);
					}
				}
			}
		}
		rotate(map);
	}
	if (counter == 0) {
		ok &= check(map, counter);
	}
	return ok;
}

int main() {
	frontier[0][0] = 0, frontier[0][1] = 5;
	for (int i = 1; i < 5; i++) {
		frontier[i][0] = frontier[i - 1][0];
		frontier[i][1] = frontier[i - 1][1] + 1;
	}
	for (int i = 5; i < N; i++) {
		frontier[i][0] = frontier[i - 1][0] + 1;
		frontier[i][1] = frontier[i - 1][1];
	}

	int test_count;
	scanf("%d", &test_count);
	while (test_count--) {
		read_positions();
		static int case_number = 1;
		printf("Case %d: ", case_number++);
		if (dead_count[1] == 5 && (win(2, map, 1) || win(3, map, 1))) {
			puts("White");
		} else if (lose(map)) {
			puts("Black");
		} else {
			puts("Draw");
		}
	}
}

