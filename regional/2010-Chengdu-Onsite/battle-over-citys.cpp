#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <queue>

#define SIZE(x) ((int)(x).size())

const int N = 22222;
const int D = 16;
const int M = 111111 * 4;
const int INF = int(1e9);

struct Node {
    int key, s, t, depth;
    Node* children[2];
};

Node nodes[M];
int node_count;
Node* root[N], *null;

inline Node* new_node(int key, int s, int t) {
    Node* ptr = nodes + node_count++;
    ptr->key = key;
    ptr->s = s;
    ptr->t = t;
    ptr->depth = 0;
    ptr->children[0] = ptr->children[1] = null;
    return ptr;
}

inline Node* merge(Node* a, Node* b) {
    if (a == null || b == null) {
        return a == null ? b : a;
    }
    if (a->key > b->key) {
        std::swap(a, b);
    }
    a->children[1] = merge(a->children[1], b);
    if (a->children[0]->depth < a->children[1]->depth) {
        std::swap(a->children[0], a->children[1]);
    }
    a->depth = a->children[0]->depth + 1;
    return a;
}

struct Edge {
    int x, s, t;
    Edge(int x, int s, int t): x(x), s(s), t(t) {}
    bool operator <(const Edge& o) const {
        return x > o.x;
    }
    void show() const {
        printf("weight = %d, s = %d, t = %d\n", x, s, t);
    }
};

struct DisjointSet {
    int f[N], n;
    void make_use(int m) {
        n = m;
        for (int i = 0; i < n; i++) {
            f[i] = i;
        }
    }
    int find(int x) {
        return f[x] == x ? x : f[x] = find(f[x]);
    }
    int& operator [](int x) {
        return f[x];
    }
    void merge(int x, int y) {
        f[find(x)] = find(y);
    }
}ds[2];

int n, m;
int first_edge[N], next_edge[M], to[M], w[M], start[M], edge_count;
bool istree[M];
int go[N][D], depth[N], ans[N], father[N];
int ischild[N], timestamp, basic;
std::vector <Edge> vector[N];

void add_edge(int s, int t, int x) {
    w[edge_count] = x;
    to[edge_count] = t;
    next_edge[edge_count] = first_edge[s];
    start[edge_count] = s;
    istree[edge_count] = false;
    first_edge[s] = edge_count++;
}

void build_tree(int x, int parent) {
    go[x][0] = parent;
    for (int i = 0; go[x][i] != -1 && go[go[x][i]][i] != -1; i++) {
        go[x][i + 1] = go[go[x][i]][i];
    }
    for (int itr = first_edge[x]; itr != -1; itr = next_edge[itr]) {
        int y = to[itr];
        if (y != parent && istree[itr]) {
            father[y] = w[itr];
            depth[y] = depth[x] + 1;
            build_tree(y, x);
        }
    }
}

int lca(int a, int b) {
    if (depth[a] < depth[b]) {
        std::swap(a, b);
    }
    for (int i = D - 1; i >= 0; i--) {
        if (depth[a] - depth[b] >> i & 1) {
            a = go[a][i];
        }
    }
    for (int i = D - 1; i >= 0; i--) {
        if (go[a][i] != go[b][i]) {
            a = go[a][i];
            b = go[b][i];
        }
    }
    return a == b ? a : go[a][0];
}

void solve(int x, const std::vector <int>& children, int times, int result) {
    std::priority_queue <Edge> queue;
    for (int i = 0; i < SIZE(vector[x]); i++) {
        queue.push(vector[x][i]);
    }
    timestamp++;
    for (int i = 0; i < SIZE(children); i++) {
        ds[1][children[i]] = children[i];
        ischild[children[i]] = timestamp;
    }
    ds[1][n] = n;
    for (int i = 0; i < SIZE(children); i++) {
        int y = children[i];
        while (root[y] != null) {
            Node* e = root[y];
            if (ds[0].find(e->s) == ds[0].find(e->t) || e->t == x || e->s == x || 
                    ischild[ds[0].find(e->t)] == timestamp && ischild[ds[0].find(e->s)] == timestamp) {
                root[y] = merge(root[y]->children[0], root[y]->children[1]);
                continue;
            }
            queue.push(Edge(e->key, e->s, e->t));
            break;
        }
    }

    ans[x] = INF;
    for (int itr = 0; itr < times; itr++) {
        if (queue.empty()) {
            return;
        }
        Edge e = queue.top();
        queue.pop();
        int u = ds[0].find(e.s), v = ds[0].find(e.t);
        if (u == x || v == x) {
            itr--;
            continue;
        }
        u = ischild[u] == timestamp ? u : n;
        v = ischild[v] == timestamp ? v : n;
        if (ds[1].find(u) == ds[1].find(v)) {
            itr--;
            continue;
        }
        ds[1].merge(u, v);
        result += e.x;
    }
    ans[x] = result;
}

void dfs(int x, int parent) {
    std::vector <int> children;
    root[x] = null;
    int temp = basic - father[x];
    for (int itr = first_edge[x]; itr != -1; itr = next_edge[itr]) {
        int y = to[itr];
        if (y != parent && istree[itr]) {
            dfs(y, x);
            children.push_back(y);
            temp -= w[itr];
        } else if (y != parent) {
            root[x] = merge(root[x], new_node(w[itr], x, y));
        }
    }
    solve(x, children, SIZE(children) - (parent == -1), temp);
    for (int i = 0; i < SIZE(children); i++) {
        int y = children[i];
        root[x] = merge(root[x], root[y]);
        ds[0].merge(y, x);
    }
}

bool cmp(int i, int j) {
    return w[i] < w[j];
}

int main() {
    node_count = 0;
    null = new_node(0, 0, 0);
    null->depth = -1;
    int test_count;
    scanf("%d", &test_count);
    while (test_count--) {
        node_count = 1;
        scanf("%d%d", &n, &m);
        std::fill(first_edge, first_edge + n, -1);
        edge_count = 0;
        std::vector <int> v;
        for (int i = 0; i < m; i++) {
            int s, t, a, b;
            scanf("%d%d%d%d", &s, &t, &a, &b);
            s--, t--;
            add_edge(s, t, (b == 0) * a);
            v.push_back(edge_count - 1);
            add_edge(t, s, (b == 0) * a);
        }

        std::sort(v.begin(), v.end(), cmp);
        ds[0].make_use(n);
        basic = 0;
        for (int i = 0; i < SIZE(v); i++) {
            int a = ds[0].find(start[v[i]]);
            int b = ds[0].find(to[v[i]]);
            if (a != b) {
                basic += w[v[i]];
                ds[0].merge(a, b);
                istree[v[i]] = istree[v[i] ^ 1] = true;
            }
        }

        for (int i = 0; i < n; i++) {
            std::fill(go[i], go[i] + D, -1);
            vector[i].clear();
        }
        father[0] = 0;
        depth[0] = 0;
        build_tree(0, -1);
        for (int i = 0; i < n; i++) {
            for (int itr = first_edge[i]; itr != -1; itr = next_edge[itr]) {
                if (!istree[itr]) {
                    int j = to[itr];
                    int k = lca(i, j);
                    if (k != i && k != j) {
                        vector[k].push_back(Edge(w[itr], i, j));
                    }
                }
            }
        }
        
        ds[0].make_use(n);
        dfs(0, -1);
        for (int i = 0; i < n; i++) {
            if (ans[i] < INF) {
                printf("%d\n", ans[i]);
            } else {
                puts("inf");
            }
        }
    }
}
