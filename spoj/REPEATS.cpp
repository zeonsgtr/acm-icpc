#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const int N = 50000 + 10;
const int LOG = 17;

int n;
int log_2[N], w[N];
char a[N], buf[9];

struct SuffixArray {
    int buffer[2][N], *rank, *tmp;
    int height[N], sa[N];
    int min[N][LOG];

    SuffixArray() {
        rank = buffer[0];
        tmp = buffer[1];
    }

    void sort(int m) {
        std::fill(w, w + m, 0);
        for (int i = 0; i < n; i++) {
            w[rank[i]]++;
        }
        for (int i = 1; i < m; i++) {
            w[i] += w[i - 1];
        }
        for (int i = n - 1; i >= 0; i--) {
            sa[--w[rank[tmp[i]]]] = tmp[i];
        }
    }

    bool check(int i, int j, int l) {
        return tmp[i] == tmp[j] && tmp[i + l] == tmp[j + l];
    }

    void build() {
        int i, j, p, m(256);
        for (i = 0; i < n; i++) {
            rank[i] = a[i];
            tmp[i] = i;
        }
        sort(m);
        for (j = p = 1; p < n; j <<= 1, m = p) {
            for (p = 0, i = n - j; i < n; i++) {
                tmp[p++] = i;
            }
            for (i = 0; i < n; i++) {
                sa[i] >= j ? tmp[p++] = sa[i] - j : 0;
            }
            sort(m);
            std::swap(rank, tmp);
            for (rank[sa[0]] = 0, i = p = 1; i < n; i++) {
                rank[sa[i]] = check(sa[i - 1], sa[i], j) ? p - 1 : p++;
            }
        }
        for (i = p = 0; i < n; i++, p ? p-- : 0) {
            if (rank[i]) {
                j = sa[rank[i] - 1];
                for (; a[i + p] == a[j + p]; p++);
                min[rank[i]][0] = height[rank[i]] = p;
            }
        }
        for (j = 1; 1 << j < n; j++) {
            for (i = 1; i + (1 << j) <= n; i++) {
                min[i][j] = std::min(min[i][j - 1], min[i + (1 << j - 1)][j - 1]);
            }
        }
    }

    int lcp(int i, int j) {
        if (i > j) {
            std::swap(i, j);
        }
        i++;
        int l = log_2[j - i + 1];
        return std::min(min[i][l], min[j - (1 << l) + 1][l]);
    }
}data[2];

int main() {
    for (int i = 2; i < N; i++) {
        log_2[i] = log_2[i >> 1] + 1;
    }
    int test_count;
    scanf("%d", &test_count);
    while (test_count--) {
        scanf("%d", &n);
        for (int i = 0; i < n; i++) {
            scanf("%s", buf);
            a[i] = buf[0];
        }
        a[n++] = 0;
        data[0].build();
        std::reverse(a, a + --n);
        a[n++] = 0;
        data[1].build();
        int result = 1;
        std::reverse(a, a + --n);
        for (int l = 1; l <= n && n / l > result; l++) {
            for (int i = 0; i + l < n; i += l) {
                int t = data[1].lcp(data[1].rank[n - i - 1], data[1].rank[n - (i + l) - 1]);
                t += data[0].lcp(data[0].rank[i + 1], data[0].rank[i + l + 1]);
                result = std::max(result, t / l + 1);
            }
        }
        printf("%d\n", result);
    }
}

