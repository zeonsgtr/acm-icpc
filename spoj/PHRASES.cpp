#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const int N = 10 * 10001 + 10;

int n, m;
int belong[N], a[N];
char buf[N];
int buffer[2][N], *rank(buffer[0]), *tmp(buffer[1]);
int sa[N], height[N];

void sort(int m) {
    static int w[N];
    std::fill(w, w + m, 0);
    for (int i = 0; i < n; i++) {
        w[rank[i]]++;
    }
    for (int i = 1; i < m; i++) {
        w[i] += w[i - 1];
    }
    for (int i = n - 1; i >= 0; i--) {
        sa[--w[rank[tmp[i]]]] = tmp[i];
    }
}

bool check(int i, int j, int l) {
    return tmp[i] == tmp[j] && tmp[i + l] == tmp[j + l];
}

void build(int m) {
    int i, j, p(1);
    for (i = 0; i < n; i++) {
        tmp[i] = i;
        rank[i] = a[i];
    }
    sort(m);
    for (j = 1; p < n; j <<= 1, m = p) {
        for (i = n - j, p = 0; i < n; i++) {
            tmp[p++] = i;
        }
        for (i = 0; i < n; i++) {
            sa[i] >= j ? tmp[p++] = sa[i] - j : 0;
        }
        sort(m);
        std::swap(rank, tmp);
        for (rank[sa[0]] = 0, i = p = 1; i < n; i++) {
            rank[sa[i]] = check(sa[i - 1], sa[i], j) ? p - 1 : p++;
        }
    }
    for (i = p = 0; i < n; i++, p ? p-- : 0) {
        if (rank[i]) {
            j = sa[rank[i] - 1];
            for (; a[i + p] == a[j + p]; p++);
            height[rank[i]] = p;
        }
    }
}

int min[N], max[N], stamp[N], mark;

bool find(int l) {
    for (int i = 1, j; i < n; i = j) {
        int record = 0;
        mark++;
        stamp[belong[sa[i]]] = mark;
        min[belong[sa[i]]] = max[belong[sa[i]]] = sa[i];
        for (j = i + 1; j < n && height[j] >= l; j++) {
            int k = belong[sa[j]];
            if (stamp[k] != mark) {
                min[k] = max[k] = sa[j];
                stamp[k] = mark;
            }
            min[k] = std::min(min[k], sa[j]);
            max[k] = std::max(max[k], sa[j]);
            if (max[k] - min[k] >= l) {
                record |= 1 << k;
            }
        }
        if (__builtin_popcount(record) == m) {
            return true;
        }
    }
    return false;
}

int main() {
    int test_count;
    scanf("%d", &test_count);
    while (test_count--) {
        int size = 256;
        n = 0;
        scanf("%d", &m);
        for (int i = 0; i < m; i++) {
            scanf("%s", buf);
            int l = strlen(buf);
            for (int j = 0; j < l; j++) {
                belong[n] = i;
                a[n++] = buf[j];
            }
            belong[n] = i;
            a[n++] = i < m - 1 ? size++ : 0;
        }
        build(size);
        int lower = 1, upper = n, result = 0;
        while (lower <= upper) {
            int mid = lower + upper >> 1;
            if (find(mid)) {
                result = mid;
                lower = mid + 1;
            } else {
                upper = mid - 1;
            }
        }
        printf("%d\n", result);
    }
}

