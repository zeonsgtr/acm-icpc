import java.io.*;
import java.math.*;
import java.util.*;

public class Main {
    void run() {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);
        int n = in.nextInt();
        BigInteger[][] comb = new BigInteger[n + 1][n + 1];
        for (int i = 0; i <= n; i++) {
            comb[i][0] = BigInteger.ONE;
            for (int j = 1; j <= i; j++) {
                comb[i][j] = comb[i - 1][j - 1].add(comb[i - 1][j]);
            }
            for (int j = i + 1; j <= n; j++) {
                comb[i][j] = BigInteger.ZERO;
            }
        }
        BigInteger[] f = new BigInteger[n + 1];
        BigInteger result = BigInteger.ZERO;
        BigInteger power = BigInteger.ONE;
        BigInteger[] factor = new BigInteger[n + 1];
        factor[0] = BigInteger.ONE;
        for (int i = 1; i <= n; i++) {
            factor[i] = factor[i - 1].multiply(BigInteger.valueOf(i));
        }
        for (int i = n; i >= 0; i--) {
            f[i] = comb[n][i].multiply(factor[n - i]);
            for (int j = i + 1; j <= n; j++) {
                f[i] = f[i].subtract(comb[j][i].multiply(f[j]));
            }
            result = result.add(f[i].multiply(power));
            power = power.multiply(BigInteger.valueOf(2));
        }
        out.println(result);
        out.close();
    }
    public static void main(String[] args) {
        new Main().run();
    }
}
