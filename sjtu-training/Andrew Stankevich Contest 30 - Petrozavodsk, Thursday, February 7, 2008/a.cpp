#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <cctype>
#include <sstream>

#define SIZE(x) ((int)(x).size())
#define ALL(x) (x).begin(), (x).end()

#define Vector std::vector
#define String std::string

const int MAX_NAME = 10;

struct Author { //{{{
    String surname, name[MAX_NAME];
    void clear() {
        surname = "";
        for (int i = 0; i < MAX_NAME; i++) {
            name[i] = "";
        }
    }
    Author() {
        clear();
    }
    bool operator !=(const Author& o) const {
        if (surname != o.surname) {
            return true;
        }
        for (int i = 0; i < MAX_NAME; i++) {
            if (name[i] != o.name[i]) {
                return true;
            }
        }
        return false;
    }
    bool operator <(const Author& o) const {
        if (surname != o.surname) {
            return surname < o.surname;
        }
        for (int i = 0; i < MAX_NAME; i++) {
            if (name[i] != o.name[i]) {
                return name[i] < o.name[i];
            }
        }
        return false;
    }
    void print() {
        printf("%s", surname.c_str());
        for (int i = 0; i < MAX_NAME && SIZE(name[i]) > 0; i++) {
            putchar(' ');
            putchar(toupper(name[i][0]));
            putchar('.');
        }
    }
}; //}}}

struct Reference { //{{{
    bool isarticle;
    Vector <Author> authors;
    String title, journal, publisher;
    int year, volume, number, page[2];
    void clear() {
        isarticle = false;
        authors.clear();
        title = journal = publisher = "";
        year = volume = number = page[0] = page[1] = -1;
    }
    Reference() {
        clear();
    }
    void normalize() {
        std::sort(ALL(authors));
    }
    bool operator <(const Reference& o) const {
        int max = std::max(SIZE(authors), SIZE(o.authors));
        for (int i = 0; i < max; i++) {
            if (i < SIZE(authors) && i < SIZE(o.authors)) {
                if (authors[i] != o.authors[i]) {
                    return authors[i] < o.authors[i];
                }
            } else {
                return i >= SIZE(authors);
            }
        }
        if (title != o.title) {
            return title < o.title;
        }
        return volume < o.volume;
    }
    void authors_title_print() {
        for (int i = 0; i < SIZE(authors); i++) {
            putchar(' ');
            authors[i].print();
            putchar(i < SIZE(authors) - 1 ? ',' : ' ');
        }
        printf("%s", title.c_str());
    }
    void article_print(int id) {
        printf("[%d]", id);
        authors_title_print();
        printf(" // ");
        printf("%s", journal.c_str());
        if (volume != -1) {
            printf(", %d", volume);
        }
        if (number != -1) {
            printf(" (%d)", number);
        }
        printf(" -- %d", year);
        if (page[0] != -1) {
            if (page[0] == page[1]) {
                printf(" -- p. %d", page[0]);
            } else {
                printf(" -- pp. %d--%d", page[0], page[1]);
            }
        }
        puts("");
    }
    void book_print(int id) {
        printf("[%d]", id);
        authors_title_print();
        if (volume != -1) {
            printf(", Vol. %d", volume);
        }
        printf(" -- %s, %d\n", publisher.c_str(), year);
    }
}; //}}}

Vector <Reference> vector;

int n;
Author aut;
Reference ref;
char a[int(1e7) + 10];
char buf[int(1e6) + 10];

int next_space(int p) {
    for (p++; p < n && a[p] != ' ' && a[p] != '{'; p++);
    return p;
}

int next_char(int p) {
    for (p++; p < n && a[p] == ' '; p++);
    return p;
}

int next_break(int p) {
    for (p++; p < n && a[p] != '{' && a[p] != '}' && a[p] != '='; p++);
    if (a[p] == '=') {
        for (; a[p] != ','; p--);
    }
    return p;
}

int next_double(int p) {
    for (p++; p < n && a[p] != '"'; p++);
    return p;
}

int to_number(const String& s) {
    std::istringstream sin(s);
    int x;
    sin >> x;
    return x;
}

int main() {
    n = 0;
    while (gets(buf)) {
        int m = strlen(buf);
        for (int i = 0; i < m; i++) {
            a[n++] = buf[i];
        }
    }
    a[n] = 0;
    for (int i = 0, j; i < n; i++) {
        if (a[i] != '@') {
            continue;
        }
        ref.clear();
        j = next_space(i);
        if (String(a + i, a + j) == String("@article")) {
            ref.isarticle = true;
        } else {
            ref.isarticle = false;
        }
        i = next_break(i);
        while (true) {
            j = next_space(i = next_char(i));
            String token = String(a + i, a + j);
            int s = next_double(j);
            int t = next_break(s);
            int pos = t;
            s++;
            for (; a[t] != '"'; t--);
            String str(a + s, a + t);
            if (token == String("author")) {
                std::istringstream sin(str);
                String tmp;
                Vector <String> v;
                while (sin >> tmp) {
                    v.clear();
                    do {
                        if (tmp == String("and")) {
                            break;
                        }
                        v.push_back(tmp);
                    } while (sin >> tmp);
                    aut.clear();
                    aut.surname = v.back();
                    for (int i = 0; i < SIZE(v) - 1; i++) {
                        aut.name[i] = v[i];
                    }
                    ref.authors.push_back(aut);
                }
            } else {
                if (token == String("title")) {
                    ref.title = str;
                } else if (token == String("journal")) {
                    ref.journal = str;
                } else if (token == String("publisher")) {
                    ref.publisher = str;
                } else if (token == String("year")) {
                    ref.year = to_number(str);
                } else if (token == String("volume")) {
                    ref.volume = to_number(str);
                } else if (token == String("number")) {
                    ref.number = to_number(str);
                } else if (token == String("pages")) {
                    bool flag = true;
                    for (int p = 0; p < SIZE(str); p++) {
                        if (str[p] == '-') {
                            ref.page[0] = to_number(str.substr(0, p));
                            ref.page[1] = to_number(str.substr(p + 2, SIZE(str)));
                            flag = false;
                            break;
                        }
                    }
                    if (flag) {
                        ref.page[0] = ref.page[1] = to_number(str);
                    }
                }
            }
            i = pos;
            if (a[pos] == '}') {
                break;
            }
        }
        ref.normalize();
        vector.push_back(ref);
    }
    std::sort(ALL(vector));
    for (int i = 0; i < SIZE(vector); i++) {
        if (vector[i].isarticle) {
            vector[i].article_print(i + 1);
        } else {
            vector[i].book_print(i + 1);
        }
    }
}

