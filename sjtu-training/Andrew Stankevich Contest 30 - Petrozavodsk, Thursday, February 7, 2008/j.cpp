#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const double EPS = 1e-8;
const double PI = acos(-1.0);

inline int signum(double x) {
    return x < -EPS ? -1 : x > EPS;
}

struct Point {
    double x, y;
    Point() {}
    Point(double x, double y): x(x), y(y) {}

    void show() {
        printf("%.9f %.9f\n", x, y);
    }

    double length() const {
        return sqrt(x * x + y * y);
    }

    Point unit() {
        double l = length();
        return Point(x / l, y / l);
    }

    Point rotate(double alpha) {
        return Point(cos(alpha) * x - sin(alpha) * y, sin(alpha) * x + cos(alpha) * y);
    }
};

inline Point operator +(const Point& a, const Point& b) {
    return Point(a.x + b.x, a.y + b.y);
}

inline Point operator -(const Point& a, const Point& b) {
    return Point(a.x - b.x, a.y - b.y);
}

inline Point operator *(const Point& a, double l) {
    return Point(a.x * l, a.y * l);
}

inline Point operator /(const Point& a, double l) {
    return Point(a.x / l, a.y / l);
}

inline double dist(const Point& a, const Point& b) {
    return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}

inline double det(const Point& a, const Point& b) {
    return a.x * b.y - a.y * b.x;
}

struct Line {
    Point s, t;
    Line() {}
    Line(const Point& s, const Point& t): s(s), t(t) {}
};

Point line_intersection(const Line& a, const Line& b) {
    double s1 = det(a.t - a.s, b.s - a.s);
    double s2 = det(a.t - a.s, b.t - a.s);
    return (b.s * s2 - b.t * s1) / (s2 - s1);
}

struct Circle {
    Point o;
    double r;
    Circle(const Point& o, double r): o(o), r(r) {}

    bool contain(const Point& p) const {
        return signum(dist(o, p) - r) <= 0;
    }
};

std::pair <Line, Line> tangent(const Circle& c, const Point& p) {
    double theta = asin(c.r / dist(c.o, p));
    return std::make_pair(Line(p, p + (c.o - p).rotate(theta)), Line(p, p + (c.o - p).rotate(-theta)));
}

std::pair <Line, Line> internal_common_tangent(const Circle& a, const Circle& b) {
    return tangent(a, a.o + (b.o - a.o).unit() * (a.r / (a.r + b.r) * dist(a.o, b.o)));
}

typedef std::pair <double, double> Pair;

int n;
double width, height;
std::vector <Circle> circles;
std::vector <Line> lines;

bool check_validity(const Point& o) {
    std::vector <Pair> events;
    for (int i = 0; i < n; i++) {
        if (signum(dist(circles[i].o, o) - circles[i].r) == 0) {
            Point p = (circles[i].o - o).rotate(PI * 0.5);
            double s = atan2(p.y, p.x);
            p = (circles[i].o - o).rotate(-PI * 0.5);
            double t = atan2(p.y, p.x);
            if (s > t) {
                std::swap(s, t);
            }
            p = circles[i].o - o;
            double r = atan2(p.y, p.x);
            if (signum(r - s) >= 0 && signum(r - t) <= 0) {
                events.push_back(Pair(s, t));
            } else {
                events.push_back(Pair(-PI, s));
                events.push_back(Pair(t, PI));
            }
            continue;
        }
        if (circles[i].contain(o)) {
            return false;
        }
        std::pair <Line, Line> l = tangent(circles[i], o);
        double s = atan2((l.first.t - o).y, (l.first.t - o).x);
        double t = atan2((l.second.t - o).y, (l.second.t - o).x);
        if (s > t) {
            std::swap(s, t);
        }
        if (signum(t - s - PI) > 0) {
            events.push_back(Pair(-PI, s));
            events.push_back(Pair(t, PI));
        } else {
            events.push_back(Pair(s, t));
        }
    }
    for (int i = 0; i < SIZE(events); i++) {
        for (int j = 0; j < i; j++) {
            if (signum(std::min(events[i].second, events[j].second) - std::max(events[i].first, events[j].first)) > 0) {
                return false;
            }
        }
    }
    return true;
}

int main() {
    scanf("%d%lf%lf", &n, &width, &height);
    if (n == 1) {
        puts("0 0");
        return 0;
    }
    for (int i = 0; i < n; i++) {
        double x, y, r;
        scanf("%lf%lf%lf", &x, &y, &r);
        circles.push_back(Circle(Point(x, y), r));
        for (int j = 0; j < i; j++) {
            if (signum(dist(circles[i].o, circles[j].o) - circles[i].r - circles[j].r) > 0) {
                std::pair <Line, Line> line = internal_common_tangent(circles[i], circles[j]);
                lines.push_back(line.first);
                lines.push_back(line.second);
            } else {
                lines.push_back(Line(circles[i].o, circles[j].o));
                Point p = circles[i].o + (circles[j].o - circles[i].o).unit() * 
                    (circles[i].r / (circles[i].r + circles[j].r) * dist(circles[i].o, circles[j].o));
                lines.push_back(Line(p, p + (p - circles[i].o).rotate(PI * 0.5)));
            }
        }
    }
    for (int i = 0; i < SIZE(lines); i++) {
        for (int j = 0; j < i; j++) {
            if (signum(det(lines[i].s - lines[i].t, lines[j].s - lines[j].t))) {
                Point p = line_intersection(lines[i], lines[j]);
                if (signum(p.x) < 0 || signum(p.x - width) > 0) {
                    continue;
                }
                if (signum(p.y) < 0 || signum(p.y - height) > 0) {
                    continue;
                }
                if (check_validity(p)) {
                    p.show();
                    return 0;
                }
            }
        }
    }
    puts("No Zen");
}

