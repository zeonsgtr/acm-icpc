#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const int N = 101;
const int INF = int(1e9);
const int MASK[] = {0, 1, 2, 4};

int n;
int a[N], b[N];
int f[N][N][2][N];
int g[N][N][2][N][2];
int p[N][N][2][N][3];
int begin[N], end[N];
char s[N];

int main() {
    gets(s);
    n = strlen(s);
    for (int i = 0; i < n; i++) {
        a[i] = s[i] == 'A' ? 0 : s[i] == 'C' ? 1 : s[i] == 'G' ? 2 : 3;
    }
    gets(s);
    for (int i = 0; i < n; i++) {
        b[i] = s[i] == 'A' ? 0 : s[i] == 'C' ? 1 : s[i] == 'G' ? 2 : 3;
    }

    for (int i = 0; i <= n; i++) {
        for (int j = 0; j <= n; j++) {
            for (int k = 0; k < 2; k++) {
                for (int l = 0; l <= n; l++) {
                    f[i][j][k][l] = INF;
                }
            }
        }
    }
    f[0][0][0][0] = 0;
    int u[3], v[3];
    for (int i = 0; i < n; i++) {
        for (u[0] = 0; u[0] <= n; u[0]++) {
            for (u[1] = 0; u[1] < 2; u[1]++) {
                for (u[2] = 0; u[2] <= n; u[2]++) {
                    if (f[i][u[0]][u[1]][u[2]] == INF) {
                        continue;
                    }
                    int value = f[i][u[0]][u[1]][u[2]];
                    for (int j = 0; j < 4; j++) {
                        bool ok = true;
                        for (int d = 0; d < 3 && ok; d++) {
                            ok &= u[d] - (MASK[j] >> d & 1) >= 0;
                        }
                        if (!ok) {
                            continue;
                        }
                        for (int k = 0; k < 4; k++) {
                            for (int d = 0; d < 3; d++) {
                                v[d] = u[d] - (MASK[j] >> d & 1) + (MASK[k] >> d & 1);
                            }
                            if (v[1] == 2 || v[0] && v[2]) {
                                continue;
                            }
                            //printf("j = %d, k = %d\n", j, k);
                            //printf("%d %d %d %d\n", i, u[0], u[1], u[2]);
                            int delta = v[0] * 1 + v[1] * 2 + v[2] * 3;
                            //printf("delta = %d\n", delta);
                            //printf("%d %d\n", (a[i] + delta) % 4, b[i]);
                            if ((a[i] + delta) % 4 == b[i] && value + (k > 0) < f[i + 1][v[0]][v[1]][v[2]]) {
                                f[i + 1][v[0]][v[1]][v[2]] = value + (k > 0);
                                g[i + 1][v[0]][v[1]][v[2]][0] = j;
                                g[i + 1][v[0]][v[1]][v[2]][1] = k;
                                p[i + 1][v[0]][v[1]][v[2]][0] = u[0];
                                p[i + 1][v[0]][v[1]][v[2]][1] = u[1];
                                p[i + 1][v[0]][v[1]][v[2]][2] = u[2];
                            }
                        }
                    }
                }
            }
        }
    }
    int result = INF;
    for (int i = 0; i <= n; i++) {
        for (int j = 0; j < 2; j++) {
            for (int k = 0; k <= n; k++) {
                if (i + j + k <= 1 && f[n][i][j][k] < result) {
                    result = f[n][i][j][k];
                    v[0] = i;
                    v[1] = j;
                    v[2] = k;
                }
            }
        }
    }
    printf("%d\n", result);
    for (int i = 0; i < 3; i++) {
        if (v[i] == 1) {
            end[n] = i + 1;
        }
    }
    for (int i = n; i > 0; i--) {
        int mask = MASK[g[i][v[0]][v[1]][v[2]][0]];
        for (int j = 0; j < 3; j++) {
            if (mask >> j & 1) {
                end[i - 1] = j + 1;
            }
        }
        mask = MASK[g[i][v[0]][v[1]][v[2]][1]];
        for (int j = 0; j < 3; j++) {
            if (mask >> j & 1) {
                begin[i] = j + 1;
            }
        }
        u[0] = p[i][v[0]][v[1]][v[2]][0];
        u[1] = p[i][v[0]][v[1]][v[2]][1];
        u[2] = p[i][v[0]][v[1]][v[2]][2];
        memcpy(v, u, sizeof(int) * 3);
    }
    for (int i = 1; i <= n; i++) {
        if (begin[i] > 0) {
            for (int j = i; j <= n; j++) {
                if (end[j] == begin[i]) {
                    printf("%d %d %d\n", i, j, begin[i]);
                    begin[i] = end[j] = 0;
                    break;
                }
            }
        }
    }
}

