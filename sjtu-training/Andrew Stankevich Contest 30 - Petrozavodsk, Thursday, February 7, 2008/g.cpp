#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <queue>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const int N = 100000 + 10;
const int INF = int(1e9);

int n;
int a[N], b[N];
std::vector <int> values;
std::vector <std::vector <int> > list;

struct Node {
    int k;
    Node(int k): k(k) {}
    bool operator <(const Node& o) const {
        return b[k] > b[o.k];
    }
};

int main() {
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d%d", a + i, b + i);
        values.push_back(a[i]);
    }
    std::sort(ALL(values));
    values.erase(std::unique(ALL(values)), values.end());
    list.resize(SIZE(values));
    for (int i = 0; i < n; i++) {
        int p = std::lower_bound(ALL(values), a[i]) - values.begin();
        list[p].push_back(i);
    }
    int i = 0;
    long long sweep_line = values[0];
    long long result = 0;
    std::priority_queue <Node> queue;
    while (i < SIZE(list) || !queue.empty()) {
        if (i < SIZE(list) && values[i] == sweep_line || queue.empty()) {
            for (int j = 0; j < SIZE(list[i]); j++) {
                queue.push(Node(list[i][j]));
            }
            sweep_line = values[i++];
        }
        int k = queue.top().k;
        queue.pop();
        int delta = std::min(b[k], i < SIZE(list) ? values[i] - int(sweep_line) : INF);
        b[k] -= delta;
        sweep_line += delta;
        if (b[k] == 0) {
            result += sweep_line;
        } else {
            queue.push(Node(k));
        }
    }
    std::cout << result << std::endl;
}

