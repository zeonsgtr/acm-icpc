#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define SIZE(x) ((int)(x).size())

const int N = 555;

int n, m;
int color[N];
bool expanded[N];
std::vector <std::pair <int, int> > edges;
std::vector <int> linked[N];

int other(int k, int x) {
    return edges[k].first == x ? edges[k].second : edges[k].first;
}

int getcolor(int a, int b) {
    if (a > b) {
        std::swap(a, b);
    }
    return a == 1 ? 0 : b == 2 ? 1 : 2;
}

void expand(int s, int t) {
    static int stamp[N] = {}, mark(0);
    if (expanded[s]) {
        std::swap(s, t);
    }
    mark++;
    expanded[s] = true;
    std::vector <int> queue;
    for (int i = 0; i < SIZE(linked[s]); i++) {
        int j = other(linked[s][i], s);
        stamp[j] = mark;
        if (color[j] != -1) {
            queue.push_back(j);
        }
    }
    for (int i = 0; i < SIZE(queue); i++) {
        int x = queue[i];
        for (int j = 0; j < SIZE(linked[x]); j++) {
            int y = other(linked[x][j], x);
            if (stamp[y] == mark && color[y] == -1) {
                color[y] = getcolor(color[x], color[s]);
                queue.push_back(y);
            }
        }
    }
}

bool bfs(int x) {
    std::fill(color, color + n, -1);
    std::fill(expanded, expanded + n, false);
    color[x] = 0;
    if (n == 1) {
        return true;
    }
    color[other(linked[x].front(), x)] = 1;
    expand(x, other(linked[x].front(), x));
    while (true) {
        bool flag = false;
        for (int i = 0; i < SIZE(edges); i++) {
            int u = edges[i].first;
            int v = edges[i].second;
            if (color[u] != -1 && color[v] != -1 && (!expanded[u] || !expanded[v])) {
                expand(edges[i].first, edges[i].second);
                flag = true;
            }
        }
        if (!flag) {
            break;
        }
    }
    for (int i = 0; i < SIZE(edges); i++) {
        if (color[edges[i].first] == color[edges[i].second]) {
            return false;
        }
    }
    return true;
}

int main() {
    scanf("%d%d", &n, &m);
    for (int i = 0; i < m; i++) {
        int s, t;
        scanf("%d%d", &s, &t);
        s--, t--;
        edges.push_back(std::make_pair(s, t));
        linked[s].push_back(i);
        linked[t].push_back(i);
    }
    if (bfs(0)) {
        puts("Plan OK");
        for (int i = 0; i < n; i++) {
            putchar(color[i] == 0 ? 'R' : color[i] == 1 ? 'G' : 'B');
        }
    } else {
        puts("Plan failed");
    }
}

