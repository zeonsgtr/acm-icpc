#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const int N = 200 + 10;
const int MOD = int(1e9) + 7;

int n, m;
int f[N][N], total;
std::vector <int> adj[N];

int main() {
    scanf("%d%d", &n, &m);
    for (int i = 0; i < m; i++) {
        int s, t;
        scanf("%d%d", &s, &t);
        s--, t--;
        f[s][t]++;
        adj[s].push_back(t);
    }
    for (int i = n - 1; i >= 0; i--) {
        f[i][i] = 1;
        for (int j = 0; j < SIZE(adj[i]); j++) {
            int x = adj[i][j];
            for (int k = x + 1; k < n; k++) {
                (f[i][k] += f[x][k]) %= MOD;
            }
        }
        for (int j = i + 1; j < n; j++) {
            total += f[i][j] > 0;
        }
    }
    printf("%d\n", total);
    scanf("%d", &m);
    char ctrl[9];
    while (m--) {
        int u, v;
        scanf("%s %d %d", ctrl, &u, &v);
        u--, v--;
        if (ctrl[0] == '?') {
            puts(f[u][v] ? "YES" : "NO");
        } else {
            for (int i = u; i >= 0; i--) {
                for (int j = v; j < n; j++) {
                    int old = f[i][j];
                    if (i == u && j == v) {
                        f[i][j] += ctrl[0] == '+' ? 1 : -1;
                        total += ctrl[0] == '+' ? old == 0 : -1 * (f[i][j] == 0);
                        continue;
                    }
                    if (ctrl[0] == '+') {
                        (f[i][j] += 1LL * f[i][u] * f[v][j] % MOD) %= MOD;
                        total += old == 0 && f[i][j];
                    } else {
                        (f[i][j] -= 1LL * f[i][u] * f[v][j] % MOD) %= MOD;
                        total -= old && f[i][j] == 0;
                    }
                }
            }
            printf("%d\n", total);
        }
    }
}

