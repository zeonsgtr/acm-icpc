#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const int N = 100 + 10;

int n, m, k;
std::vector <int> adj[N];
int degree[N], rank[N];
int sequence[N][N];

bool cmp(int i, int j) {
    return degree[i] < degree[j];
}

bool dfs(int d, int cnt) {
    std::sort(sequence[d], sequence[d] + d, cmp);
    int lower = 0;
    for (int i = 0; i < d; i++) {
        lower += std::max(0, degree[sequence[d][i]] - i);
    }
    if (lower + cnt > k) {
        return false;
    }
    if (d == 1) {
        rank[sequence[d][0]] = d;
        return true;
    }
    for (int i = 0; i < d; i++) {
        int x = sequence[d][i];
        rank[x] = d;
        for (int j = 0; j < SIZE(adj[x]); j++) {
            degree[adj[x][j]]--;
        }
        for (int j = 0; j < d; j++) {
            sequence[d - 1][j] = sequence[d][j];
        }
        std::swap(sequence[d - 1][i], sequence[d - 1][d - 1]);
        if (dfs(d - 1, cnt + degree[x])) {
            return true;
        }
        for (int j = 0; j < SIZE(adj[x]); j++) {
            degree[adj[x][j]]++;
        }
    }
    return false;
}

int main() {
    scanf("%d%d", &n, &m);
    k = n * (n - 1) / 2 - m;
    std::vector <std::pair <int, int> > edges;
    for (int i = 0; i < m; i++) {
        int s, t;
        scanf("%d%d", &s, &t);
        s--, t--;
        degree[t]++;
        adj[s].push_back(t);
        edges.push_back(std::make_pair(s, t));
    }
    for (int i = 0; i < n; i++) {
        sequence[n][i] = i;
    }
    if (!dfs(n, 0)) {
        puts("-1");
    } else {
        std::vector <int> v;
        for (int i = 0; i < m; i++) {
            if (rank[edges[i].first] < rank[edges[i].second]) {
                v.push_back(i);
            }
        }
        printf("%d\n", SIZE(v));
        for (int i = 0; i < SIZE(v); i++) {
            printf("%d%c", v[i] + 1, i < SIZE(v) - 1 ? ' ' : '\n');
        }
    }
}

