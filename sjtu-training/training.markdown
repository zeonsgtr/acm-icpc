# March-2013

## 3.15(Wed) -> Andrew Stankevich Contest 30, Petrozavodsk, Thursday, February 7, 2008

### A. BibTEX
* tag: implementation
* state: 4Y
* identifier: TRvIF
* remark: 写了很久的模拟题，现在还处在初级阶段和积累经验阶段，不追求AC速度，只求能AC。

### B. Signed Derangements
* tags: couting, permutation
* state: 1Y
* identifier: TH

### C. Electricity 2
* tags: construction, search
* state: N/A -> 2Y
* identifier: TRtIF
* remark: 搜索可以过，但是正解是构造，还不会构造。

### D. Currency Exchange
* tags: digital dp
* state: N/A
* identifier: F

### E. New Mayors
* tags: wit, observation, bfs
* state: 5Y
* identifier: F

### F. \sqrt{NIM}
* tags: sg
* state: N/A
* identifier: F

### G. Pulp Fiction
* tags: greedy
* state: 3Y
* identifier: F

### H. Settling the Universe Up
* tags: bitset, brute force, wit
* state: N/A -> 1Y
* identifier: F

### I. Segment Transformation
* tags: dp, interval dp
* state: N/A -> 2Y
* identifier: TH, TP

### J. Zen Garden
* tags: geometry
* state: N/A -> 8Y
* identifier: F, TP

# explanation of identifier
## TRvIF
to review in the futuer, to summarize general experience

## TRtIF
to rethink in the futuer, to implement my two steps of one problem

## F
finished

## TH
to think

## TSE
to solve, easy problems

## TSH
to solve, hard problems

# some topics to investigate
## TP, threshold property 
counter = I

## GD, great difficulty
counter = 0
