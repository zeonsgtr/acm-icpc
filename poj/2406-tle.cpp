#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const int N = int(1e6) + 10;

int n;
char a[N];
int buffer[2][N], *rank(buffer[0]), *tmp(buffer[1]);
int height[N], sa[N];

void sort(int m) {
    static int w[N];
    std::fill(w, w + m, 0);
    for (int i = 0; i < n; i++) {
        w[rank[i]]++;
    }
    for (int i = 1; i < m; i++) {
        w[i] += w[i - 1];
    }
    for (int i = n - 1; i >= 0; i--) {
        sa[--w[rank[tmp[i]]]] = tmp[i];
    }
}

bool check(int i, int j, int l) {
    return tmp[i] == tmp[j] && tmp[i + l] == tmp[j + l];
}

void prepare() {
    int i, j, p, m(256);
    for (i = 0; i < n; i++) {
        rank[i] = a[i];
        tmp[i] = i;
    }
    sort(m);
    for (j = p = 1; p < n; j <<= 1, m = p) {
        for (p = 0, i = n - j; i < n; i++) {
            tmp[p++] = i;
        }
        for (i = 0; i < n; i++) {
            sa[i] >= j ? tmp[p++] = sa[i] - j : 0;
        }
        sort(m);
        std::swap(rank, tmp);
        for (rank[sa[0]] = 0, i = p = 1; i < n; i++) {
            rank[sa[i]] = check(sa[i - 1], sa[i], j) ? p - 1 : p++;
        }
    }
    for (i = p = 0; i < n; i++, p ? p-- : 0) {
        if (rank[i]) {
            j = sa[rank[i] - 1];
            for (; a[i + p] == a[j + p]; p++);
            height[rank[i]] = p;
        }
    }
}

int main() {
    while (gets(a)) {
        n = strlen(a);
        if (n == 1 && a[0] == '.') {
            break;
        }
        a[n++] = 0;
        prepare();
        int result = 1;
        for (int i = 1; i < n - 1; i++) {
            if ((n - 1) % i == 0) {
                bool ok = true;
                int min = n;
                for (int j = rank[0] + 1; j < n && ok; j++) {
                    min = std::min(min, height[j]);
                    if (sa[j] % i == 0 && sa[j] < n - 1) {
                        ok = min >= i;
                    }
                }
                min = n;
                for (int j = rank[0] - 1; j >= 0 && ok; j--) {
                    min = std::min(min, height[j + 1]);
                    if (sa[j] % i == 0 && sa[j] < n - 1) {
                        ok = min >= i;
                    }
                }
                if (ok) {
                    result = (n - 1) / i;
                    break;
                }
            }
        }
        printf("%d\n", result);
    }
}

