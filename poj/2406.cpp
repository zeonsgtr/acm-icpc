#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const int N = 2 * int(1e6) + 10;

int n, m;
char a[N], s[N];
int fail[N];

void kmp() {
    fail[0] = -1;
    for (int i = 1, j; i <= m; fail[i++] = j + 1) {
        for (j = fail[i - 1]; j >= 0 && s[i] != s[j + 1]; j = fail[j]);
    }
}

int main() {
    while (gets(a)) {
        n = strlen(a);
        if (a[0] == '.' && n == 1) {
            break;
        }
        int result = 1;
        for (int i = 1; i < n; i++) {
            if (n % i == 0) {
                m = i + n + 1;
                memcpy(s + 1, a, sizeof(char) * i);
                s[i + 1] = 0;
                memcpy(s + i + 2, a, sizeof(char) * n);
                kmp();
                bool ok = true;
                for (int j = i + i + 1; j <= m; j += i) {
                    if (fail[j] < i) {
                        ok = false;
                        break;
                    }
                }
                if (ok) {
                    result = n / i;
                    break;
                }
            }
        }
        printf("%d\n", result);
    }
}

