#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const int N = 2 * int(1e5) + 10;

int n, m, l;
char buf[N], a[N];
int buffer[2][N], *rank(buffer[0]), *tmp(buffer[1]);
int sa[N], height[N];

void sort(int m) {
    static int w[N];
    std::fill(w, w + m, 0);
    for (int i = 0; i < n; i++) {
        w[rank[i]]++;
    }
    for (int i = 1; i < m; i++) {
        w[i] += w[i - 1];
    }
    for (int i = n - 1; i >= 0; i--) {
        sa[--w[rank[tmp[i]]]] = tmp[i];
    }
}

bool check(int i, int j, int l) {
    return tmp[i] == tmp[j] && tmp[i + l] == tmp[j + l];
}

void build() {
    int i, j, p, m(256);
    for (i = 0; i < n; i++) {
        rank[i] = a[i];
        tmp[i] = i;
    }
    sort(m);
    for (j = p = 1; p < n; j <<= 1, m = p) {
        for (i = n - j, p = 0; i < n; i++) {
            tmp[p++] = i;
        }
        for (i = 0; i < n; i++) {
            sa[i] >= j ? tmp[p++] = sa[i] - j : 0;
        }
        sort(m);
        std::swap(rank, tmp);
        for (rank[sa[0]] = 0, i = p = 1; i < n; i++) {
            rank[sa[i]] = check(sa[i - 1], sa[i], j) ? p - 1 : p++;
        }
    }
    for (i = p = 0; i < n; i++, p ? p-- : 0) {
        if (rank[i]) {
            j = sa[rank[i] - 1];
            for (; a[i + p] == a[j + p]; p++);
            height[rank[i]] = p;
        }
    }
}

int stack[N], top;
long long sum[N][2];

void solve() {
    long long result = 0;
    top = 0;
    sum[0][0] = sum[0][1] = 0;
    for (int i = 1; i < n; i++) {
        long long count[2] = {};
        bool flag = true;
        if (height[i] < m) {
            top = 0;
        } else {
            int j = height[i] - m + 1;
            for (; top > 0 && height[stack[top]] >= height[i]; top--) {
                if (flag) {
                    count[sa[stack[top]] > l] += height[i] - m + 1;
                    if ((sa[stack[top]] < l) ^ (sa[i] < l)) {
                        result += height[i] - m + 1;
                    }
                }
                flag = false;
                int k = height[stack[top]] - m + 1;
                long long delta = (sum[top][sa[i] < l] - sum[top - 1][sa[i] < l]) / k;
                result += j * delta;
                count[0] += (sum[top][0] - sum[top - 1][0]) / k * j;
                count[1] += (sum[top][1] - sum[top - 1][1]) / k * j;
            }
        }
        if (top > 0) {
            result += sum[top][sa[i] < l];
            if (flag) {
                if ((sa[stack[top]] < l) ^ (sa[i] < l)) {
                    result += height[i] - m + 1;
                }
                count[sa[stack[top]] > l] += height[i] - m + 1;
            }
        }
        stack[++top] = i;
        sum[top][0] = sum[top - 1][0] + count[0];
        sum[top][1] = sum[top - 1][1] + count[1];
    }
    std::cout << result << std::endl;
}

int main() {
    while (scanf("%d", &m) == 1 && m > 0) {
        gets(buf);
        gets(buf);
        n = l = strlen(buf);
        memcpy(a, buf, sizeof(char) * l);
        a[n++] = '*';
        gets(buf);
        memcpy(a + n, buf, sizeof(char) * strlen(buf));
        n += strlen(buf);
        a[n++] = 0;
        build();
        solve();
    }
}

