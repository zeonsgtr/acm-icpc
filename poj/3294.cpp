#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())
#define index INDEX

const int N = 100 * 1001 + 10;

int n, m;
int index[N], a[N];
char buf[N];
int buffer[2][N], *rank(buffer[0]), *tmp(buffer[1]);
int sa[N], height[N];

void sort(int m) {
    static int w[N];
    std::fill(w, w + m, 0);
    for (int i = 0; i < n; i++) {
        w[rank[i]]++;
    }
    for (int i = 1; i < m; i++) {
        w[i] += w[i - 1];
    }
    for (int i = n - 1; i >= 0; i--) {
        sa[--w[rank[tmp[i]]]] = tmp[i];
    }
}

bool check(int i, int j, int l) {
    return tmp[i] == tmp[j] && tmp[i + l] == tmp[j + l];
}

void build(int m) {
    int i, j, p;
    for (i = 0; i < n; i++) {
        tmp[i] = i;
        rank[i] = a[i];
    }
    sort(m);
    for (j = p = 1; p < n; j <<= 1, m = p) {
        for (i = n - j, p = 0; i < n; i++) {
            tmp[p++] = i;
        }
        for (i = 0; i < n; i++) {
            sa[i] >= j ? tmp[p++] = sa[i] - j : 0;
        }
        sort(m);
        std::swap(rank, tmp);
        for (rank[sa[0]] = 0, i = p = 1; i < n; i++) {
            rank[sa[i]] = check(sa[i - 1], sa[i], j) ? p - 1 : p++;
        }
    }
    for (i = p = 0; i < n; i++, p ? p-- : 0) {
        if (rank[i]) {
            j = sa[rank[i] - 1];
            for (; a[i + p] == a[j + p]; p++);
            height[rank[i]] = p;
        }
    }
    /*
    for (int i = 1; i < n; i++) {
        for (int j = sa[i]; j < n; j++) {
            putchar(a[j] >= 256 ? '*' : a[j]);
        }
        puts("");
    }
    for (int i = 1; i < n; i++) {
        printf("h[%d] = %d, index[%d] = %d\n", i, height[i], i, index[sa[i]]);
    }
    */
}

int length;
int stamp[N], mark(1);
std::vector <int> position;

bool cmp(int i, int j) {
    for (int k = 0; k < length; k++) {
        if (a[i + k] != a[j + k]) {
            return a[i + k] < a[j + k];
        }
    }
    return false;
}

bool find(int l) {
    position.clear();
    for (int i = 1, j; i < n; i = j) {
        mark++;
        stamp[index[sa[i]]] = mark;
        int counter = 1;
        for (j = i + 1; j < n && height[j] >= l; j++) {
            if (stamp[index[sa[j]]] != mark) {
                stamp[index[sa[j]]] = mark;
                counter++;
            }
        }
        //printf("i = %d, counter = %d\n", i, counter);
        if (counter > m / 2) {
            position.push_back(sa[i]);
        }
    }
    return !position.empty();
}

int main() {
    bool flag = false;
    while (scanf("%d", &m) == 1 && m > 0) {
        gets(buf);
        if (flag) {
            puts("");
        }
        flag = true;
        int count = 256;
        n = 0;
        for (int i = 0; i < m; i++) {
            gets(buf);
            int l = strlen(buf);
            for (int j = 0; j < l; j++) {
                index[n] = i;
                a[n++] = buf[j];
            }
            index[n] = i;
            a[n++] = i < m - 1 ? count++ : 0;
        }
        //for (int i = 0; i < n; i++) printf("%d", index[i]); exit(0);
        build(count);
        int lower = 1, upper = n, result = 0;
        while (lower <= upper) {
            int mid = lower + upper >> 1;
            if (find(mid)) {
                result = mid;
                lower = mid + 1;
            } else {
                upper = mid - 1;
            }
        }
        if (result > 0) {
            find(length = result);
            std::sort(ALL(position), cmp);
            for (int i = 0; i < SIZE(position); i++) {
                for (int j = 0; j < length; j++) {
                    putchar(a[position[i] + j]);
                }
                puts("");
            }
        } else {
            puts("?");
        }
    }
}

