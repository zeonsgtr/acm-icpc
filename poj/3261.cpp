#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const int N = 20000 + 10;
const int M = int(1e6) + 10;

int n, m, a[N];
int buffer[2][N], *rank = buffer[0], *tmp = buffer[1];
int sa[N], height[N];

bool cmp(int i, int j) {
    return a[i] < a[j];
}

bool check(int i, int j, int l) {
    return tmp[i] == tmp[j] && tmp[i + l] == tmp[j + l];
}

void radix_sort(int m) {
    static int w[N];
    std::fill(w, w + m, 0);
    for (int i = 0; i < n; i++) {
        w[rank[i]]++;
    }
    for (int i = 1; i < m; i++) {
        w[i] += w[i - 1];
    }
    for (int i = n - 1; i >= 0; i--) {
        sa[--w[rank[tmp[i]]]] = tmp[i];
    }
}

void prepare() {
    int i, j, p, m;
    for (i = 0; i < n; i++) {
        sa[i] = i;
    }
    std::sort(sa, sa + n, cmp);
    rank[sa[0]] = 0;
    for (i = p = 1; i < n; i++) {
        rank[sa[i]] = a[sa[i - 1]] == a[sa[i]] ? p - 1 : p++;
    }
    for (j = 1, m = p; p < n; j <<= 1, m = p) {
        for (i = p = 0; i < n; i++) {
            if (sa[i] >= j) {
                tmp[p++] = sa[i] - j;
            }
        }
        for (i = n - j; i < n; i++) {
            tmp[p++] = i;
        }
        radix_sort(m);
        std::swap(rank, tmp);
        rank[sa[0]] = 0;
        for (i = p = 1; i < n; i++) {
            rank[sa[i]] = check(sa[i - 1], sa[i], j) ? p - 1 : p++;
        }
    }
    for (i = p = 0; i < n; i++, p ? p-- : p) {
        if (rank[i]) {
            j = sa[rank[i] - 1];
            for (; a[i + p] == a[j + p]; p++);
            height[rank[i]] = p;
        }
    }
}

int main() {
    scanf("%d%d", &n, &m);
    for (int i = 0; i < n; i++) {
        scanf("%d", a + i);
        a[i]++;
    }
    a[n++] = 0;
    prepare();
    int result = 0, lower = 1, upper = n;
    while (lower <= upper) {
        int mid = lower + upper >> 1;
        bool have = false;
        for (int i = 0, j; i < n; i = j) {
            for (j = i + 1; j < n && height[j] >= mid; j++);
            if (j - i >= m) {
                have = true;
                break;
            }
        }
        if (have) {
            result = mid;
            lower = mid + 1;
        } else {
            upper = mid - 1;
        }
    }
    printf("%d\n", result);
}

