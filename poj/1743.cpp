#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define ALL(x) (x).begin(), (x).end()
#define SIZE(x) ((int)(x).size())

const int N = 20000 + 10;

int n, m, a[N];
int sa[N], buffer[2][N];
int *rank = buffer[0], *old_rank = buffer[1];
int height[N];

bool cmp(int i, int j) {
    return rank[i] < rank[j] || rank[i] == rank[j] && rank[i + m] < rank[j + m];
}

bool identical(int i, int j) {
    return old_rank[i] == old_rank[j] && old_rank[i + m] == old_rank[j + m];
}

int main() {
    while (scanf("%d", &n) == 1 && n) {
        for (int i = 0; i < n; i++) {
            scanf("%d", a + i);
            if (i) {
                a[i - 1] -= a[i] - 100;
            }
        }
        if (n == 1) {
            puts("0");
            continue;
        }
        a[n] = 0;
        for (int i = 0; i < n; i++) {
            rank[i] = a[i];
            sa[i] = i;
        }
        m = 0;
        std::sort(sa, sa + n, cmp);
        for (m = 1; m < n; m <<= 1) {
            std::sort(sa, sa + n, cmp);
            std::swap(rank, old_rank);
            rank[sa[0]] = 0;
            for (int i = 1, j = 1; i < n; i++) {
                rank[sa[i]] = identical(sa[i - 1], sa[i]) ? j - 1 : j++;
            }
        }
        for (int i = 0, k = 0; i < n; i++, k = std::max(0, k - 1)) {
            if (rank[i]) {
                int j = sa[rank[i] - 1];
                for (; a[i + k] == a[j + k]; k++);
                height[rank[i]] = k;
            }
        }
        int result = 0, lower = 1, upper = n / 2;
        while (lower <= upper) {
            int mid = lower + upper >> 1;
            bool have = false;
            for (int i = 0, j; i < n; i = j) {
                int min = sa[i];
                int max = sa[i];
                for (j = i + 1; j < n && height[j] >= mid; j++) {
                    min = std::min(min, sa[j]);
                    max = std::max(max, sa[j]);
                }
                if (max - min > mid) {
                    have = true;
                    break;
                }
            }
            if (have) {
                result = mid;
                lower = mid + 1;
            } else {
                upper = mid - 1;
            }
        }
        printf("%d\n", result >= 4 ? result + 1 : 0);
    }
}

