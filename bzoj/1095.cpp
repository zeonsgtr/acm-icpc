#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define index INDEX

const int INF = int(1e9);
const int N = 3 * int(1e5) + 100;

int n, q;
int first_edge[N], next_edge[N << 1], to[N << 1], edge_count;
int a[N], index[N], color[N], position[N], m;

void add_edge(int s, int t) {
    to[edge_count] = t;
    next_edge[edge_count] = first_edge[s];
    first_edge[s] = edge_count++;
}

void dfs(int x, int parent) {
    a[m++] = 0;
    position[index[m] = x] = m;
    a[m++] = -1;
    for (int itr = first_edge[x]; itr != -1; itr = next_edge[itr]) {
        int y = to[itr];
        if (y != parent) {
            dfs(y, x);
        }
    }
    a[m++] = 1;
}

struct Node {
    int lp, lm, rp, rm, d, first, second;
}tree[N * 4];

void modify_leaf(int k, int p) {
    if (a[p] == -1 && color[index[p]] == 0) {
        tree[k].lp = tree[k].lm = tree[k].rp = tree[k].rm = tree[k].d = 0;
    } else {
        tree[k].lp = tree[k].lm = tree[k].rp = tree[k].rm = tree[k].d = -INF;
    }
    tree[k].first = a[p] == 1;
    tree[k].second = a[p] == 0;
}

void update(int k) {
    Node& c = tree[k];
    const Node& a = tree[k << 1];
    const Node& b = tree[k << 1 | 1];
    if (a.second >= b.first) {
        c.first = a.first;
        c.second = a.second - b.first + b.second;
    } else {
        c.first = a.first - a.second + b.first;
        c.second = b.second;
    }
    c.d = std::max(std::max(a.rp + b.lm, a.rm + b.lp), std::max(a.d, b.d));
    c.rp = std::max(std::max(a.rp - b.first + b.second, a.rm + b.first + b.second), b.rp);
    c.rm = std::max(a.rm + b.first - b.second, b.rm);
    c.lp = std::max(std::max(b.lp - a.second + a.first, b.lm + a.first + a.second), a.lp);
    c.lm = std::max(b.lm + a.second - a.first, a.lm);
}

void build(int k, int s, int t) {
    if (s == t) {
        modify_leaf(k, s);
        return;
    }
    int mid = s + t >> 1;
    build(k << 1, s, mid);
    build(k << 1 | 1, mid + 1, t);
    update(k);
}

void modify(int k, int s, int t, int p) {
    if (s == t) {
        modify_leaf(k, s);
        return;
    }
    int mid = s + t >> 1;
    if (p <= mid) {
        modify(k << 1, s, mid, p);
    } else {
        modify(k << 1 | 1, mid + 1, t, p);
    }
    update(k);
}

int main() {
    scanf("%d", &n);
    std::fill(first_edge, first_edge + n, -1);
    edge_count = 0;
    for (int i = 1; i < n; i++) {
        int s, t;
        scanf("%d%d", &s, &t);
        s--, t--;
        add_edge(s, t);
        add_edge(t, s);
    }
    dfs(0, -1);
    build(1, 0, m - 1);
    scanf("%d", &q);
    char ctrl[9];
    while (q--) {
        scanf("%s", ctrl);
        if (ctrl[0] == 'G') {
            printf("%d\n", std::max(-1, tree[1].d));
        } else {
            int x;
            scanf("%d", &x);
            color[x - 1] ^= 1;
            modify(1, 0, m - 1, position[x - 1]);
        }
    }
}

